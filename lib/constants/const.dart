import 'package:choicestory/network/uploader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:soy_common/utils/utils.dart';
import 'package:choicestory/network/api_services.dart';
import 'package:choicestory/page_route/get_route.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/time_utils.dart';
import 'package:choicestory/utils/utils.dart';

/// Image picker
final imagePicker = ImagePicker();

/// Uploader
final uploadManager = UploadManager();

/// App color
Color mainColor = Colors.black;
Color appBgColor = littleGrey;

/// List of country
List<dynamic> countryList = [];

/// List of religion
List<dynamic> religionList = [];

/// List of profession
List<dynamic> professionList = [];

/// List of education
List<dynamic> educationLevelList = [];

/// Quick profile
UserPreference myPreference = UserPreference();

/// Quick profile
UserProfile qp = UserProfile(
  id: "",
  email: "",
  authPw: "",
);

/// Has called fcm
bool hasCalledFCMRegistration = false;
bool hasShownLocationDialog = false;
bool showMessageCount = true;
bool showLikeAnimation = true;
bool showGreetAnimation = true;
bool showNotificationCount = true;
double profileShadeValue = 0.8;

/// Firebase user holder
User? firebaseUser;

/// Firebase analysis
final FirebaseAnalytics analytics = FirebaseAnalytics();

/// Cloud firestore
final firestore = FirebaseFirestore.instance;

/// Cloud messaging
FirebaseMessaging? firebaseMessaging;

/// Remote config
RemoteConfig? remoteConfig;

/// Firebase auth instance
final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

/// tag
const userTag = "@";

/// device dimensions
double deviceHeight = 0;
double deviceWidth = 0;

/// youtube url
const youtubeUrl = "https://www.youtube.com/watch?v=";

/// App Theme
Color appTheme = CustomWhite;

/// shared pref
SharedPreferences? prefs;

/// Utils instance
Utils utils = Utils();

/// Common utils instance
CommonUtils commonUtils = CommonUtils();

/// Time utils instance
TimeUtils timeUtils = TimeUtils();

/// Api manager
ApiManager apiManager = ApiManager();

/// Navigator get
GetRoute getRoute = GetRoute();

String? apiBaseUrl;

/// Game IDs
const psnAddFriendBaseLink = "https://my.playstation.com/profile/";
const psnLoginLink = "https://my.playstation.com/";
const xboxAddFriendBaseLink = "https://account.xbox.com/en-US/profile?gamertag=";
const steamLoginLink = "https://steamcommunity.com/login/home/";
const steamProfileLink = "$steamLoginLink?goto=https://steamcommunity.com/profiles/";
const psnIdHelpLink = "https://www.playstation.com/en-us/support/account/change-psn-online-id/";
const xboxHelpLink = "https://support.microsoft.com/en-us/windows/what-s-a-gamertag-10782a06-e989-d248-9ba7-9b89499a1120";
const nintendoHelpLink = "https://www.nintendo.com.au/help/switch-system-set-up-and-operation/how-to-add-friends";
const steamCalculateFactor = 61197960265728;

/// App font family
const appFontFamily = "proxima";

/// header font familt
const headerFontFamily = "proxima_alt_bold";

/// Already seen profile ids
List<String> likeOrPassOrGreetedIds = [];

/// Common radius
final commonRadius = 16.0;

/// App name
const myAppName = "Flashery";

/// Company contact email
const companyEmail = "admin@yarner.app";

/// Icons 8 link
const icon8Link = "https://icons8.com/";

/// Company website
const companyWebsiteLink = "yarner.app";

/// Forum EULA link
const eulaLink = companyWebsiteLink + "/#/terms-of-use";

/// Link to privacy policy
const privacyPolicyUrl = companyWebsiteLink + "/#/privacy-policy";

/// App package information retrieved
String? appName, versionName;
int? versionCode;

const defaultProfilePictureLink = "https://res.cloudinary.com/hyclslbe2/image/upload/v1588150703/profile/placeholder_s9bajp.png";
const gameCoverLink = "https://firebasestorage.googleapis.com/v0/b/story-teller-3124b.appspot.com/o/profile%2FgameCoverHolder.png?alt=media&token=5a4f4b70-7d94-4f41-b951-157b5a6e6c75";
const verticalGreyPlaceholder = "https://firebasestorage.googleapis.com/v0/b/story-teller-3124b.appspot.com/o/general%2Fgrey_placeholder.jpg?alt=media&token=696a91b7-0f30-44b7-81e7-271072f1ac9b";
const horizontalGreyPlaceholder = "https://firebasestorage.googleapis.com/v0/b/story-teller-3124b.appspot.com/o/general%2Fhorizontal_placeholder.jpg?alt=media&token=b2ff0a20-5a9e-4b06-b839-ee5d1ba1d12c";