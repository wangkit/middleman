import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:soy_common/models/env.dart';

class UploadManager {

  Future<String?> uploadFileToFirebase(File file, {bool isVideo = false}) async {
    try {
      String fileName = path.basename(file.path);
      String referenceName;
      String folderName;
      switch (env!.flavor!) {
        case BuildFlavor.production:
          referenceName = "prod/";
          break;
        case BuildFlavor.development:
          referenceName = "dev/";
          break;
        case BuildFlavor.staging:
          referenceName = "staging/";
          break;
      }
      if (isVideo) {
        folderName = "video";
      } else {
        folderName = "image";
      }
      TaskSnapshot _uploadTask = await FirebaseStorage.instance
          .ref("$referenceName/$folderName/$fileName").putFile(file);
      return await _uploadTask.ref.getDownloadURL();
    } on Exception catch (e) {
      debugPrint("Firebase error: $e");
    }
    return null;
  }
}