import 'dart:convert';
import 'dart:io';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/activity.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/values/status_code.dart';
import 'package:choicestory/resources/values/user_status.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:choicestory/apps/home/main/main_page.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/analysis.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:soy_common/models/env.dart';
import 'package:uuid/uuid.dart';
import 'package:validators2/validators.dart';

class ApiManager {

  Future<void> block(UserProfile profile) async {
    logAnalysis(Analysis.block);
    String _newBlockId = Uuid().v4();
    await firestore.collection(FirestoreCollections.block).doc(_newBlockId).set({
      BlockCollection.id: qp.id!,
      BlockCollection.blockId: _newBlockId,
      BlockCollection.blockedId: profile.id,
      BlockCollection.createdAt: DateTime.now(),
    });
  }


  Future<void> report(UserProfile profile, String reason) async {
    logAnalysis(Analysis.report);
    String _newReportId = Uuid().v4();
    await firestore.collection(FirestoreCollections.report).doc(_newReportId).set({
      ReportCollection.id: qp.id!,
      ReportCollection.reportId: _newReportId,
      ReportCollection.reportedId: profile.id,
      ReportCollection.reason: reason,
      ReportCollection.createdAt: DateTime.now(),
    });
  }

  Future<void> updateProfile(UserProfile profile) async {
    Dialogs.showLoadingDialog(title: "Updating profile...");
    await firestore.collection(FirestoreCollections.profiles).doc(qp.id).update({
      UserCollection.id: qp.id!,
      UserCollection.city: profile.city,
      UserCollection.country: profile.country,
      UserCollection.birthday: profile.birthday,
      UserCollection.age: profile.age,
      UserCollection.profession: profile.profession,
      UserCollection.education: profile.education,
      UserCollection.religion: profile.religion,
      UserCollection.personality: profile.personality,
      UserCollection.physicalGender: profile.physicalGender != null ? GenderExtension.names[profile.physicalGender] : null,
      UserCollection.genderIdentity: profile.genderIdentity != null ? GenderExtension.names[profile.genderIdentity] : null,
      UserCollection.relationshipStatus: profile.relationshipStatus != null ? RelationshipStatusExtension.names[profile.relationshipStatus] : null,
      UserCollection.lastName: profile.lastName,
      UserCollection.firstName: profile.firstName,
      UserCollection.longitude: profile.longitude,
      UserCollection.latitude: profile.latitude,
      UserCollection.zodiacSign: profile.zodiacSign != null ? ZodiacSignExtension.names[profile.zodiacSign] : null,
      UserCollection.updatedAt: DateTime.now(),
      UserCollection.lastActiveAt: DateTime.now(),
    });
    if (qp.lastName != profile.lastName || qp.firstName != profile.firstName) {
      /// Update name for notifications
      var notificationData = await firestore.collection(FirestoreCollections.notifications).where(NotificationCollection.senderId, isEqualTo: qp.id!).get();
      var chatData = await firestore.collection(FirestoreCollections.chats).where(ChatCollection.targetId, isEqualTo: qp.id!).get();

      if (notificationData.size > 0) {
        for (QueryDocumentSnapshot snapshot in notificationData.docs) {
          firestore.collection(FirestoreCollections.notifications).doc(snapshot.id).update({
            NotificationCollection.senderLastName: profile.lastName,
            NotificationCollection.senderFirstName: profile.firstName,
          });
        }
      }

      /// Update name for chats
      if (chatData.size > 0) {
        for (QueryDocumentSnapshot snapshot in chatData.docs) {
          firestore.collection(FirestoreCollections.chats).doc(snapshot.id).update({
            ChatCollection.targetLastName: profile.lastName,
            ChatCollection.targetFirstName: profile.firstName,
          });
        }
      }
    }
    getRoute.pop();
  }

  Future<void> updatePreference(UserPreference preference) async {
    Dialogs.showLoadingDialog(title: "Updating preference...");
    DateTime _now = DateTime.now();
    QuerySnapshot _data = await firestore.collection(FirestoreCollections.preferences).where(UserCollection.id, isEqualTo: qp.id).get();
    Map<String, dynamic> _newPreference = {
      UserCollection.id: qp.id!,
      UserCollection.distance: preference.distance,
      UserCollection.physicalGender: preference.physicalGender != null ? GenderExtension.names[preference.physicalGender] : null,
      UserCollection.genderIdentity: preference.genderIdentity != null ? GenderExtension.names[preference.genderIdentity] : null,
      UserCollection.minAge: preference.minAge,
      UserCollection.maxAge: preference.maxAge,
      UserCollection.zodiacSign: preference.zodiacSign != null ? ZodiacSignExtension.names[preference.zodiacSign] : null,
      UserCollection.profession: preference.profession,
      UserCollection.education: preference.education,
      UserCollection.religion: preference.religion,
      UserCollection.relationshipStatus: preference.relationshipStatus != null ? RelationshipStatusExtension.names[preference.relationshipStatus] : null,
      UserCollection.updatedAt: _now,
    };
    if (_data.size == 0) {
      _newPreference.addAll({
        UserCollection.createdAt: _now,
      });
      await firestore.collection(FirestoreCollections.preferences).doc(qp.id).set(_newPreference);
    } else {
      await firestore.collection(FirestoreCollections.preferences).doc(qp.id).update(_newPreference);
    }
    getRoute.pop();
  }

  Future<void> signIn(String email, String password, {String social = ""}) async {
    Dialogs.showLoadingDialog(title: TextData.signingIn);
    QuerySnapshot snapshot = await firestore
        .collection(FirestoreCollections.profiles)
        .where(UserCollection.email, isEqualTo: email)
        .where(UserCollection.password, isEqualTo: commonUtils.encodeString(password))
        .where(UserCollection.status, isEqualTo: UserStatus.normal)
        .get();
    if (snapshot.size > 0) {
      Map _myData = snapshot.docs.first.data() as Map;
      qp = UserProfile.fromMapToProfile(_myData);
      utils.saveStr(PrefKey.email, qp.email!);
      utils.saveStr(PrefKey.id, qp.id!);
      utils.saveStr(PrefKey.authPwKey, qp.authPw!);
      await signInToFirebase();
      logAnalysis(Analysis.signIn);
      getRoute.pop();
      getRoute.navigateToAndPopAll(MainPage());
      utils.toast(TextData.signInSuccessfully);
    } else {
      logAnalysis(Analysis.signInFail);
      getRoute.pop();
      utils.toast(TextData.credentialFail, isWarning: true);
    }
  }

  Future<bool> isEmailTaken(String email) async {
    QuerySnapshot data = await firestore
        .collection(FirestoreCollections.users)
        .where(UserCollection.email, isEqualTo: email)
        .get();
    return data.size > 0;
  }

  Future<void> sendNotification(String targetId, Activity activity) async {
    String _notiId = Uuid().v4();
    sendPush(targetId, activity.value.toString());
    firestore.collection(FirestoreCollections.notifications).doc(_notiId).set({
      NotificationCollection.id: _notiId,
      NotificationCollection.isRead: false,
      NotificationCollection.senderId: qp.id!,
      NotificationCollection.action: activity.value,
      NotificationCollection.senderFirstName: qp.firstName,
      NotificationCollection.senderLastName: qp.lastName,
      NotificationCollection.senderImages: qp.images,
      NotificationCollection.targetId: targetId,
      NotificationCollection.createdAt: DateTime.now(),
    });
  }

  Future<void> greet(String targetId) async {
    sendNotification(targetId, Activity.greet);
    firestore.collection(FirestoreCollections.greets).doc(qp.id! + targetId).set({
      MatchCollection.id: qp.id!,
      MatchCollection.targetId: targetId,
      MatchCollection.createdAt: DateTime.now(),
    });
    firestore.collection(FirestoreCollections.profiles).doc(targetId).update({
      UserCollection.greetCount: FieldValue.increment(1),
    });
  }

  Future<void> sendPush(String targetId, String action) async {
    final url = env!.baseUrl! + "/sendPush?targetId=$targetId&action=$action&firstName=${qp.firstName}";
    await http.get(Uri.parse(url));
  }

  Future<void> updateImage(List<String> imageLink) async {
    firestore.collection(FirestoreCollections.profiles).doc(qp.id).update({
      UserCollection.images: imageLink,
    });
    qp.images = imageLink;
  }

  Future<void> matchOrPass(UserProfile profile, bool isMatch) async {
    String targetId = profile.id!;

    if (isMatch) {
      sendNotification(targetId, Activity.match);
    }

    /// Remove from current batch
    qp.currentBatchIds!.remove(profile.id);
    await firestore.collection(FirestoreCollections.profiles).doc(qp.id!).update({
      UserCollection.currentBatchIds: qp.currentBatchIds,
    });

    /// Create new match
    firestore.collection(FirestoreCollections.matches).doc(qp.id! + targetId).set({
      MatchCollection.id: qp.id!,
      MatchCollection.targetId: targetId,
      MatchCollection.isMatch: isMatch,
      MatchCollection.createdAt: DateTime.now(),
    });

    /// Alter the match count of the target
    firestore.collection(FirestoreCollections.profiles).doc(targetId).update({
      UserCollection.likeCount: FieldValue.increment(isMatch ? 1 : 0),
    });

    /// Check if these two have liked each other
    DocumentSnapshot _data = await firestore.collection(FirestoreCollections.matches)
        .doc(targetId + qp.id!)
        .get();
    if (_data.exists) {
      Map _map = _data.data() as Map;
      bool _hasTargetLikedThisUser = _map[MatchCollection.isMatch];
      DateTime _now = DateTime.now();
      /// If they have liked each other
      if (_hasTargetLikedThisUser) {
        /// Create a chat room for them
        /// First, create a room for this user
        firestore.collection(FirestoreCollections.chats).doc(qp.id! + targetId).set({
          ChatCollection.targetId: targetId,
          ChatCollection.userId: qp.id,
          ChatCollection.lastMessage: AppConfig.newChatRoomMessage,
          ChatCollection.targetLastName: profile.lastName!,
          ChatCollection.targetFirstName: profile.firstName!,
          ChatCollection.isLastMessageMyself: false,
          ChatCollection.hasUnread: false,
          ChatCollection.targetImages: profile.images,
          ChatCollection.createdAt: _now,
          ChatCollection.updatedAt: _now,
        });
        /// Then, create a room for the target
        firestore.collection(FirestoreCollections.chats).doc(targetId + qp.id!).set({
          ChatCollection.targetId: qp.id!,
          ChatCollection.userId: targetId,
          ChatCollection.lastMessage: AppConfig.newChatRoomMessage,
          ChatCollection.targetLastName: qp.lastName!,
          ChatCollection.targetFirstName: qp.firstName!,
          ChatCollection.isLastMessageMyself: false,
          ChatCollection.hasUnread: false,
          ChatCollection.targetImages: qp.images,
          ChatCollection.createdAt: _now,
          ChatCollection.updatedAt: _now,
        });

        /// Insert into pair table
        firestore.collection(FirestoreCollections.pairs).add({
          PairCollection.pairs: [qp.id, targetId],
          PairCollection.createdAt: _now,
        });

        /// Increase match count
        firestore.collection(FirestoreCollections.profiles).doc(targetId).update({
          UserCollection.matchCount: FieldValue.increment(1),
        });

        firestore.collection(FirestoreCollections.profiles).doc(qp.id).update({
          UserCollection.matchCount: FieldValue.increment(1),
        });
      }
    }
  }

  void deleteChat(bool isOnlyMe, String targetId) {
    firestore.collection(FirestoreCollections.chats).doc(qp.id! + targetId).delete();
    if (!isOnlyMe) {
      /// Delete chat for other party as well
      firestore.collection(FirestoreCollections.chats).doc(targetId + qp.id!).delete();
    }
  }

  Future<void> clearSingleMessage(String messageId, String targetId) async {
    firestore.collection(FirestoreCollections.messages).doc(messageId).delete();
    DocumentSnapshot _doc = await firestore.collection(FirestoreCollections.chats).doc(qp.id! + targetId).get();
    if (_doc.exists) {
      Map _map = _doc.data() as Map;
      String _lastMessageId = _map[ChatCollection.lastMessageId];
      if (_lastMessageId == messageId) {
        firestore.collection(FirestoreCollections.chats).doc(qp.id! + targetId).update({
          ChatCollection.hasUnread: false,
          ChatCollection.lastMessage: AppConfig.deletedMessage,
          ChatCollection.isLastMessageMyself: false,
        });
        /// Update target chat item
        firestore.collection(FirestoreCollections.chats).doc(targetId + qp.id!).update({
          ChatCollection.hasUnread: false,
          ChatCollection.lastMessage: AppConfig.deletedMessage,
          ChatCollection.isLastMessageMyself: false,
        });
      }
    }
  }

  void clearMessageInChat(String chatId, String targetId) {
    firestore.collection(FirestoreCollections.messages).where(MessageCollection.chatId, isEqualTo: chatId).get().then((result) {
      if (result.docs.isNotEmpty) {
        result.docs.forEach((messageToBeDeleted) {
          firestore.collection(FirestoreCollections.messages).doc(messageToBeDeleted.id).delete();
        });
      }
      /// Update my chat item
      firestore.collection(FirestoreCollections.chats).doc(qp.id! + targetId).update({
        ChatCollection.hasUnread: false,
        ChatCollection.lastMessage: "",
        ChatCollection.isLastMessageMyself: false,
      });
      /// Update target chat item
      firestore.collection(FirestoreCollections.chats).doc(targetId + qp.id!).update({
        ChatCollection.hasUnread: false,
        ChatCollection.lastMessage: "",
        ChatCollection.isLastMessageMyself: false,
      });
    });
  }

  Future<void> updateCurrentBatch(List<String> batchId) async {
    firestore.collection(FirestoreCollections.profiles).doc(qp.id).update({
      UserCollection.currentBatchIds: batchId,
      UserCollection.lastActiveAt: DateTime.now(),
    });
    qp.currentBatchIds = batchId;
  }

  Future<void> clearUnseenNotifications() async {
    QuerySnapshot _data = await firestore.collection(FirestoreCollections.notifications).where(NotificationCollection.targetId, isEqualTo: qp.id!).where(NotificationCollection.isRead, isEqualTo: false).get();
    for (QueryDocumentSnapshot _snapshot in _data.docs) {
      await firestore.collection(FirestoreCollections.notifications).doc(_snapshot.id).update({
        NotificationCollection.isRead: true,
      });
    }
  }

  Future<void> clearUnreadChat() async {
    QuerySnapshot _data = await firestore.collection(FirestoreCollections.messages).where(MessageCollection.receiverId, isEqualTo: qp.id!).where(MessageCollection.unreadPartyId, arrayContains: qp.id!).get();
    for (QueryDocumentSnapshot _snapshot in _data.docs) {
      await firestore.collection(FirestoreCollections.messages).doc(_snapshot.id).update({
        MessageCollection.unreadPartyId: FieldValue.arrayRemove([qp.id!]),
      });
    }
  }

  Future<void> deleteAccount() async {
    await firestore.collection(FirestoreCollections.profiles).doc(qp.id).update({
      UserCollection.status: UserStatus.deleted,
    });
    await firebaseUser!.delete();
  }

  Future<void> createAccount(String password, UserProfile profile, {String profilePicUrl = defaultProfilePictureLink}) async {
    /// Create firebase auth first
    String authPw = utils.generatePassword();
    if (isEmail(profile.email!)) {
      if (await isEmailTaken(profile.email!)) {
        debugPrint("Already exist email");
        getRoute.pop();
        utils.toast(TextData.duplicateEmail, isWarning: true);
      } else {
        bool success = await createFirebaseAccount(profile.email!, authPw);
        if (success) {
          debugPrint('No such user. Creating new...');
          DeviceInfoPlugin deviceInfoPlug = DeviceInfoPlugin();
          String deviceName, deviceVersion, deviceId;
          if (Platform.isAndroid) {
            final androidInfo = await deviceInfoPlug.androidInfo;
            deviceName = androidInfo.model!;
            deviceVersion = androidInfo.version.release.toString();
            deviceId = androidInfo.androidId!;
          } else {
            final iosInfo = await deviceInfoPlug.iosInfo;
            deviceName = iosInfo.name!;
            deviceVersion = iosInfo.systemVersion!;
            deviceId = iosInfo.identifierForVendor!;
          }
          String _newId = Uuid().v4();
          DateTime _now = DateTime.now().toUtc();
          logAnalysis(Analysis.signUp);
          profile.id = _newId;
          profile.firebaseUserId = firebaseUser!.uid;
          profile.authPw = authPw;
          profile.createdAt = _now;
          profile.updatedAt = _now;
          profile.lastActiveAt = _now;
          profile.isShowDiscover = true;
          profile.isReceive = true;
          UserPreference _defaultPreference = UserPreference(
            relationshipStatus: profile.relationshipStatus,
            minAge: AppConfig.minAge,
            maxAge: AppConfig.maxAge,
            zodiacSign: ZodiacSign.gemini,
            distance: AppConfig.minDistance,
            physicalGender: profile.physicalGender == Gender.female ? Gender.male : profile.physicalGender == Gender.transwoman ? Gender.transman : Gender.female,
            genderIdentity: profile.physicalGender == Gender.female ? Gender.male : profile.physicalGender == Gender.transwoman ? Gender.transman : Gender.female,
          );
          await firestore.collection(FirestoreCollections.users).doc(_newId).set({
            UserCollection.email: profile.email,
          });
          firestore.collection(FirestoreCollections.unseen).doc(_newId).set({
            UnseenCollection.id: _newId,
            UnseenCollection.unseenCount: 0,
            UnseenCollection.updatedAt: _now,
            UnseenCollection.createdAt: _now,
          });
          await firestore.collection(FirestoreCollections.preferences).doc(_newId).set({
            UserCollection.id: qp.id!,
            UserCollection.distance: _defaultPreference.distance,
            UserCollection.physicalGender: GenderExtension.names[_defaultPreference.physicalGender],
            UserCollection.genderIdentity: GenderExtension.names[_defaultPreference.genderIdentity],
            UserCollection.minAge: _defaultPreference.minAge,
            UserCollection.maxAge: _defaultPreference.maxAge,
            UserCollection.zodiacSign: ZodiacSignExtension.names[_defaultPreference.zodiacSign],
            UserCollection.relationshipStatus: RelationshipStatusExtension.names[_defaultPreference.relationshipStatus],
            UserCollection.updatedAt: _now,
            UserCollection.createdAt: _now,
          });
          await firestore.collection(FirestoreCollections.profiles).doc(_newId).set({
            UserCollection.id: _newId,
            UserCollection.firebaseUserId: firebaseUser!.uid,
            UserCollection.authPw: authPw,
            UserCollection.password: commonUtils.encodeString(password),
            UserCollection.email: profile.email,
            UserCollection.createdAt: _now,
            UserCollection.deviceId: deviceId,
            UserCollection.deviceVersion: deviceVersion,
            UserCollection.likeCount: 0,
            UserCollection.greetCount: 0,
            UserCollection.matchCount: 0,
            UserCollection.deviceModel: deviceName,
            UserCollection.updatedAt: _now,
            UserCollection.lastName: profile.lastName,
            UserCollection.lastActiveAt: _now,
            UserCollection.firstName: profile.firstName,
            UserCollection.country: profile.country,
            UserCollection.city: profile.city,
            UserCollection.isReceive: profile.isReceive,
            UserCollection.isShowDiscover: profile.isShowDiscover,
            UserCollection.longitude: profile.longitude,
            UserCollection.latitude: profile.latitude,
            UserCollection.genderIdentity: GenderExtension.names[profile.genderIdentity],
            UserCollection.physicalGender: GenderExtension.names[profile.physicalGender],
            UserCollection.zodiacSign: ZodiacSignExtension.names[profile.zodiacSign],
            UserCollection.personality: profile.personality,
            UserCollection.relationshipStatus: RelationshipStatusExtension.names[profile.relationshipStatus],
            UserCollection.birthday: profile.birthday,
            UserCollection.status: UserStatus.normal,
          });
          qp = profile;
          myPreference = _defaultPreference;
          utils.saveStr(PrefKey.id, qp.id!);
          utils.saveStr(PrefKey.email, qp.email!);
          utils.saveStr(PrefKey.authPwKey, qp.authPw!);
          await apiManager.signInToFirebase();
        }
      }
    }
  }

  Future<void> signInToFirebase({String? email, String? password}) async {
    try {
      firebaseUser = (await firebaseAuth.signInWithEmailAndPassword(
        email: email != null ? email : qp.email!,
        password: password != null ? password : qp.authPw!,
      )).user;
      if (firebaseUser != null) {
        await setFcmToken();
      }
    } on Exception catch (e) {
      debugPrint("catch: $e");
    }
  }

  Future<void> setConfig(String key, bool value) async {
    firestore.collection(FirestoreCollections.profiles).doc(qp.id!).update({
      key: value,
    });
  }

  Future<void> setFcmToken({bool forceUseToken = false}) async {
    if (!hasCalledFCMRegistration || forceUseToken) {
      firebaseMessaging!.getToken().then((String? token) async {
        if (!token!.isNullOrEmpty) {
          DocumentSnapshot existingFcm = await firestore.collection(FirestoreCollections.fcmToken).doc(qp.id!).get();
          if (existingFcm.exists) {
            firestore.collection(FirestoreCollections.fcmToken).doc(qp.id!).update({
              FcmTokenCollection.token: token,
              FcmTokenCollection.updatedAt: DateTime.now().toUtc(),
            });
          } else {
            firestore.collection(FirestoreCollections.fcmToken).doc(qp.id!).set({
              FcmTokenCollection.id: qp.id!,
              FcmTokenCollection.email: qp.email!,
              FcmTokenCollection.token: token,
              FcmTokenCollection.createdAt: DateTime.now().toUtc(),
              FcmTokenCollection.updatedAt: DateTime.now().toUtc(),
            });
          }
          hasCalledFCMRegistration = true;
          utils.saveBoo(PrefKey.hasCalledFCMRegistration, hasCalledFCMRegistration);
        }
      });
    }
  }

  Future<bool> createFirebaseAccount(String email, String password) async {
    try {
      firebaseUser = (await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      )).user;
      return true;
    } on Exception catch (e) {
      debugPrint("Create firebase error: $e");
      getRoute.pop();
      utils.toast(TextData.duplicateEmail, isWarning: true);
      return false;
    }
  }

  Future<void> logAnalysis(String name) async {
    analytics.logEvent(
      name: name.trim(),
    );
  }

}