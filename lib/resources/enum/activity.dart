import 'package:choicestory/resources/values/text.dart';

enum Activity {
  match,
  greet,
  message,
}

extension ActivityExtension on Activity {

  static const values = {
    Activity.match: 0,
    Activity.greet: 1,
    Activity.message: 2
  };

  static const messages = {
    0: " consider you a match!😍😍😍",
    1: " sent you greetings.",
    2: " sent you a message.",
  };

  int? get value => values[this];
}

