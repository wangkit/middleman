import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

enum RelationshipStatus {
  marriage,
  longTerm,
  causal,
  soulmate,
  friend
}

extension RelationshipStatusExtension on RelationshipStatus {

  static const names = {
    RelationshipStatus.marriage: TextData.marriage,
    RelationshipStatus.longTerm: TextData.longTerm,
    RelationshipStatus.causal: TextData.causal,
    RelationshipStatus.soulmate: TextData.soulmate,
    RelationshipStatus.friend: TextData.friend,
  };

  static const enums = {
    TextData.marriage: RelationshipStatus.marriage,
    TextData.longTerm: RelationshipStatus.longTerm,
    TextData.causal: RelationshipStatus.causal,
    TextData.soulmate: RelationshipStatus.soulmate,
    TextData.friend: RelationshipStatus.friend,
  };

  static final Iterable<String> valueList = enums.keys.toList();

  String? get name => names[this];

  RelationshipStatus? get toEnum => enums[this];
}

