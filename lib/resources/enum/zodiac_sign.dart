import 'package:choicestory/resources/values/text.dart';

enum ZodiacSign {
  aquarius,
  capricorn,
  pisces,
  aries,
  taurus,
  gemini,
  cancer,
  leo,
  virgo,
  libra,
  scorpio,
  sagittarius,
  noPreference,
}

extension ZodiacSignExtension on ZodiacSign {

  static const names = {
    ZodiacSign.aquarius: TextData.aquarius,
    ZodiacSign.capricorn: TextData.capricorn,
    ZodiacSign.pisces: TextData.pisces,
    ZodiacSign.aries: TextData.aries,
    ZodiacSign.taurus: TextData.taurus,
    ZodiacSign.gemini: TextData.gemini,
    ZodiacSign.cancer: TextData.cancer,
    ZodiacSign.leo: TextData.leo,
    ZodiacSign.virgo: TextData.virgo,
    ZodiacSign.libra: TextData.libra,
    ZodiacSign.scorpio: TextData.scorpio,
    ZodiacSign.sagittarius: TextData.sagittarius,
    ZodiacSign.noPreference: TextData.noPreference,
  };

  static const enums = {
    TextData.aquarius: ZodiacSign.aquarius,
    TextData.capricorn: ZodiacSign.capricorn,
    TextData.pisces: ZodiacSign.pisces,
    TextData.aries: ZodiacSign.aries,
    TextData.taurus: ZodiacSign.taurus,
    TextData.gemini: ZodiacSign.gemini,
    TextData.cancer: ZodiacSign.cancer,
    TextData.leo: ZodiacSign.leo,
    TextData.virgo: ZodiacSign.virgo,
    TextData.libra: ZodiacSign.libra,
    TextData.scorpio: ZodiacSign.scorpio,
    TextData.sagittarius: ZodiacSign.sagittarius,
    TextData.noPreference: ZodiacSign.noPreference,
  };

  static final Iterable<String> valueList = enums.keys.toList();

  String? get name => names[this];

  ZodiacSign? get toEnum => enums[this];
}

