import 'package:choicestory/resources/values/text.dart';

enum Gender {
  male,
  female,
  transwoman,
  transman,
  agender,
}

extension GenderExtension on Gender {

  static const names = {
    Gender.male: TextData.male,
    Gender.female: TextData.female,
    Gender.transwoman: TextData.transwoman,
    Gender.transman: TextData.transman,
    Gender.agender: TextData.agender,
  };

  static const enums = {
    TextData.male: Gender.male,
    TextData.female: Gender.female,
    TextData.transman: Gender.transman,
    TextData.transwoman: Gender.transwoman,
    TextData.agender: Gender.agender,
  };

  static final Iterable<String> valueList = [""].followedBy(enums.keys.toList());

  static final Iterable<String> valueListWithoutSpace = enums.keys.toList();

  String? get name => names[this];

  Gender? get toEnum => enums[this];
}

