class UserCollection {
  static const String id = "id";
  static const String firebaseUserId = "firebaseUserId";
  static const String password = "password";
  static const String authPw = "authPw";
  static const String createdAt = "createdAt";
  static const String email = "email";
  static const String deviceId = "deviceId";
  static const String deviceModel = "deviceModel";
  static const String deviceVersion = "deviceVersion";
  static const String images = "images";
  static const String lastName = "lastName";
  static const String greetCount = "greetCount";
  static const String firstName = "firstName";
  static const String country = "country";
  static const String isShowDiscover = "isShowDiscover";
  static const String city = "city";
  static const String likeCount = "likeCount";
  static const String matchCount = "matchCount";
  static const String isReceive = "isReceive";
  static const String currentBatchIds = "currentBatchIds";
  static const String longitude = "longitude";
  static const String maxAge = "maxAge";
  static const String minAge = "minAge";
  static const String latitude = "latitude";
  static const String genderIdentity = "genderIdentity";
  static const String physicalGender = "physicalGender";
  static const String religion = "religion";
  static const String distance = "distance";
  static const String age = "age";
  static const String zodiacSign = "zodiacSign";
  static const String profession = "profession";
  static const String education = "education";
  static const String personality = "personality";
  static const String relationshipStatus = "relationshipStatus";
  static const String birthday = "birthday";
  static const String lastActiveAt = "lastActiveAt";
  static const String updatedAt = "updatedAt";
  static const String status = "status";
}

class GreetCollection {
  static String id = 'id';
  static String targetId = 'targetId';
  static String createdAt = 'createdAt';
}

class MatchCollection {
  static String id = 'id';
  static String targetId = 'targetId';
  static String isMatch = 'isMatch';
  static String createdAt = 'createdAt';
}

class MessageCollection {
  static String messageId = 'messageId';
  static String message = 'message';
  static String createdAt = 'createdAt';
  static String senderName = 'senderName';
  static String receiverName = 'receiverName';
  static String senderId = 'senderId';
  static String chatId = 'chatId';
  static String receiverId = 'receiverId';
  static String unreadPartyId = 'unreadPartyId';
}

class NotificationCollection {
  static String senderId = "senderId";
  static String id = "id";
  static String targetId = "targetId";
  static String isRead = "isRead";
  static String senderFirstName = "senderFirstName";
  static String senderLastName = "senderLastName";
  static String senderImages = "senderImages";
  static String action = "action";
  static String createdAt = 'createdAt';
}

class PairCollection {
  static String pairs = "pairs";
  static String createdAt = "createdAt";
}

class ChatCollection {
  static String userId = "userId";
  static String lastMessageId = "lastMessageId";
  static String targetId = 'targetId';
  static String lastMessage = 'lastMessage';
  static String targetLastName = 'targetLastName';
  static String targetFirstName = 'targetFirstName';
  static String isLastMessageMyself = 'isLastMessageMyself';
  static String hasUnread = 'hasUnread';
  static String targetImages = 'targetImages';
  static String createdAt = 'createdAt';
  static String updatedAt = 'updatedAt';
}

class BlockCollection {
  static const String id = "id";
  static const String blockId = "blockId";
  static const String blockedId = "blockedId";
  static const String createdAt = "createdAt";
}

class ReportCollection {
  static const String id = "id";
  static const String reportId = "reportId";
  static const String reportedId = "reportedId";
  static const String reason = "reason";
  static const String createdAt = "createdAt";
}

class UnseenCollection {
  static const String id = "id";
  static const String unseenCount = "unseenCount";
  static const String createdAt = "createdAt";
  static const String updatedAt = "updatedAt";
}

class FcmTokenCollection {
  static const String email = "email";
  static const String id = "id";
  static const String token = "token";
  static const String createdAt = "createdAt";
  static const String updatedAt = "updatedAt";
}
