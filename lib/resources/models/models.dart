import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'dbmodels.dart';

String? _getString(Map map, String key) {
  if (map.containsKey(key)) {
    return map[key];
  }
  return null;
}

bool? _getBool(Map map, String key, {bool defaultValue = true}) {
  if (map.containsKey(key)) {
    return map[key];
  }
  return defaultValue;
}

int? _getInt(Map map, String key, {int defaultValue = 0}) {
  if (map.containsKey(key)) {
    return map[key];
  }
  return defaultValue;
}

double? _getDouble(Map map, String key) {
  if (map.containsKey(key)) {
    return map[key];
  }
  return null;
}

Gender? _getGender(Map map, String key) {
  if (map.containsKey(key)) {
    return GenderExtension.enums[map[key]];
  }
  return null;
}

RelationshipStatus? _getRelationshipStatus(Map map, String key) {
  if (map.containsKey(key)) {
    return RelationshipStatusExtension.enums[map[key]];
  }
  return null;
}

ZodiacSign? _getZodiac(Map map, String key) {
  if (map.containsKey(key)) {
    return ZodiacSignExtension.enums[map[key]];
  }
  return null;
}

DateTime? _getDateTime(Map map, String key) {
  if (map.containsKey(key)) {
    return timeUtils.getDateTimeFromTimeStamp(map[key]) != null ? timeUtils.getDateTimeFromTimeStamp(map[key])!.toLocal() : null;
  }
  return null;
}

class FirestoreCollections {
  static const users = "users";
  static const profiles = "profiles";
  static const greets = "greets";
  static const preferences = "preferences";
  static const fcmToken = "fcm_token";
  static const messages = "messages";
  static const notifications = "notifications";
  static const unseen = "unseen";
  static const chats = "chats";
  static const pairs = "pairs";
  static const matches = "matches";
  static const report = "reports";
  static const block = "block";
}

class UserPreference {
  String? id;
  double? distance;
  Gender? physicalGender;
  Gender? genderIdentity;
  int? minAge;
  int? maxAge;
  ZodiacSign? zodiacSign;
  String? profession;
  String? education;
  String? religion;
  RelationshipStatus? relationshipStatus;
  DateTime? updatedAt;
  DateTime? createdAt;

  UserPreference({
    this.id,
    this.distance,
    this.physicalGender,
    this.genderIdentity,
    this.minAge,
    this.maxAge,
    this.zodiacSign,
    this.profession,
    this.education,
    this.religion,
    this.relationshipStatus,
    this.updatedAt,
    this.createdAt,
  });

  static UserPreference fromMapToPreference(Map map) {

    return UserPreference(
      id: map[UserCollection.id],
      religion: _getString(map, UserCollection.religion),
      distance: _getDouble(map, UserCollection.distance),
      physicalGender: _getGender(map, UserCollection.physicalGender),
      genderIdentity: _getGender(map, UserCollection.genderIdentity),
      zodiacSign: _getZodiac(map, UserCollection.zodiacSign),
      relationshipStatus: _getRelationshipStatus(map, UserCollection.relationshipStatus),
      profession: _getString(map, UserCollection.profession),
      education: _getString(map, UserCollection.education),
      minAge: _getInt(map, UserCollection.minAge),
      maxAge: _getInt(map, UserCollection.maxAge),
      updatedAt: _getDateTime(map, UserCollection.updatedAt),
      createdAt: _getDateTime(map, UserCollection.createdAt),
    );
  }
}

class NotificationMessage {
  String id;
  String senderId;
  String targetId;
  int action;
  bool isRead;
  DateTime createdAt;
  bool isTime;
  String senderFirstName;
  String senderLastName;
  List<String> senderImages;

  NotificationMessage({
    required this.id,
    required this.senderId,
    required this.targetId,
    required this.senderFirstName,
    required this.senderLastName,
    required this.senderImages,
    required this.action,
    required this.isRead,
    required this.createdAt,
    this.isTime = false,
  });

  static NotificationMessage fromMapToNotification(Map map) {

    return NotificationMessage(
      id: map[NotificationCollection.id],
      senderId: map[NotificationCollection.senderId],
      targetId: map[NotificationCollection.targetId],
      action: map[NotificationCollection.action],
      senderFirstName: map[NotificationCollection.senderFirstName],
      senderLastName: map[NotificationCollection.senderLastName],
      senderImages: map[NotificationCollection.senderImages].cast<String>(),
      isRead: map[NotificationCollection.isRead],
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[NotificationCollection.createdAt])!.toLocal(),
    );
  }
}

class ChatItem {
  String userId;
  String targetId;
  String targetLastName;
  String lastMessageId;
  String targetFirstName;
  String lastMessage;
  bool isLastMessageMyself;
  bool hasUnread;
  List<String> targetImages;
  DateTime createdAt;
  DateTime updatedAt;

  ChatItem({
    required this.userId,
    required this.lastMessageId,
    required this.targetId,
    required this.targetImages,
    required this.updatedAt,
    required this.isLastMessageMyself,
    required this.targetLastName,
    required this.targetFirstName,
    required this.createdAt,
    required this.hasUnread,
    required this.lastMessage,
  });

  static ChatItem fromMapToChatItem(Map map) {

    return ChatItem(
      userId: map[ChatCollection.userId],
      targetId: map[ChatCollection.targetId],
      lastMessageId: map[ChatCollection.lastMessageId],
      targetImages: map[ChatCollection.targetImages].cast<String>(),
      isLastMessageMyself: map[ChatCollection.isLastMessageMyself],
      targetFirstName: map[ChatCollection.targetFirstName],
      targetLastName: map[ChatCollection.targetLastName],
      hasUnread: map[ChatCollection.hasUnread],
      lastMessage: map[ChatCollection.lastMessage],
      updatedAt: timeUtils.getDateTimeFromTimeStamp(map[ChatCollection.updatedAt])!.toLocal(),
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[ChatCollection.createdAt])!.toLocal(),
    );
  }
}

class UserProfile {
  String? id;
  String? authPw;
  String? firebaseUserId;
  String? email;
  String? lastName;
  String? firstName;
  String? country;
  String? city;
  double? longitude;
  double? latitude;
  Gender? physicalGender;
  Gender? genderIdentity;
  int? age;
  ZodiacSign? zodiacSign;
  String? profession;
  bool? isReceive;
  bool? isShowDiscover;
  String? education;
  String? religion;
  List<String>? images;
  int? greetCount;
  int? matchCount;
  String? personality;
  int? likeCount;
  RelationshipStatus? relationshipStatus;
  List<String>? currentBatchIds;
  DateTime? birthday;
  DateTime? lastActiveAt;
  DateTime? updatedAt;
  DateTime? createdAt;

  UserProfile({
    this.authPw,
    this.id,
    this.email,
    this.firebaseUserId,
    this.createdAt,
    this.greetCount,
    this.matchCount,
    this.lastName,
    this.likeCount,
    this.isShowDiscover,
    this.firstName,
    this.religion,
    this.country,
    this.city,
    this.longitude,
    this.latitude,
    this.isReceive,
    this.physicalGender,
    this.genderIdentity,
    this.age,
    this.zodiacSign,
    this.profession,
    this.education,
    this.images,
    this.currentBatchIds,
    this.personality,
    this.relationshipStatus,
    this.birthday,
    this.lastActiveAt,
    this.updatedAt,
  });

  static UserProfile fromMapToProfile(Map map) {

    return UserProfile(
      authPw: map[UserCollection.authPw],
      email: map[UserCollection.email],
      id: map[UserCollection.id],
      lastName: map[UserCollection.lastName],
      firstName: map[UserCollection.firstName],
      religion: _getString(map, UserCollection.religion),
      country: map[UserCollection.country],
      city: map[UserCollection.city],
      likeCount: _getInt(map, UserCollection.likeCount, defaultValue: 0),
      matchCount: _getInt(map, UserCollection.matchCount, defaultValue: 0),
      isReceive: _getBool(map, UserCollection.isReceive, defaultValue: true),
      longitude: map[UserCollection.longitude],
      latitude: map[UserCollection.latitude],
      isShowDiscover: _getBool(map, UserCollection.isShowDiscover, defaultValue: true),
      physicalGender: GenderExtension.enums[map[UserCollection.physicalGender]],
      genderIdentity: GenderExtension.enums[map[UserCollection.genderIdentity]],
      zodiacSign: ZodiacSignExtension.enums[map[UserCollection.zodiacSign]],
      relationshipStatus: RelationshipStatusExtension.enums[map[UserCollection.relationshipStatus]],
      profession: _getString(map, UserCollection.profession),
      education: _getString(map, UserCollection.education),
      greetCount: _getInt(map, UserCollection.greetCount, defaultValue: 0),
      personality: map[UserCollection.personality],
      images: map[UserCollection.images].cast<String>(),
      currentBatchIds: map[UserCollection.currentBatchIds] != null ? map[UserCollection.currentBatchIds].cast<String>() : [],
      age: utils.getAge(timeUtils.getDateTimeFromTimeStamp(map[UserCollection.birthday])!.toLocal()),
      birthday: timeUtils.getDateTimeFromTimeStamp(map[UserCollection.birthday])!.toLocal(),
      lastActiveAt: timeUtils.getDateTimeFromTimeStamp(map[UserCollection.lastActiveAt]) != null ? timeUtils.getDateTimeFromTimeStamp(map[UserCollection.lastActiveAt])!.toLocal() : null,
      updatedAt: timeUtils.getDateTimeFromTimeStamp(map[UserCollection.updatedAt])!.toLocal(),
      createdAt: timeUtils.getDateTimeFromTimeStamp(map[UserCollection.createdAt])!.toLocal(),
    );
  }
}

