class Analysis {
  static const signUp = "sign_up";
  static const signIn = "login";
  static const conflictedQuit = "conflicted_quit";
  static const signInFail = "login_fail";
  static const signOut = "sign_out";
  static const report = "report";
  static const existingMemberButtonClick = "existing_member_click";
  static const newMemberButtonClick = "new_member_click";
  static const deleteAccount = "delete_account";
  static const updateGameId = "update_";
  static const openAppByNotification = "open_app_by_notification";
  static const block = "block";
  static const shareThroughInvite = "share_through_invite";
  static const getLocationBtn = "get_location_button";
  static const fillBDay = "fill_birthday";
}
