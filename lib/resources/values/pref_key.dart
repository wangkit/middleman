class PrefKey {
  static String authPwKey = "passwordKey";
  static String id = "idKey";
  static String email = "emailKey";
  static String hasCalledFCMRegistration = "hasCalledFCMRegistrationKey";
  static String showMessageCount = "showMessageCount";
  static String showNotificationCount = "showNotificationCount";
  static String showLikeAnimation = "showLikeAnimation";
  static String showGreetAnimation = "showGreetAnimation";
  static String hasShownLocationDialog = "hasShownLocationDialog";
  static String profileShadeValue = "profileShadeValue";
}