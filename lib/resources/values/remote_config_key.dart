class RemoteConfigKey {
  static const minimumVersion = "minimum_version";
  static const enforceMinimumVersion = "enforce_minimum_version";
  static const socialSubtitle = "social_meta_tag_subtitle";
  static const socialTitle = "social_meta_tag_title";
  static const socialImage = "social_meta_tag_image";
  static const landingPageQuote = "landing_page_quote";
  static const startBackgroundTopColor = "start_bg_top_color";
  static const startBackgroundBottomColor = "start_bg_bottom_color";
  static const startBackgroundNewMemberButtonColor = "new_member_btn_color";
  static const education = "education";
  static const professions = "professions";
  static const religions = "religions";
  static const country = "country";
}