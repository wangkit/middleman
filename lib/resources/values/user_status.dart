class UserStatus {
  static int normal = 0;
  static int deleted = 1;
  static int banned = 2;
}