class DataKey {
  static const String id = 'id';
  static const String secret = 'secret';
  static const String firstName = 'first_name';
  static const String lastName = 'last_name';
  static const String nickname = 'nickname';
  static const String email = 'email';
  static const String phone = 'phone';
  static const String country = 'country';
  static const String city = 'city';
  static const String longitude = 'longitude';
  static const String password = 'password';
  static const String useGradient = 'useGradient';
  static const String backgroundTop = 'backgroundTop';
  static const String startPageLongButtonColor = 'startPageLongButtonColor';
  static const String backgroundBottom = 'backgroundBottom';
  static const String targetId = 'target_id';
  static const String fcm = 'fcm';
  static const String checkFace = 'checkFace';
  static const String quickDisplayOne = "quick_display_one";
  static const String quickDisplayTwo = "quick_display_two";
  static const String action = 'action';
  static const String versionName = 'versionName';
  static const String value = 'value';
  static const String message = 'message';
  static const String isForce = 'isForce';
  static const String nudeKey = 'nude_key';
  static const String startFrom = 'start_from';
  static const String isDiscover = 'is_discover';
  static const String remainingCount = 'remaining_count';
  static const String thumbnail = 'thumbnail';
  static const String reason = "reason";
  static const String preferences = 'preferences';
  static const String activity = 'activity';
  static const String productId = 'product_id';
  static const String isRefresh = 'is_refresh';
  static const String price = 'price';
  static const String currencyCode = 'currency_code';
  static const String platform = "platform";
  static const String appleStore = 'appleStoreUrl';
  static const String myProfile = 'my_profile';
  static const String location = "location";
  static const String catKey = "cat_key";
  static const String name = "name";
  static const String likeCount = "like_count";
  static const String profiles = "profiles";
  static const String myPreference = 'my_preference';
  static const String myStrictPreference = 'my_strict_preference';
  static const String googleStore = 'googleStoreUrl';
  static const String status = "status";
  static const String instruction = "instruction";
  static const String latitude = 'latitude';
  static const String hasPreference = 'has_preference';
  static const String preference = 'preference';
  static const String profile = 'profile';
  static const String secDiff = 'sec_diff';
  static const String hobbies = 'hobbies';
  static const String gender = 'gender';
  static const String distance = 'distance';
  static const String color = 'color';
  static const String hasProfile = 'has_profile';
  static const String video = "video";
  static const String image = "image";
  static const String otp = 'otp';
  static const String uniqueId = 'uniqueid';
  static const String isVerification = "is_verification";
  static const String birthday = 'birthday';
  static const String countries = 'countries';
  static const String age = 'age';
  static const String schools = 'schools';
  static const String professions = 'professions';
  static const String ethnicities = "ethnicities";
  static const String religions = "religions";
  static const String ethnicity = 'ethnicity';
  static const String religion = 'religion';
  static const String zodiacSign = 'zodiac_sign';
  static const String profession = 'profession';
  static const String birthplace = 'birthplace';
  static const String education = 'education';
  static const String graduatedFrom = 'graduated_from';
  static const String personality = 'personality';
  static const String preferredRelationship = 'preferred_relationship';
  static const String isGreenHead = 'is_green_head';
  static const String hobby = 'hobby';
  static const String deviceId = 'device_id';
  static const String deviceName = 'device_name';
  static const String deviceVersion = 'device_version';
  static const String fcmToken = 'fcm_token';
  static const String tt = "tt";
}