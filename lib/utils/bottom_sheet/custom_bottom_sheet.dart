import 'dart:io';
import 'package:flutter/material.dart';
import 'package:soy_common/widgets/widgets.dart';

void getCustomBottomSheet(context, List<Widget> listOfWidget, {Color? bgColor, bool needTopIndicator = true, Function? onCompleted, bool needOverlay = true, bool fullScreen = false, double radius = 16}) {

  List<Widget> columnChildren;
  if (needTopIndicator) {
    columnChildren = [CommonWidgets.bottomSheetPullIndicator(context)] + listOfWidget;
  } else {
    columnChildren = listOfWidget;
  }

  showModalBottomSheet(
    barrierColor: needOverlay ? null : Colors.white.withOpacity(0),
    backgroundColor: bgColor,
    isScrollControlled: fullScreen,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(radius), topRight: Radius.circular(radius)),
    ),
    context: context,
    builder: (BuildContext ctx) {
      return SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: columnChildren,
        ),
      );
    },
  ).whenComplete(() {
    if (onCompleted != null) {
      onCompleted();
    }
  });
}