import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/configs/dialog_configs.dart';

import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/widgets/button/dialog_botton_row.dart';

Future<void> getBinaryDialog(
    String title,
    String content,
    Function onPressed,
    {
      Function? onDismiss,
      bool barrierDismissible = true,
      String positiveText = "Confirm",
      String negativeText = "Cancel",
      Function? onNegative,
      Color? positiveColor,
    }) async {
  await showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: barrierDismissible,
    ),
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: deviceWidth,
              child: Text(
                content,
                style: DialogConfig.dialogMessageStyle,
              ),
            ),
            DialogButtonRow(
              onOkTap: onPressed,
              onCancelTap: onNegative,
              buttonOkText: positiveText,
              buttonCancelText: negativeText,
              okColor: positiveColor,
              topPadding: 24,
            ),
          ],
        ),
      );
    },
  );
  if (onDismiss != null) {
    onDismiss();
  }
}