import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:soy_common/color/color.dart';

import 'binary_dialog.dart';
import 'loading_dialog.dart';

Future<void> getBlockDialog(UserProfile _thisUser) async {

  /// We only use
  /// 1. id
  /// 2. username
  /// of the given parameter

  await getBinaryDialog("Block ${_thisUser.firstName}", "Are you sure you want to block ${_thisUser.firstName}?", () async {
    Dialogs.showLoadingDialog(title: "Blocking...");
    await apiManager.block(_thisUser);
    apiManager.deleteChat(false, _thisUser.id!);
    apiManager.clearMessageInChat(utils.getChatId([qp.id!, _thisUser.id!]), _thisUser.id!);
    getRoute.pop();
    getRoute.pop();
    utils.toast(TextData.blockedSuccess);
  }, positiveColor: errorColor, positiveText: TextData.block);
}