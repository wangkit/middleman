import 'package:animations/animations.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:flutter/material.dart';

class Dialogs {
  static Future<void> showLoadingDialog({String? title}) async {
    return showModal<void>(
        context: getRoute.getContext(),
        configuration: FadeScaleTransitionConfiguration(
          barrierDismissible: false,
        ),
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(commonRadius),
                  ),
                  backgroundColor: appBgColor,
                  children: <Widget> [
                    Center(
                      child: Column(
                          children: [
                            LoadingWidget(),
                            Container(height: 20,),
                            Text(
                              title ?? TextData.pleaseWait,
                            ),
                          ]
                      ),
                    )
                  ]
              ),
          );
        }
      );
  }
}