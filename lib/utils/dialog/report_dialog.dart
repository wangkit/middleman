import 'package:animations/animations.dart';
import 'package:choicestory/configs/dialog_configs.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/button/dialog_negative_button.dart';
import 'package:choicestory/widgets/button/dialog_positive_button.dart';
import 'package:choicestory/widgets/dialog/custom_alert_dialog.dart';
import 'package:choicestory/widgets/textfield/generic_textfield.dart';
import 'package:flutter/material.dart';
import 'package:soy_common/color/color.dart';

import 'loading_dialog.dart';

getReportDialog(UserProfile profile) {

  TextEditingController _controller = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey();

  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: true,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          TextData.report,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Please briefly tell us what has happened.",
                  style: DialogConfig.dialogMessageStyle,
                ),
                SizedBox(
                  height: 10,
                ),
                GenericTextField(
                  controller: _controller,
                  canEmpty: false,
                  hint: "Reason",
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          DialogNegativeButton(),
          DialogPositiveButton(
            onOkTap: () async {
              if (_formKey.currentState!.validate()) {
                Dialogs.showLoadingDialog(title: "Reporting...");
                await apiManager.report(profile, _controller.text);
                getRoute.pop();
                getRoute.pop();
                utils.toast(TextData.reportedSuccess);
              }
            },
            buttonOkText: TextData.report,
            okColor: errorColor,
          ),
        ],
      );
    },
  );
}