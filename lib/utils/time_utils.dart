import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class TimeUtils {

  /// Used to get current time for a new message sent from this device
  String getCurrentUTCDateTime({bool isWithTrailing = true}) {
    String _utcTime = DateTime.now().toUtc().toString();
    if (isWithTrailing) {
      return _utcTime;
    }
    return _utcTime.split(".").first;
  }

  String getParsedYearMonthDay(DateTime time) {
    DateFormat dateFormat = DateFormat("d MMM yyyy");
    return dateFormat.format(time);
  }

  String getParsedDateTime(DateTime time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    return dateFormat.format(time);
  }

  DateTime getParsedTime(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    return dateFormat.parse(time);
  }

  String getParsedDateTimeLongFormatWithHourMin(DateTime time) {
    DateFormat dateFormat = DateFormat("EEE, d MMM yyyy HH:mm");
    return dateFormat.format(time);
  }

  String getParsedDateTimeLongFormat(DateTime time) {
    DateFormat dateFormat = DateFormat("EEE, d MMM yyyy");
    return dateFormat.format(time);
  }

  DateTime getParsedTimeLongFormat(String time) {
    DateFormat dateFormat = DateFormat("EEE, d MMM yyyy");
    return dateFormat.parse(time);
  }

  String getParsedDateTimeHourMin(DateTime time) {
    DateFormat dateFormat = DateFormat("HH:mm");
    return dateFormat.format(time);
  }

  DateTime getParsedTimeHourMin(String time) {
    DateFormat dateFormat = DateFormat("HH:mm");
    return dateFormat.parse(time);
  }

  String getLocalDateTimeFromUTC(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var offsetTime = dateFormat.parse(time);
    return offsetTime.add(offsetTime.timeZoneOffset).toString().replaceAll(".000", "");
  }

  String convertDateTimeDisplay(DateTime date) {
    final DateFormat serverFormatter = DateFormat('yyyy-MM-dd');
    final String formatted = serverFormatter.format(date);
    return formatted;
  }

  String parseDateTimeInt(int value) {
    if (value < 10) {
      return "0" + value.toString();
    }
    return value.toString();
  }

  String getYearMonthDay(DateTime offsetTime, {bool fromLocalTime = false, String nullValue = "Never"}) {
    if (!fromLocalTime) {
      offsetTime = offsetTime.add(offsetTime.timeZoneOffset);
    }
    int year = offsetTime.year;
    int month = offsetTime.month;
    int day = offsetTime.day;
    String monthString = parseDateTimeInt(month);
    String dayString = parseDateTimeInt(day);
    return year.toString() + "-" + monthString + "-" + dayString;
  }

  String getStringFromTimestamp(Timestamp timestamp) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000, isUtc: true);
    return date.toString().split(".").first;
  }
  
  DateTime? getDateTimeFromTimeStamp(Timestamp? timestamp) {
    if (timestamp == null) {
      return null;
    }
    return DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000, isUtc: true);
  }

  String getHourMinuteFromUTCDateTime(DateTime time) {
    int hour = time.hour;
    int minute = time.minute;
    String hourStr;
    String minuteStr;
    if (hour < 10) {
      hourStr = "0" + hour.toString();
    } else {
      hourStr = hour.toString();
    }
    if (minute < 10) {
      minuteStr = "0" + minute.toString();
    } else {
      minuteStr = minute.toString();
    }
    return hourStr + ":" + minuteStr;
  }

  String convertTimestampToEngFormat(String time) {
    DateFormat dateFormat = DateFormat.yMMMd("en_US");
    DateTime createdAt = DateTime.parse(time);
    var currentUtcTime = dateFormat.format(createdAt);
    return currentUtcTime.toString();
  }

  Duration getTimeDifferenceFromNow(DateTime receivedDateTime) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var currentUtcTime = dateFormat.parse(dateFormat.format(DateTime.now().toUtc()));
    return currentUtcTime.difference(receivedDateTime);
  }

  int calculateDifference(String time) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime date = dateFormat.parse(time);
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
  }

  bool getIsSameDay(String time, String secondTime) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime date = dateFormat.parse(time);
    DateTime dateTwo = dateFormat.parse(secondTime);
    return date.year == dateTwo.year && date.month == dateTwo.month && date.day == dateTwo.day;
  }

  bool isTomorrow(DateTime date, {bool isFromUtc = true}) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays == 1;
  }

  bool isYesterday(DateTime date, {bool isFromUtc = true}) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays == -1;
  }

  bool isToday(DateTime date, {bool isFromUtc = true}) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays == 0;
  }

  String getTimeDiffString(String createdAt) {
    var timeDifferenceString;
    var timeDifference = getTimeDifferenceFromNow(DateTime.parse(createdAt));
    if (timeDifference.inSeconds < 0) {
      timeDifference = Duration(seconds: 1);
    }
    if (timeDifference.inMinutes < 1) {
      timeDifferenceString = timeDifference.inSeconds.toString() + " seconds before";
    } else if (timeDifference.inMinutes > 59) {
      if (timeDifference.inHours > 23) {
        if (timeDifference.inDays > 6) {
          timeDifferenceString = (timeDifference.inDays ~/ 7).toString() + " weeks before";
        } else {
          timeDifferenceString = timeDifference.inDays.toString() + " days before";
        }
      } else {
        timeDifferenceString = timeDifference.inHours.toString() + " hours before";
      }
    } else {
      timeDifferenceString = timeDifference.inMinutes.toString() + " minutes before";
    }
    return timeDifferenceString;
  }

  String getApproximateBirthday(DateTime? birthday) {
    if (birthday == null) {
      return "";
    }
    late String _range;
    if (birthday.day <= 10) {
      _range = "Early";
    } else if (birthday.day <= 20) {
      _range = "Mid";
    } else {
      _range = "Late";
    }
    String _month = DateFormat("MMM").format(birthday).capitalize;

    return "$_range $_month";
  }
}