import 'dart:collection';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:animate_do/animate_do.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:app_settings/app_settings.dart';
import 'package:choicestory/apps/landing/landing_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/remote_config_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:soy_common/color/hex.dart';
import 'package:tinycolor/tinycolor.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';
import 'dialog/binary_dialog.dart';

class Utils {

  /// Quit focus on text field and close keyboard
  void quitFocus({bool createNewFocusToRemoveOld = false}) {
    FocusScopeNode currentFocus = FocusScope.of(getRoute.getContext());
    if (!currentFocus.hasPrimaryFocus) {
      if (createNewFocusToRemoveOld) {
        currentFocus.requestFocus(FocusNode());
      } else {
        currentFocus.unfocus();
      }
    }
    closeKeyboard();
  }

  void logoutClearData() {
    prefs!.remove(PrefKey.email);
    prefs!.remove(PrefKey.id);
    prefs!.remove(PrefKey.authPwKey);
    prefs!.remove(PrefKey.hasCalledFCMRegistration);
    qp = UserProfile(
      id: "",
      email: "",
      authPw: "",
    );
    firebaseAuth.signOut();
    firebaseUser = null;
    getRoute.navigateToAndPopAll(LandingPage());
  }

  String getProfileThumbnail(List<String> imageList) {
    if (imageList.isEmpty) return defaultProfilePictureLink;
    return imageList.first;
  }

  bool isDarkColor(String color) {
    return ThemeData.estimateBrightnessForColor(utils.getColorByHex(color)) == Brightness.dark;
  }

  Color getCorrectContrastColor(String color) {
    return getDefaultWhiteOrBlackColor(isDarkColor(color));
  }

  Color getDefaultWhiteOrBlackColor(bool isWhite) {
    return isWhite ? Colors.white : Colors.black;
  }

  String getChatId(List<String> listOfIds) {
    /// Chat id must always remain the same when:
    /// sender is A, receiver is B
    /// and
    /// receiver is A, sender is B
    /// Using integer parsed from the uuid as id is a way to make sure the id will always remain the same
    /// whenever the conversation involves A and B

    assert(listOfIds.length > 1, "List of ids must have at least two ids in order to form a chat and get the chat id");
    String chatId = "";
    SplayTreeMap<int, String> mapOfIdAndSum = SplayTreeMap();
    listOfIds.forEach((id) {
      int sum = 0;
      Uuid.parse(id).forEach((value) {
        if (value.isEven) {
          sum += value;
        } else {
          sum -= value;
        }
      });
      mapOfIdAndSum[sum] = id;
    });
    var newMap = Map.fromEntries(mapOfIdAndSum.entries.toList()..sort((e1, e2) =>
        e1.key.compareTo(e2.key)));
    newMap.values.toList().forEach((id) {
      chatId += id;
    });
    return chatId;
  }

  double getProfileMediaHeight() {
    return deviceHeight - kToolbarHeight - kToolbarHeight - (deviceHeight * 0.13);
  }

  fieldFocusChange(FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(getRoute.getContext()).requestFocus(nextFocus);
  }

  String getHexCodeByColor(Color color) {
    return color.value.toRadixString(16);
  }

  void closeApp() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  Color getVisibleColor(String colorCode) {
    return getColorByHex(colorCode).withRed(getColorByHex(colorCode).red ~/ 2).withBlue(getColorByHex(colorCode).blue ~/ 2).withGreen(getColorByHex(colorCode).green ~/ 2);
  }

  /// Close the keyboard.
  void closeKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  /// Open the keyboard.
  void openKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.show');
  }

  Color getColorByHex(String? colorCode, {Color? nullColor}) {
    if (colorCode!.isNullOrEmpty) {
      return nullColor == null ? unfocusedColor : nullColor;
    } else {
      return HexColor(colorCode);
    }
  }

  DynamicLinkParameters _getDynamicLink(String link) {
    return DynamicLinkParameters(
      uriPrefix: 'https://spinnerofyarns.page.link',
      link: Uri.parse(link),
      androidParameters: AndroidParameters(
        packageName: 'com.soy.storyteller',
        minimumVersion: 1,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.soy.storyteller',
        minimumVersion: '1.0.0',
        appStoreId: '1562804586',
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        imageUrl: Uri.parse(remoteConfig!.getValue(RemoteConfigKey.socialImage).asString()),
        title: remoteConfig!.getValue(RemoteConfigKey.socialTitle).asString(),
        description: remoteConfig!.getValue(RemoteConfigKey.socialSubtitle).asString(),
      ),
    );
  }

  Future<void> shareShortUrl(String link) async {
    final ShortDynamicLink dynamicUrl = await _getDynamicLink(link).buildShortLink();
    await Share.share(dynamicUrl.shortUrl.toString());
  }

  void copyToClipboard(String target) {
    Clipboard.setData(ClipboardData(text: target));
  }

  Future<void> launchURL(String url, {String? fallbackUrl, bool isHttp = true}) async {
    if (fallbackUrl == null) fallbackUrl = url;
    if (!url.contains("http") && !url.contains("mailto") && isHttp) {
      url = "https://" + url;
    }
    try {
      await canLaunch(url) ? launch(url, forceSafariVC: false) : launch(fallbackUrl, forceSafariVC: false);
    } on Exception catch(e) {
      toast(TextData.cantOpenUrl);
    }
  }

  String generatePassword({bool isWithLetters = true, bool isWithUppercase = true, bool isWithNumbers = true, bool isWithSpecial = false, double numberCharPassword = 14}) {

    //Define the allowed chars to use in the password
    String _lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
    String _upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String _numbers = "0123456789";
    String _special = "@#=+!£\$%&?[](){}";

    //Create the empty string that will contain the allowed chars
    String _allowedChars = "";

    //Put chars on the allowed ones based on the input values
    _allowedChars += (isWithLetters ? _lowerCaseLetters : '');
    _allowedChars += (isWithUppercase ? _upperCaseLetters : '');
    _allowedChars += (isWithNumbers ? _numbers : '');
    _allowedChars += (isWithSpecial ? _special : '');

    int i = 0;
    String _result = "";

    //Create password
    while (i < numberCharPassword.round()) {
      //Get random int
      int randomInt = Random.secure().nextInt(_allowedChars.length);
      //Get random char and append it to the password
      _result += _allowedChars[randomInt];
      i++;
    }

    return _result;
  }

  void addEmoji(TextEditingController controller, String emoji) {
    int offsetStart;
    int originalOffsetStart;
    String currentText;
    Runes sRunes;
    int emojiCount;
    var beforeInsertionText;
    var beforeInsertionRunes;
    int emojiLength = emoji.length;
    try {
      /// Sometimes, some weird emoji that can't be understood is passed into this function as an argument
      /// To filter these unwanted emojis, make sure the length of the emoji string is 2
      if (emojiLength == 2) {
        if (controller.selection.start < 0) {
          controller.text = controller.text + emoji;
        } else {
          /// Need to turn string into runes since emoji will crash app
          /// Calculate number of emoji in text before insertion point first
          /// then, since each emoji's length is 2 instead of 1 of normal character
          /// Deduct the offset by the number of emoji to get the correct offset of runes
          /// Emoji utf16 code are all larger than 125000, but i am not sure if there are any normal characters beyond 125000, so this might be a problem
          emojiCount = 0;
          offsetStart = controller.selection.start;
          originalOffsetStart = offsetStart;
          currentText = controller.text;
          sRunes = currentText.runes;
          beforeInsertionText = currentText.substring(0, offsetStart);
          beforeInsertionRunes = beforeInsertionText.runes;
          beforeInsertionRunes.forEach((int rune) {
            if (rune > 125000 && rune < 130000) {
              emojiCount++;
            }
          });
          offsetStart = offsetStart - emojiCount;
          controller.text = String.fromCharCodes(sRunes, 0, offsetStart)
              + emoji + String.fromCharCodes(sRunes, offsetStart, sRunes.length);
          if (Platform.isAndroid) {
            controller.value = controller.value.copyWith(
              text: controller.text,
              selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
              composing: TextRange.empty,
            );
          } else {
            /// This delay is needed for ios to allow jumping of cursor in text field
            /// Any milliseconds smaller than 30 will cause unstable performance
            Future.delayed(Duration(milliseconds: 30), () {
              controller.value = controller.value.copyWith(
                text: controller.text,
                selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
                composing: TextRange.empty,
              );
            });
          }
        }
      }
    } on Exception catch (e) {
      print("Add emoji error: $e");
    }
  }

  void emailToSpinnerOfYarns({String subject = "Contacting Spinner of Yarns", String? body}) async {
    if (Platform.isAndroid) {
      final emailUrl = body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body";
      await launchURL(emailUrl);
    } else if (Platform.isIOS) {
      final emailUrl = Uri.encodeFull(body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body");
      await launchURL(emailUrl);
    } else {
      toast(TextData.notSupportedOs, isWarning: true);
    }
  }

  Future<void> openFacebook(String fbId) async {
    var fallbackUrl = "https://www.facebook.com/$fbId";
    var fbProtocolUrl = Platform.isAndroid ? "fb.me/$fbId" : Platform.isIOS ? fallbackUrl : fallbackUrl;
    await launchURL(fbProtocolUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openTwitter(String twitterId) async {
    var fallbackUrl = "https://twitter.com/$twitterId";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openYoutube() async {
    var fallbackUrl = "https://www.youtube.com/channel/UCdGAjAYzhEtB6wuawH0dvaA";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }


  String getRandomString() {
    const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();
    return String.fromCharCodes(Iterable.generate(102, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  ZodiacSign getZodiacSign(DateTime date)  {
    var days = date.day;
    var months = date.month;
    if (months == 1) {
      if (days >= 21) {
        return ZodiacSign.aquarius;
      } else {
        return ZodiacSign.capricorn;
      }
    } else if (months == 2) {
      if (days >= 20) {
        return ZodiacSign.pisces;
      }else {
        return ZodiacSign.aquarius;
      }
    } else if (months == 3) {
      if (days >= 21) {
        return ZodiacSign.aries;
      } else {
        return ZodiacSign.pisces;
      }
    } else if (months == 4) {
      if (days >= 21) {
        return ZodiacSign.taurus;
      } else {
        return ZodiacSign.aries;
      }
    } else if (months == 5) {
      if (days >= 22) {
        return ZodiacSign.gemini;
      } else {
        return ZodiacSign.taurus;
      }
    } else if (months == 6) {
      if (days >= 22) {
        return ZodiacSign.cancer;
      } else {
        return ZodiacSign.gemini;
      }
    } else if (months == 7) {
      if (days >= 23) {
        return ZodiacSign.leo;
      } else {
        return ZodiacSign.cancer;
      }
    } else if (months == 8) {
      if (days >= 23) {
        return ZodiacSign.virgo;
      } else {
        return ZodiacSign.leo;
      }
    } else if (months == 9) {
      if (days >= 24) {
        return ZodiacSign.libra;
      } else {
        return ZodiacSign.virgo;
      }
    } else if (months == 10) {
      if (days >= 24) {
        return ZodiacSign.scorpio;
      } else {
        return ZodiacSign.libra;
      }
    } else if (months == 11) {
      if (days >= 23) {
        return ZodiacSign.sagittarius;
      } else {
        return ZodiacSign.scorpio;
      }
    }
    return ZodiacSign.sagittarius;
  }

  int calculateDistance(lat1, lon1, lat2, lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return (12742 * asin(sqrt(a))).toInt();
  }

  int getAge(DateTime birthday) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthday.year;
    int month1 = currentDate.month;
    int month2 = birthday.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthday.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  bool isAdult(DateTime bDay) {
    DateTime today = DateTime.now();
    DateTime adultDate = DateTime(
      bDay.year + AppConfig.minAge,
      bDay.month,
      bDay.day,
    );
    return adultDate.isBefore(today);
  }

  Future<void> loadPref() async {
    hasCalledFCMRegistration = prefs?.getBool(PrefKey.hasCalledFCMRegistration) ?? false;
    hasShownLocationDialog = prefs?.getBool(PrefKey.hasShownLocationDialog) ?? true;
    showMessageCount = prefs?.getBool(PrefKey.showMessageCount) ?? true;
    showGreetAnimation = prefs?.getBool(PrefKey.showGreetAnimation) ?? true;
    showLikeAnimation = prefs?.getBool(PrefKey.showLikeAnimation) ?? true;
    showNotificationCount = prefs?.getBool(PrefKey.showNotificationCount) ?? true;
    profileShadeValue = prefs?.getDouble(PrefKey.profileShadeValue) ?? 0.8;
    qp.id = prefs?.getString(PrefKey.id) ?? "";
    qp.email = prefs?.getString(PrefKey.email) ?? "";
    qp.authPw = prefs?.getString(PrefKey.authPwKey) ?? "";
    if (qp.id!.isNullOrEmpty || qp.email!.isNullOrEmpty || qp.authPw!.isNullOrEmpty) {
      prefs?.remove(PrefKey.id);
      prefs?.remove(PrefKey.email);
      prefs?.remove(PrefKey.authPwKey);
      qp = UserProfile(
        id: "",
        email: "",
        authPw: "",
      );
    }
  }

  String shortStringForLongInt(int? value) {
    if (value == null) {
      return "0";
    }
    if (value < 0) {
      return "0";
    }
    if (value > 999) {
      var _formattedNumber = NumberFormat.compactCurrency(
        symbol: "",
        decimalDigits: 1,
      ).format(value);
      return _formattedNumber.toLowerCase();
    } else {
      return value.toString();
    }
  }

  String determineNeedS(int number, String text, {bool showNumber = true}) {
    if (number > 1) {
      if (showNumber) {
        return shortStringForLongInt(number) + " " + text + "s";
      } else {
        return text + "s";
      }
    } else {
      if (showNumber) {
        return shortStringForLongInt(number) + " " + text;
      } else {
        return text;
      }
    }
  }

  Future<File> uintToFile(Uint8List data) async {
    final tempDir = await getTemporaryDirectory();
    final _tempFile = await File('${tempDir.path}/${Uuid().v4()}.jpg').create();
    _tempFile.writeAsBytesSync(data);
    return _tempFile;
  }

  String getFullName(UserProfile profile) {
    return profile.firstName! + " " + profile.lastName!;
  }

  bool hasLoggedIn() {
    return qp.id != null && qp.id!.isNotEmpty && firebaseUser != null && qp.email!.isNotEmpty && qp.authPw!.isNotEmpty;
  }

  Color convertStrToColor(String text, {bool needOpacity = true, bool isDarken = false}){
    var hash = 0;
    for (var i = 0; i < text.length; i++) {
      hash = text.codeUnitAt(i)+ ((hash << 5) - hash);
    }
    final finalHash = hash.abs() % (256*256*256);
    final red = ((finalHash & 0xFF0000) >> 16);
    final blue = ((finalHash & 0xFF00) >> 8);
    final green = ((finalHash & 0xFF));
    final color = Color.fromRGBO(red, green, blue, 1);
    if (isDarken) {
      if (TinyColor(color).darken(35).color == Colors.black) {
        return TinyColor(color).color.withOpacity(needOpacity ? 0.5 : 1);
      }
      return TinyColor(color).darken(35).color.withOpacity(needOpacity ? 0.8 : 1);
    } else {
      if (TinyColor(color).lighten(35).color == Colors.white) {
        return TinyColor(color).color.withOpacity(needOpacity ? 0.5 : 1);
      }
      return TinyColor(color).lighten(35).color.withOpacity(needOpacity ? 0.8 : 1);
    }
  }

  toast(String msg, {int duration = 3, String? title, bool isDismissible = true, bool isWarning = false, bool blockBackgroundInteraction = false}) {
    if (kIsWeb) {
      Get.rawSnackbar(
        message: msg,
        leftBarIndicatorColor: isWarning ? warning : success,
        isDismissible: isDismissible,
        reverseAnimationCurve: Curves.fastOutSlowIn,
      );
    } else {
      Flushbar(
        title: title,
        message: msg,
        blockBackgroundInteraction: blockBackgroundInteraction,
        routeBlur: blockBackgroundInteraction ? 3 : 0,
        duration: Duration(seconds: duration),
        reverseAnimationCurve: Curves.fastOutSlowIn,
        flushbarStyle: FlushbarStyle.FLOATING,
        margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
        borderRadius: BorderRadius.circular(8),
        flushbarPosition: FlushbarPosition.BOTTOM,
        leftBarIndicatorColor: isWarning ? warning : success,
        isDismissible: isDismissible,
      )..show(getRoute.getContext());
    }
  }

  saveStr(String key, String value) {
    prefs!.setString(key, value);
  }

  saveStrList(String key, List<String> value) {
    prefs!.setStringList(key, value);
  }

  saveInt(String key, int value) {
    prefs!.setInt(key, value);
  }

  saveBoo(String key, bool value) {
    prefs!.setBool(key, value);
  }

  saveDouble(String key, double value) {
    prefs!.setDouble(key, value);
  }
}
