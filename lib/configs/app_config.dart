class AppConfig {
  static const listAnimationDuration = const Duration(milliseconds: 375);
  static const slideAnimationVerticalOffset = 50.0;
  static const minAge = 18;
  static const maxAge = 100;
  static const allowedPicturePerProfile = 6;
  static const minDistance = 10.0;
  static const maxDistance = 600.0;
  static const minShade = 0.0;
  static const maxShade = 0.9;
  static const newChatRoomMessage = "You two have liked each other!🎉🎉🎉";
  static const deletedMessage = "This message is deleted";
  static const distanceSuffix = "km";
  static const numberOfMessagesPerPage = 100;
  static const batchInterval = 12;
  static const getPotentialCount = 10;
  static const getDiscoverCount = 20;
  static const personalityMaxLength = 200;
}