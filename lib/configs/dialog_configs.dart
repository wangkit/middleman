import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/constants/const.dart';

class DialogConfig {
  static double dialogCornerRadius = 12.0;
  static RoundedRectangleBorder dialogShape({double? radius}) {
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(dialogCornerRadius),
    );
  }
  static double dialogTitleFontSize = 19.0;
  static FontWeight dialogTitleFontWeight = FontWeight.w500;
  static ModalConfiguration getTransition({bool isBarrierDismissible = true}) {
    return FadeScaleTransitionConfiguration(
      barrierDismissible: isBarrierDismissible,
    );
  }
  static TextStyle dialogTitleStyle = TextStyle(
    color: mainColor,
    fontSize: DialogConfig.dialogTitleFontSize,
    fontWeight: DialogConfig.dialogTitleFontWeight,
  );
  static TextStyle dialogMessageStyle = TextStyle(
    color: mainColor,
  );
  static TextStyle dialogButtonStyle = TextStyle(
    color: Colors.black,
  );
  static Color dialogBackgroundColor = appBgColor;
}