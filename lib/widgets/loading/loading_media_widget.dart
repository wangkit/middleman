import 'package:animate_do/animate_do.dart';
import 'package:choicestory/constants/const.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingMediaWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.all(0.0),
      child: SpinKitThreeBounce(
        color: mainColor,
      ),
    );
  }
}
