import 'package:choicestory/constants/const.dart';
import 'package:flutter/cupertino.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:flutter/material.dart';

class GenericStringDropDownField extends StatefulWidget {
  final Function onChanged;
  final String text;
  final String value;
  final List list;
  final double? width;
  final bool capitalize;
  final bool canEmpty;

  GenericStringDropDownField({
    Key? key,
    required this.onChanged,
    required this.text,
    required this.value,
    required this.list,
    this.capitalize = true,
    this.canEmpty = true,
    this.width,
  }) : super(key: key);

  @override
  GenericStringDropDownFieldState createState() => GenericStringDropDownFieldState();
}

class GenericStringDropDownFieldState extends State<GenericStringDropDownField> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
      child: Container(
        width: widget.width ?? deviceWidth * 0.9,
        child: DropdownButtonFormField<String?>(
          decoration: InputDecoration(
            labelText: widget.text,
            labelStyle: TextStyle(
              color: mainColor,
            ),
            alignLabelWithHint: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(commonRadius),
              borderSide: BorderSide(
                color: mainColor,
              ),
            ),
          ),
          value: widget.value,
          validator: !widget.canEmpty ? (String? value) {
            if (value!.isNullOrEmpty) {
              return "${widget.text} must not be empty";
            }
            return null;
          } : null,
          isDense: true,
          dropdownColor: appBgColor,
          onChanged: (var value) {
            widget.onChanged(value);
          },
          items: widget.list.map((value) {
            return DropdownMenuItem<String?>(
              value: value,
              child: Text(
                widget.capitalize ? value.toString().capitalize : value,
                style: TextStyle(
                  color: mainColor,
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}