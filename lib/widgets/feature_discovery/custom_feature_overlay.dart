import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/models/env.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';

class CustomFeatureOverlay extends StatefulWidget {

  final Widget child;
  final String id;
  final String? title;
  final String? description;
  final Color backgroundColor;

  CustomFeatureOverlay({
    Key? key,
    required this.child,
    required this.id,
    required this.backgroundColor,
    this.title,
    this.description,
  }) : super(key: key);

  @override
  _CustomFeatureOverlayState createState() => _CustomFeatureOverlayState();
}

class _CustomFeatureOverlayState extends State<CustomFeatureOverlay> {

  @override
  Widget build(BuildContext context) {

    return DescribedFeatureOverlay(
      featureId: widget.id,
      backgroundDismissible: false,
      barrierDismissible: false,
      textColor: Colors.white,
      backgroundOpacity: 0.9,
      backgroundColor: widget.backgroundColor,
      tapTarget: IgnorePointer(
        ignoring: true,
        child: Icon(
          MdiIcons.handOkay,
        ),
      ),
      title: widget.title == null ? null : Text(
        widget.title!,
      ),
      description: widget.description == null ? null : Text(
        widget.description!,
      ),
      child: widget.child,
    );
  }
}
