import 'dart:convert';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:soy_common/color/color.dart';
import 'package:uuid/uuid.dart';

class ChatInputBar extends StatefulWidget {
  
  final UserProfile targetProfile;
  final bool isGreet;
  final Function? onMessage;

  ChatInputBar({
    Key? key,
    required this.targetProfile,
    this.isGreet = false,
    this.onMessage,
  }) : super(key: key);

  @override
  _ChatInputBarState createState() => _ChatInputBarState();
}

class _ChatInputBarState extends State<ChatInputBar> {

  late TextEditingController _inputController;
  late FocusNode _inputFocusNode;

  @override
  void initState() {
    _inputController = TextEditingController();
    _inputFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _inputController.dispose();
    _inputFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.isGreet ? EdgeInsets.symmetric(vertical: 8, horizontal: 12) : EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Material(
              borderRadius: BorderRadius.circular(20.0),
              color: unfocusedColor,
              child: Row(
                children: <Widget>[
                  Container(width: 8.0),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(4.0),
                      child: TextField(
                        focusNode: _inputFocusNode,
                        maxLines: null,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.multiline,
                        textInputAction: TextInputAction.newline,
                        controller: _inputController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          counterText: null,
                          hintText: TextData.message,
                          hintStyle: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: FloatingActionButton(
              key: UniqueKey(),
              heroTag: UniqueKey(),
              elevation: 0,
              backgroundColor: widget.isGreet ? utils.convertStrToColor(widget.targetProfile.id!) : appIconColor,
              onPressed: () {
                if (_inputController.text.isNotEmpty) {
                  DateTime _now = DateTime.now();
                  String _messageId = Uuid().v4();
                  String messageText = _inputController.text;
                  _inputController.clear();
                  /// Create my chat item if not exists and if not greeting
                  if (!widget.isGreet) {
                    firestore.collection(FirestoreCollections.chats).where(ChatCollection.userId, isEqualTo: qp.id).where(ChatCollection.targetId, isEqualTo: widget.targetProfile.id).get().then((result) {
                      if (result.size == 0) {
                        firestore.collection(FirestoreCollections.chats).doc(qp.id! + widget.targetProfile.id!).set({
                          ChatCollection.userId: qp.id,
                          ChatCollection.createdAt: _now,
                          ChatCollection.targetFirstName: widget.targetProfile.firstName,
                          ChatCollection.targetLastName: widget.targetProfile.lastName,
                          ChatCollection.hasUnread: false,
                          ChatCollection.isLastMessageMyself: true,
                          ChatCollection.lastMessageId: _messageId,
                          ChatCollection.lastMessage: messageText,
                          ChatCollection.targetImages: widget.targetProfile.images,
                          ChatCollection.targetId: widget.targetProfile.id,
                          ChatCollection.updatedAt: _now,
                        });
                      } else if (result.docs.isNotEmpty) {
                        /// Update chat table's last message and updated at
                        firestore.collection(FirestoreCollections.chats).doc(result.docs.first.id).update({
                          ChatCollection.updatedAt: _now,
                          ChatCollection.lastMessage: messageText,
                          ChatCollection.isLastMessageMyself: true,
                          ChatCollection.lastMessageId: _messageId,
                          ChatCollection.targetFirstName: widget.targetProfile.firstName,
                          ChatCollection.targetLastName: widget.targetProfile.lastName,
                          ChatCollection.targetImages: widget.targetProfile.images,
                        });
                      }
                    });
                  }
                  /// Create chat item for target if not exists
                  firestore.collection(FirestoreCollections.chats).where(ChatCollection.userId, isEqualTo: widget.targetProfile.id).where(ChatCollection.targetId, isEqualTo: qp.id).get().then((result) {
                    if (result.size == 0) {
                      firestore.collection(FirestoreCollections.chats).doc(widget.targetProfile.id! + qp.id!).set({
                        ChatCollection.userId: widget.targetProfile.id,
                        ChatCollection.createdAt: _now,
                        ChatCollection.targetFirstName: qp.firstName,
                        ChatCollection.targetLastName: qp.lastName,
                        ChatCollection.hasUnread: true,
                        /// is last message myself here means is last message by target
                        ChatCollection.isLastMessageMyself: false,
                        ChatCollection.lastMessage: messageText,
                        ChatCollection.targetImages: qp.images,
                        ChatCollection.lastMessageId: _messageId,
                        ChatCollection.targetId: qp.id,
                        ChatCollection.updatedAt: _now,
                      });
                    } else if (result.docs.isNotEmpty) {
                      /// Update chat table's last message and updated at and hasUnread to notify target that there is a new message
                      firestore.collection(FirestoreCollections.chats).doc(result.docs.first.id).update({
                        ChatCollection.updatedAt: _now,
                        ChatCollection.lastMessage: messageText,
                        ChatCollection.hasUnread: true,
                        ChatCollection.isLastMessageMyself: false,
                        ChatCollection.lastMessageId: _messageId,
                        ChatCollection.targetFirstName: qp.firstName,
                        ChatCollection.targetLastName: qp.lastName,
                        ChatCollection.targetImages: qp.images,
                      });
                    }
                  });
                  /// Send message
                  firestore.collection(FirestoreCollections.messages).doc(_messageId).set({
                      MessageCollection.chatId: utils.getChatId([qp.id!, widget.targetProfile.id!]),
                      MessageCollection.messageId: _messageId,
                      MessageCollection.message: messageText,
                      MessageCollection.senderId: qp.id,
                      MessageCollection.receiverId: widget.targetProfile.id,
                      MessageCollection.createdAt: _now,
                      MessageCollection.unreadPartyId: [widget.targetProfile.id],
                    },
                  );
                  if (widget.isGreet) {
                    apiManager.greet(widget.targetProfile.id!);
                  }
                  if (widget.onMessage != null) {
                    widget.onMessage!();
                  }
                }
              },
              child: Icon(
                Icons.send,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}