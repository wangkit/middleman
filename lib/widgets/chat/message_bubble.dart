import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:soy_common/color/color.dart';

class MessageBubble extends StatefulWidget {
  final String message;
  final DateTime createdAt;
  final String messageId;
  final String senderName;
  final String senderId;
  bool isSelected;
  final String receiverId;
  final Function notifyParent;
  final bool isMyself;
  final String receiverName;
  List<String> selectedMessages;

  MessageBubble({
    Key? key,
    required this.message,
    required this.createdAt,
    required this.receiverName,
    required this.notifyParent,
    required this.isMyself,
    required this.messageId,
    required this.senderName,
    required this.receiverId,
    required this.isSelected,
    required this.senderId,
    required this.selectedMessages,
  });

  @override
  _MessageBubbleState createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  late bool isMyself;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    isMyself = widget.senderId == qp.id!;

    return InkWell(
      onLongPress: !isMyself ? null : () {
        widget.isSelected = true;
        widget.selectedMessages.add(widget.messageId);
        widget.notifyParent();
      },
      onTap: !isMyself ? null : () {
        if (widget.isSelected) {
          widget.isSelected = false;
          widget.selectedMessages.remove(widget.messageId);
          widget.notifyParent();
        } else if (widget.selectedMessages.length > 0) {
          widget.isSelected = true;
          widget.selectedMessages.add(widget.messageId);
          widget.notifyParent();
        }
      },
      child: AnimatedContainer(
        duration: kThemeAnimationDuration,
        decoration: BoxDecoration(
          color: widget.isSelected ? unfocusedColor : transparent,
          borderRadius: BorderRadius.circular(commonRadius),
        ),
        child: Bubble(
          margin: BubbleEdges.only(top: 4, bottom: 4, right: 2, left: 2),
          alignment: isMyself ? Alignment.centerRight : Alignment.centerLeft,
          nip: isMyself ? BubbleNip.rightBottom : BubbleNip.leftTop,
          shadowColor: mainColor.withOpacity(0.35),
          elevation: 1 * MediaQuery.of(context).devicePixelRatio,
          radius: Radius.circular(commonRadius),
          color: isMyself ? unfocusedColor : utils.convertStrToColor(widget.senderId, needOpacity: false, isDarken: true),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(width: 4,),
              !isMyself ? Flexible(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Text(
                        userTag + widget.senderName,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        widget.message,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ) : Flexible(
                child: Text(
                  widget.message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, top: 10.0),
                child: Text(
                  timeUtils.getHourMinuteFromUTCDateTime(widget.createdAt),
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}