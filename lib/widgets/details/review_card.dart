import 'package:animate_do/animate_do.dart';
import 'package:choicestory/constants/const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReviewCard extends StatefulWidget {
  final List<Widget> children;

  ReviewCard({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  ReviewCardState createState() => ReviewCardState();
}

class ReviewCardState extends State<ReviewCard> {

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        width: deviceWidth * 1,
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 10),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(commonRadius / 2),
          ),
          elevation: 1,
          child: Column(
            children: widget.children,
          ),
        ),
      ),
    );
  }
}