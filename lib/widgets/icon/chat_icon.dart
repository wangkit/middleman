import 'package:animate_do/animate_do.dart';
import 'package:badges/badges.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

import 'gradient_icon.dart';

class ChatIcon extends StatefulWidget {

  bool isSelected;
  double iconSize;

  ChatIcon({
    Key? key,
    this.isSelected = false,
    required this.iconSize,
  }) : super(key: key);

  @override
  ChatIconState createState() => ChatIconState();
}

class ChatIconState extends State<ChatIcon> {

  int count = 0;
  late Stream<QuerySnapshot> _stream;

  @override
  void initState() {
    _stream = firestore.collection(FirestoreCollections.messages)
        .where(MessageCollection.receiverId, isEqualTo: qp.id!)
        .where(MessageCollection.unreadPartyId, arrayContains: qp.id!)
        .snapshots();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> clearUnread() async {
    if (count > 0) {
      await apiManager.clearUnreadChat();
      count = 0;
      refreshThisPage();
    }
  }

  Widget _icon() {
    return GradientIcon(
      Icons.chat_bubble,
      widget.iconSize,
      LinearGradient(
        colors: widget.isSelected ? [
        turquoise,
        lightTurquoise,
        ] : [unfocusedColor, unfocusedColor],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return !showMessageCount ? _icon() : StreamBuilder<QuerySnapshot>(
      stream: _stream,
      builder: (ctx, snapshot) {
        if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
          count = snapshot.data!.docs.length;
        }
        return Badge(
          position: BadgePosition(
            top: -12,
            start: 10,
            end: -4,
          ),
          badgeColor: count > 0 ? appIconColor : unfocusedColor,
          badgeContent: Center(
            child: Text(
              count.toString(),
            ),
          ),
          child: _icon(),
        );
      },
    );
  }
}
