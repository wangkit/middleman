import 'dart:math';
import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/constants/const.dart';
import 'package:confetti/confetti.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/button/custom_floating_action_button.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/chat/chat_input_bar.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:choicestory/widgets/image/profile_media.dart';
import 'package:choicestory/widgets/textfield/generic_textfield.dart';
import 'package:flip_card/flip_card.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';

class DiscoverBubble extends StatefulWidget {

  final UserProfile profile;

  DiscoverBubble({
    Key? key,
    required this.profile,
  });

  @override
  _DiscoverBubbleState createState() => _DiscoverBubbleState();
}

class _DiscoverBubbleState extends State<DiscoverBubble> with AutomaticKeepAliveClientMixin<DiscoverBubble>, TickerProviderStateMixin {

  @override
  bool get wantKeepAlive => true;

  double _height = deviceHeight * 0.65;
  late Color _thisColor;
  static const double _widthPadding = 4;
  GlobalKey<ProfileMediaState> _mediaKey = GlobalKey();
  late ValueNotifier<int> _pictureIndex;
  final double _borderWidth = 1.5;
  late TextEditingController _controller;
  late Widget _greetWidget;
  late ConfettiController _confettiController;

  @override
  void initState() {
    _confettiController = ConfettiController(
      duration: Duration(seconds: 10),
    );
    _controller = TextEditingController();
    _pictureIndex = ValueNotifier<int>(0);
    _thisColor = utils.convertStrToColor(widget.profile.id!);
    _greetWidget = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        FloatingActionButton(
          heroTag: UniqueKey().toString(),
          mini: true,
          tooltip: TextData.previous,
          backgroundColor: Colors.black,
          shape: CircleBorder(
            side: BorderSide(
              color: Colors.white,
              width: _borderWidth,
            ),
          ),
          child: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.white,
          ),
          onPressed: () {
            _mediaKey.currentState!.previousPage();
          },
        ),
        InkWell(
          borderRadius: BorderRadius.circular(commonRadius),
          onTap: () {
            if (!likeOrPassOrGreetedIds.contains(widget.profile.id!)) {
              _greetWidget = GestureDetector(
                child: ChatInputBar(
                  targetProfile: widget.profile,
                  onMessage: () {
                    _greetWidget = Container(
                      width: deviceWidth * 0.4,
                      height: 45,
                      decoration: BoxDecoration(
                        color: _thisColor,
                        borderRadius: BorderRadius.circular(commonRadius),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            TextData.greeted,
                            style: TextStyle(
                              fontSize: 18,
                              color: utils.isDarkColor(utils.getHexCodeByColor(_thisColor)) ? Colors.white : Colors.black,
                            ),
                          ),
                        ],
                      ),
                    );
                    refreshThisPage();
                    if (showGreetAnimation) {
                      _confettiController.play();
                    }
                  },
                  isGreet: true,
                ),
              );
              refreshThisPage();
            }
          },
          child: Container(
            width: deviceWidth * 0.4,
            height: 45,
            decoration: BoxDecoration(
              color: _thisColor,
              borderRadius: BorderRadius.circular(commonRadius),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  TextData.greet,
                  style: TextStyle(
                    fontSize: 18,
                    color: utils.isDarkColor(utils.getHexCodeByColor(_thisColor)) ? Colors.white : Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
        FloatingActionButton(
          heroTag: UniqueKey().toString(),
          mini: true,
          tooltip: TextData.next,
          backgroundColor: Colors.black,
          shape: CircleBorder(
            side: BorderSide(
              color: Colors.white,
              width: _borderWidth,
            ),
          ),
          child: Icon(
            Icons.arrow_forward_ios_outlined,
            color: Colors.white,
          ),
          onPressed: () {
            _mediaKey.currentState!.nextPage();
          },
        ),
      ],
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _confettiController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void changePictureIndex(int newIndex) {
    _pictureIndex.value = newIndex;
  }

  /// A custom Path to paint stars.
  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 5;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return likeOrPassOrGreetedIds.contains(widget.profile.id!) ? CommonWidgets.emptyBox() : Stack(
      children: [
        FadeIn(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Container(
              height: _height + deviceHeight * 0.08,
              width: deviceWidth,
              child: Card(
                elevation: 4,
                color: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(commonRadius),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 30,
                      child: Stack(
                        children: [
                          ShaderMask(
                            shaderCallback: (rect) {
                              return LinearGradient(
                                begin: Alignment.center,
                                end: Alignment.bottomCenter,
                                colors: [Colors.black, Colors.transparent],
                              ).createShader(Rect.fromLTRB(0, rect.height * 0.8, rect.width, rect.height));
                            },
                            blendMode: BlendMode.dstIn,
                            child: ProfileMedia(
                              key: _mediaKey,
                              height: _height,
                              userProfile: widget.profile,
                              shouldAutoPlay: true,
                              changePictureIndex: changePictureIndex,
                            ),
                          ),
                          Positioned.fill(
                            right: 10,
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 16, bottom: 40),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                      child: Row(
                                        children: [
                                          Text(
                                            widget.profile.firstName!,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 24
                                            ),
                                          ),
                                          SizedBox(width: 8),
                                          Text(
                                            "${widget.profile.age}",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 24
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 8,),
                                    Flexible(
                                      child: Text(
                                        widget.profile.personality!,
                                        maxLines: 5,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 19
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 4),
                    AnimatedSwitcher(
                      duration: kThemeAnimationDuration,
                      transitionBuilder: (Widget child, Animation<double> animation) {
                        return FadeTransition(child: child, opacity: animation);
                      },
                      child: _greetWidget,
                    ),
                    SizedBox(height: 12),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: List.generate(widget.profile.images!.length, (index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                child: ValueListenableBuilder(
                                  valueListenable: _pictureIndex,
                                  builder: (ctx, value, child) {
                                    return Container(
                                      width: (deviceWidth - (_widthPadding * 26)) / widget.profile.images!.length,
                                      height: 3,
                                      decoration: BoxDecoration(
                                        color: _pictureIndex.value == index ? _thisColor : Colors.grey,
                                        borderRadius: BorderRadius.circular(commonRadius),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            confettiController: _confettiController,
            blastDirectionality: BlastDirectionality.explosive,
            shouldLoop: false,
            colors: const [
              Colors.green,
              Colors.blue,
              Colors.pink,
              Colors.orange,
              Colors.purple
            ], // manually specify the colors to be used
            createParticlePath: drawStar, // define a custom shape/path.
          ),
        ),
      ],
    );
  }
}


