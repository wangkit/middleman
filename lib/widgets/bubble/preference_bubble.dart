import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/widgets/image/profile_media.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PreferenceBubble extends StatefulWidget {

  final UserPreference preference;

  PreferenceBubble({
    Key? key,
    required this.preference,
  });

  @override
  _PreferenceBubbleState createState() => _PreferenceBubbleState();
}

class _PreferenceBubbleState extends State<PreferenceBubble> {

  late Color _thisColor;
  late Future<DocumentSnapshot> _getProfile;

  @override
  void initState() {
    _getProfile = firestore
        .collection(FirestoreCollections.profiles)
        .doc(widget.preference.id!)
        .get();
    _thisColor = utils.convertStrToColor(widget.preference.id!);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12.0),
      child: Container(
        width: deviceWidth * 0.95,
        height: (deviceHeight - kToolbarHeight - 200) / 2,
        child: Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(commonRadius),
          ),
          color: _thisColor,
          child: FutureBuilder<DocumentSnapshot>(
            future: _getProfile,
            builder: (ctx, snapshot) {

              if (!snapshot.hasData || snapshot.connectionState != ConnectionState.done) {
                return LoadingWidget();
              }

              Map data = snapshot.data!.data() as Map;
              UserProfile _thisProfile = UserProfile.fromMapToProfile(data);

              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ProfileMedia(
                    key: Key(_thisProfile.images!.toString()),
                    userProfile: _thisProfile,
                    shouldAutoPlay: true,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}


