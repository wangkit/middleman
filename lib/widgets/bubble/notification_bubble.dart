import 'package:choicestory/apps/account/view_profile_page.dart';
import 'package:choicestory/apps/home/chat/chat_page.dart';
import 'package:choicestory/apps/home/chat/message_page.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/enum/activity.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/block_dialog.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/utils/dialog/report_dialog.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';

class NotificationBubble extends StatefulWidget {

  final NotificationMessage notification;

  NotificationBubble({
    Key? key,
    required this.notification,
  }) : super(key: key);

  @override
  NotificationBubbleState createState() => NotificationBubbleState();
}

class NotificationBubbleState extends State<NotificationBubble> with AutomaticKeepAliveClientMixin<NotificationBubble> {

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    super.build(context);
    return widget.notification.isTime ? Container(
      height: 65,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            timeUtils.isToday(widget.notification.createdAt) ? TextData.today : timeUtils.isYesterday(widget.notification.createdAt) ? TextData.yesterday : timeUtils.getYearMonthDay(widget.notification.createdAt),
            style: TextStyle(
              fontSize: 18,
            ),
          ),
        ],
      ),
    ) : Container(
      width: deviceWidth,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        child: ListTile(
          onTap: () {
            if (widget.notification.action == Activity.match.value) {

            } else if (widget.notification.action == Activity.greet.value) {
              getRoute.navigateTo(MessagePage(
                targetProfile: UserProfile(
                  id: widget.notification.senderId,
                  lastName: widget.notification.senderLastName,
                  firstName: widget.notification.senderFirstName,
                  images: widget.notification.senderImages,
                ),
                setNoUnread: null,
              ));
            }
          },
          leading: Icon(
            widget.notification.isRead ? Icons.mark_email_read : Icons.markunread,
            color: widget.notification.isRead ? unfocusedColor : utils.convertStrToColor(widget.notification.senderId),
            size: 30,
          ),
          title: Text(
            "${widget.notification.senderFirstName} ${widget.notification.senderLastName}" + ActivityExtension.messages[widget.notification.action]!,
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          trailing: Text(
            timeUtils.getHourMinuteFromUTCDateTime(widget.notification.createdAt),
            style: TextStyle(
              fontSize: 18,
            ),
          ),
        ),
      ),
    );
  }
}


