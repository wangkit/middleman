import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/configs/dialog_configs.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/button/custom_floating_action_button.dart';
import 'package:choicestory/widgets/button/dialog_botton_row.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/custom_flip_card/custom_flip_card.dart';
import 'package:choicestory/widgets/feature_discovery/custom_feature_overlay.dart';
import 'package:choicestory/widgets/image/profile_media.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';

class ProfileBubble extends StatefulWidget {

  final UserProfile profile;
  final Function? nextPage;
  final double? height;
  final bool showBottom;

  ProfileBubble({
    Key? key,
    required this.profile,
    this.nextPage,
    this.height,
    this.showBottom = true,
  });

  @override
  _ProfileBubbleState createState() => _ProfileBubbleState();
}

class _ProfileBubbleState extends State<ProfileBubble> with AutomaticKeepAliveClientMixin<ProfileBubble>, TickerProviderStateMixin {

  @override
  bool get wantKeepAlive => true;

  static const int _name = 0;
  static const int _location = 1;
  static const int _distance = 2;
  static const int _physicalGender = 3;
  static const int _genderIdentity = 4;
  static const int _age = 5;
  static const int _birthday = 6;
  static const int _zodiacSign = 7;
  static const int _profession = 8;
  static const int _education = 9;
  static const int _religion = 10;
  static const int _relationshipStatus = 11;
  static const int _likeCount = 12;
  static const int _lastActiveAt = 13;
  late Color _thisColor;
  late ValueNotifier<bool?> _opacity;
  GlobalKey<ProfileMediaState> _mediaKey = GlobalKey();
  GlobalKey<CustomFlipCardState> _flipCardKey = GlobalKey();
  static const double _widthPadding = 2;
  final int _totalIndex = 14;
  late ValueNotifier<int> _pictureIndex;
  final double _borderWidth = 1.5;

  @override
  void initState() {
    _opacity = ValueNotifier(null);
    _pictureIndex = ValueNotifier<int>(0);
    _thisColor = utils.convertStrToColor(widget.profile.id!);
    super.initState();
  }
  
  @override
  void dispose() {
    _opacity.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _card({Widget? child}) {
    return Container(
      height: widget.height != null ? widget.height : utils.getProfileMediaHeight() + deviceHeight * 0.08,
      width: deviceWidth,
      child: ValueListenableBuilder<bool?>(
        valueListenable: _opacity,
        builder: (ctx, bool? value, _) {
          return Card(
            elevation: 4,
            color: Colors.black,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(commonRadius),
              side: value == null ? BorderSide.none : BorderSide(
                color: value ? hotPink : turquoise,
              ),
            ),
            child: child!,
          );
        },
      ),
    );
  }

  void matchOrPass(bool isMatch) {
    apiManager.matchOrPass(widget.profile, isMatch);
    if (widget.nextPage != null) {
      widget.nextPage!(isMatch);
    }
  }

  void changePictureIndex(int newIndex) {
    _pictureIndex.value = newIndex;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return FadeIn(
      child: Scaffold(
        body: Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 1, left: _widthPadding, right: _widthPadding,),
            child: CustomFlipCard(
              key: _flipCardKey,
              back: _card(
                child: SingleChildScrollView(
                  child: Wrap(
                    children: List.generate(_totalIndex, (index) {

                      switch (index) {
                        case _name:
                          if (widget.profile.lastName == null || widget.profile.firstName == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _location:
                          if (widget.profile.country == null || widget.profile.city == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _physicalGender:
                          if (widget.profile.physicalGender == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _genderIdentity:
                          if (widget.profile.genderIdentity == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _age:
                          if (widget.profile.age == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _zodiacSign:
                          if (widget.profile.zodiacSign == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _profession:
                          if (widget.profile.profession == null || widget.profile.profession!.isEmpty) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _education:
                          if (widget.profile.education == null || widget.profile.education!.isEmpty) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _religion:
                          if (widget.profile.religion == null || widget.profile.religion!.isEmpty) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _relationshipStatus:
                          if (widget.profile.relationshipStatus == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _likeCount:
                          if (widget.profile.likeCount == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _lastActiveAt:
                          if (widget.profile.lastActiveAt == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _distance:
                          if (widget.profile.longitude == null || widget.profile.latitude == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                        case _birthday:
                          if (widget.profile.birthday == null) {
                            return CommonWidgets.emptyBox();
                          }
                          break;
                      }

                      dynamic _getValue() {
                        switch (index) {
                          case _name:
                            return utils.getFullName(widget.profile);
                          case _location:
                            return widget.profile.country!;
                          case _physicalGender:
                            return GenderExtension.names[widget.profile.physicalGender]!.capitalize;
                          case _genderIdentity:
                            return GenderExtension.names[widget.profile.genderIdentity]!.capitalize;
                          case _distance:
                            return "About ${utils.calculateDistance(widget.profile.latitude!, widget.profile.longitude!, qp.latitude!, qp.longitude!)}${AppConfig.distanceSuffix}";
                          case _birthday:
                            return timeUtils.getApproximateBirthday(widget.profile.birthday);
                          case _age:
                            return widget.profile.age!.toString();
                          case _zodiacSign:
                            return ZodiacSignExtension.names[widget.profile.zodiacSign]!;
                          case _profession:
                            return widget.profile.profession!;
                          case _education:
                            return widget.profile.education!;
                          case _religion:
                            return widget.profile.religion!;
                          case _relationshipStatus:
                            return RelationshipStatusExtension.names[widget.profile.relationshipStatus]!;
                          case _likeCount:
                            return widget.profile.likeCount;
                          case _lastActiveAt:
                            return timeUtils.getTimeDiffString(timeUtils.getParsedDateTime(widget.profile.lastActiveAt!));
                        }
                        return "";
                      }

                      String _getTitle() {
                        switch (index) {
                          case _name:
                            return "Name";
                          case _location:
                            return "Country";
                          case _physicalGender:
                            return "Physical Gender";
                          case _genderIdentity:
                            return "Gender Identity";
                          case _age:
                            return "Age";
                          case _zodiacSign:
                            return "Zodiac Sign";
                          case _profession:
                            return "Profession";
                          case _education:
                            return "Education";
                          case _religion:
                            return "Religion";
                          case _relationshipStatus:
                            return "Relationship";
                          case _likeCount:
                            return "Match Count";
                          case _distance:
                            return "Distance";
                          case _birthday:
                            return "Birthday";
                          case _lastActiveAt:
                            return "Last Active";
                        }
                        return "";
                      }

                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
                        child: Card(
                          color: ([appIconColor, gulfStream, turquoise, java, dustyRose, sandyBrown, bitterSweet, brinkPink, hotPink, coral, deluge, nepal, nudePink, neonCarrot, carrot, electricViolet, denim, creamCan, sunglow, carnation]..shuffle()).first,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(commonRadius),
                          ),
                          elevation: 4,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                child: Text(
                                  _getTitle(),
                                  style: Theme.of(context).textTheme.headline6!.merge(
                                    TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    )
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 16, right: 16, top: 4, bottom: 12),
                                child: Text(
                                  _getValue().toString(),
                                  style: Theme.of(context).textTheme.subtitle1!.merge(
                                    TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    )
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                ),
              ),
              front: _card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 30,
                      child: Stack(
                        children: [
                          ShaderMask(
                            shaderCallback: (rect) {
                              return LinearGradient(
                                begin: Alignment.center,
                                end: Alignment.bottomCenter,
                                colors: [Colors.black, Colors.transparent],
                              ).createShader(Rect.fromLTRB(0, rect.height * profileShadeValue, rect.width, rect.height));
                            },
                            blendMode: BlendMode.dstIn,
                            child: CustomFeatureOverlay(
                              id: DiscoveryValue.tapToFlip,
                              title: TextData.tapToFlip,
                              description: TextData.tapToFlipDetails,
                              backgroundColor: brinkPink,
                              child: ProfileMedia(
                                key: _mediaKey,
                                height: widget.height,
                                userProfile: widget.profile,
                                shouldAutoPlay: true,
                                changePictureIndex: changePictureIndex,
                              ),
                            ),
                          ),
                          Positioned.fill(
                            right: 10,
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 16, bottom: 40),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                      child: Row(
                                        children: [
                                          Text(
                                            widget.profile.firstName!,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 24
                                            ),
                                          ),
                                          SizedBox(width: 8),
                                          Text(
                                            "${widget.profile.age}",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 24
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 8,),
                                    Flexible(
                                      child: GestureDetector(
                                        onLongPress: () async {
                                          await showModal(
                                            context: getRoute.getContext(),
                                            configuration: DialogConfig.getTransition(
                                              isBarrierDismissible: true,
                                            ),
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                title: Text(
                                                  "Change Shade Value",
                                                  textAlign: TextAlign.center,
                                                  style: DialogConfig.dialogTitleStyle,
                                                ),
                                                shape: DialogConfig.dialogShape(),
                                                content: StatefulBuilder(
                                                  builder: (ctx, customState) {
                                                    return Column(
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: [
                                                        Slider.adaptive(
                                                          value: profileShadeValue,
                                                          min: AppConfig.minShade.toDouble(),
                                                          max: AppConfig.maxShade.toDouble(),
                                                          divisions: 10,
                                                          activeColor: appIconColor,
                                                          inactiveColor: unfocusedColor,
                                                          label: profileShadeValue < 0.3 ? "More shade" : profileShadeValue > 0.7 ? "Less shade" : "Medium shade",
                                                          onChanged: (double newValue) {
                                                            profileShadeValue = newValue;
                                                            utils.saveDouble(PrefKey.profileShadeValue, profileShadeValue);
                                                            customState(() {});
                                                          },
                                                        ),
                                                        DialogButtonRow(
                                                          onOkTap: () {
                                                            getRoute.pop();
                                                          },
                                                          okColor: positiveColor,
                                                          topPadding: 24,
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                ),
                                              );
                                            },
                                          );
                                          refreshThisPage();
                                        },
                                        child: Text(
                                          widget.profile.personality!,
                                          maxLines: 5,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 19
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: widget.showBottom ? 4 : 0),
                    !widget.showBottom ? CommonWidgets.emptyBox() : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        FloatingActionButton(
                          heroTag: UniqueKey().toString(),
                          mini: true,
                          tooltip: TextData.previous,
                          backgroundColor: Colors.black,
                          shape: CircleBorder(
                            side: BorderSide(
                              color: Colors.white,
                              width: _borderWidth,
                            ),
                          ),
                          child: Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            _mediaKey.currentState!.previousPage();
                          },
                        ),
                        CustomFloatingActionButton(
                          heroTag: UniqueKey().toString(),
                          tooltip: TextData.pass,
                          onTapDown: (TapDownDetails value) {
                            _opacity.value = false;
                          },
                          backgroundColor: Colors.black,
                          shape: CircleBorder(
                            side: BorderSide(
                              color: turquoise,
                              width: _borderWidth,
                            ),
                          ),
                          child: JelloIn(
                            child: Icon(
                              Icons.clear,
                              color: turquoise,
                              size: 30,
                            ),
                          ),
                          onPressed: () {
                            matchOrPass(false);
                          },
                        ),
                        FloatingActionButton(
                          heroTag: UniqueKey().toString(),
                          mini: true,
                          tooltip: TextData.flip,
                          backgroundColor: Colors.black,
                          shape: CircleBorder(
                            side: BorderSide(
                              color: Colors.white,
                              width: _borderWidth,
                            ),
                          ),
                          child: Icon(
                            Icons.flip_outlined,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            if (_flipCardKey.currentState != null) {
                              _flipCardKey.currentState!.toggleCard();
                            }
                          },
                        ),
                        CustomFloatingActionButton(
                          heroTag: UniqueKey().toString(),
                          tooltip: TextData.match,
                          backgroundColor: Colors.black,
                          onTapDown: (TapDownDetails value) {
                            _opacity.value = true;
                          },
                          shape: CircleBorder(
                            side: BorderSide(
                              color: hotPink,
                              width: _borderWidth,
                            ),
                          ),
                          child: JelloIn(
                            child: Icon(
                              MdiIcons.heart,
                              color: hotPink,
                              size: 30,
                            ),
                          ),
                          onPressed: () {
                            matchOrPass(true);
                          },
                        ),
                        FloatingActionButton(
                          heroTag: UniqueKey().toString(),
                          mini: true,
                          tooltip: TextData.next,
                          backgroundColor: Colors.black,
                          shape: CircleBorder(
                            side: BorderSide(
                              color: Colors.white,
                              width: _borderWidth,
                            ),
                          ),
                          child: Icon(
                            Icons.arrow_forward_ios_outlined,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            _mediaKey.currentState!.nextPage();
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: widget.showBottom ? 8 : 0),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: List.generate(widget.profile.images!.length, (index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                child: ValueListenableBuilder(
                                  valueListenable: _pictureIndex,
                                  builder: (ctx, value, child) {
                                    return Container(
                                      width: (deviceWidth - (_widthPadding * 26)) / widget.profile.images!.length,
                                      height: 3,
                                      decoration: BoxDecoration(
                                        color: _pictureIndex.value == index ? _thisColor : Colors.grey,
                                        borderRadius: BorderRadius.circular(commonRadius),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}


