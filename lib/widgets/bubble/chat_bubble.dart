import 'package:choicestory/apps/account/view_profile_page.dart';
import 'package:choicestory/apps/home/chat/chat_page.dart';
import 'package:choicestory/apps/home/chat/message_page.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/block_dialog.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/utils/dialog/report_dialog.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';

class ChatBubble extends StatefulWidget {

  final ChatItem chatItem;

  ChatBubble({
    Key? key,
    required this.chatItem,
  }) : super(key: key);

  @override
  ChatBubbleState createState() => ChatBubbleState();
}

class ChatBubbleState extends State<ChatBubble> with AutomaticKeepAliveClientMixin<ChatBubble> {

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void getChatBottomSheet() {
    getGenericBottomSheet(context, [
      BottomSheetButton(
        label: TextData.viewProfile,
        iconData: MdiIcons.accountDetails,
        onTap: () {
          getRoute.navigateTo(
            ViewProfilePage(
              targetId: widget.chatItem.targetId,
            )
          );
        },
      ),
      BottomSheetButton(
        iconData: Icons.delete,
        label: TextData.deleteChat,
        onTap: () {
          getBinaryDialog(TextData.deleteChat, TextData.deleteChatForMyself, () {
            getRoute.pop();
            apiManager.deleteChat(true, widget.chatItem.targetId);
          });
        },
      ),
      BottomSheetButton(
        label: TextData.clearChat,
        iconData: Icons.clear_all,
        onTap: () {
          getBinaryDialog(TextData.deleteAllMessages, TextData.deleteAllMessagesAlert, () {
            getRoute.pop();
            apiManager.clearMessageInChat(utils.getChatId([qp.id!, widget.chatItem.targetId]), widget.chatItem.targetId);
          });
        },
      ),
      BottomSheetButton(
        label: TextData.block,
        iconData: Icons.block,
        onTap: () => getBlockDialog(UserProfile(
          id: widget.chatItem.targetId,
          firstName: widget.chatItem.targetFirstName,
          lastName: widget.chatItem.targetLastName
        )),
      ),
      BottomSheetButton(
        label: TextData.report,
        iconData: Icons.report,
        onTap: () => getReportDialog(UserProfile(
          id: widget.chatItem.targetId,
          firstName: widget.chatItem.targetFirstName,
          lastName: widget.chatItem.targetLastName
        )),
      ),
    ]);
  }

  void setNoUnread() {
    widget.chatItem.hasUnread = false;
  }

  @override
  Widget build(BuildContext context) {

    super.build(context);
    return Material(
      color: appBgColor,
      child: InkWell(
        onTap: () {
          getRoute.navigateTo(MessagePage(
            targetProfile: UserProfile(
              id: widget.chatItem.targetId,
              lastName: widget.chatItem.targetLastName,
              firstName: widget.chatItem.targetFirstName,
              images: widget.chatItem.targetImages,
            ),
            setNoUnread: setNoUnread,
          ));
        },
        onLongPress: () {
          getChatBottomSheet();
        },
        child: Container(
          /// Determines the height of each chat item
          height: 90,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 12.0, top: 8.0, bottom: 8.0),
                child: NetworkThumbnail(
                  width: 65,
                  height: 65,
                  imageUrl: utils.getProfileThumbnail(widget.chatItem.targetImages),
                  disableOnTap: true,
                ),
              ),
              Expanded(
                child: ListTile(
                  title: Row(
                    children: [
                      Text(
                        widget.chatItem.targetFirstName + " " + widget.chatItem.targetLastName,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(width: 10,),
                      widget.chatItem.hasUnread ? Icon(
                        MdiIcons.messageAlert,
                        color: hotPink,
                      ) : CommonWidgets.emptyBox(),
                    ],
                  ),
                  trailing: widget.chatItem.lastMessage.isEmpty ? CommonWidgets.emptyBox() : Text(
                    timeUtils.getHourMinuteFromUTCDateTime(widget.chatItem.updatedAt),
                  ),
                  subtitle: !widget.chatItem.lastMessage.isEmpty ? Padding(
                    padding: EdgeInsets.only(top: 12.0),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Wrap(
                            children: <Widget>[
                              Text(
                                widget.chatItem.isLastMessageMyself ? "You: ${widget.chatItem.lastMessage}" : widget.chatItem.lastMessage,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: TextStyle(
                                  color: widget.chatItem.hasUnread ? mainColor : Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ) : null,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


