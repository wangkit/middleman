import 'package:choicestory/constants/const.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/const/constant.dart';
import 'package:string_similarity/string_similarity.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class GenericTypeAheadTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode? nextFocusNode;
  final String hint;
  final String? helper;
  final bool canEmpty;
  final bool canSpace;
  final TextInputType? textInputType;
  final List dataList;
  final int? maxLines;
  final int? maxLength;
  final Function? onSubmitted;
  final Widget? trailing;
  final double borderRadius;

  GenericTypeAheadTextField({
    Key? key,
    required this.controller,
    required this.focusNode,
    this.canSpace = true,
    this.maxLines,
    this.maxLength,
    this.onSubmitted,
    this.trailing,
    this.textInputType,
    required this.dataList,
    this.nextFocusNode,
    required this.hint,
    this.helper,
    this.canEmpty = false,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  _GenericTypeAheadTextFieldState createState() => _GenericTypeAheadTextFieldState();
}

class _GenericTypeAheadTextFieldState extends State<GenericTypeAheadTextField> {

  late bool isVisible;

  @override
  void initState() {
    isVisible = false;
    widget.focusNode.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    super.initState();
  }

  FadeTransition suggestionTransition(Widget suggestionsBox, AnimationController animationController) {
    return FadeTransition(
      child: suggestionsBox,
      opacity: CurvedAnimation(
          parent: animationController,
          curve: Curves.fastOutSlowIn
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
      child: TypeAheadFormField(
        validator: (value) {
          if (value!.isNullOrEmpty && !widget.canEmpty) {
            return "${widget.hint.capitalize} must not be empty";
          }
          return null;
        },
        keepSuggestionsOnSuggestionSelected: false,
        autoFlipDirection: true,
        suggestionsCallback: (String currentString) {
          List<dynamic> _tempList = [];
          try {
            _tempList = new List.from(widget.dataList);
          } on Exception catch(e) {
            print("Exception: $e");
          }
          List<double> _ratingList = [];
          widget.dataList.forEach((data) {
            double rating = StringSimilarity.compareTwoStrings(currentString.capitalize, data);
            _ratingList.add(rating);
          });
          final Map<double, String> mappings = {
            for (int i = 0; i < _ratingList.length; i++)
              _ratingList[i]: _tempList[i]
          };
          _ratingList.sort((b, a) => a.compareTo(b));
          _tempList = [for (double rating in _ratingList) if (rating > 0.1) mappings[rating]!];
          _tempList = _tempList.toSet().toList();
          return _tempList;
        },
        hideOnEmpty: true,
        hideOnError: true,
        itemBuilder: (context, suggestion) {
          return Padding(
            padding: EdgeInsets.only(top: 14.0, left: 8.0, bottom: 14.0,),
            child: Text(
              suggestion.toString(),
            ),
          );
        },
        transitionBuilder: (context, suggestionsBox, animationController) => suggestionTransition(suggestionsBox, animationController!),
        onSuggestionSelected: (suggestion) {
          widget.controller.text = suggestion.toString();
        },
        textFieldConfiguration: TextFieldConfiguration(
          focusNode: widget.focusNode,
          maxLength: widget.maxLength ?? 150,
          maxLines: widget.maxLines ?? 1,
          textCapitalization: TextCapitalization.sentences,
          onSubmitted: (term) {
            if (widget.nextFocusNode != null) {
              utils.fieldFocusChange(widget.focusNode, widget.nextFocusNode!);
            }
          },
          onEditingComplete: () {
            if (widget.nextFocusNode != null) {
              utils.fieldFocusChange(widget.focusNode, widget.nextFocusNode!);
            }
          },
          controller: widget.controller,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: mainColor,
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(widget.borderRadius),
              borderSide: BorderSide(
                color: mainColor,
              ),
            ),
            helperText: widget.helper,
            counterText: "",
            helperMaxLines: 3,
            hintMaxLines: 3,
            errorMaxLines: 3,
            helperStyle: TextStyle(
              color: mainColor,
            ),
            hintText: widget.hint,
            hintStyle: TextStyle(
              color: unfocusedColor,
            ),
            suffixIcon: AnimatedOpacity(
              opacity: isVisible ? 1.0 : 0.0,
              duration: textFieldOpacityAnimationDuration,
              child: widget.trailing ?? CommonWidgets.clearTextFieldButton(widget.controller),
            ),
          ),
        ),
      ),
    );
  }
}