import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class PasswordTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final String? label;
  final String? helper;
  final bool isNewPassword;
  final EdgeInsets? edgeInsets;
  final double borderRadius;
  final bool showLabel;
  final bool showHint;
  final TextStyle? labelHintStyle;
  final TextAlign? textAlign;
  final Color? borderColor;
  final bool needSuffix;
  final Color? cursorColor;
  final Color? fillColor;

  PasswordTextField({
    Key? key,
    this.isNewPassword = false,
    this.needSuffix = true,
    this.cursorColor,
    required this.controller,
    this.fillColor,
    this.focusNode,
    this.labelHintStyle,
    this.borderColor,
    this.textAlign,
    this.showHint = true,
    this.showLabel = false,
    this.nextFocusNode,
    this.borderRadius = 0,
    this.label,
    this.helper,
    this.edgeInsets,
  }) : super(key: key);

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {

  final regPattern = RegExp(r"^(?=.*[a-zA-Z])(?=.*[0-9])");
  late bool isObscureText;
  late bool isValidFormat;
  late bool isVisible;
  late int minLength;

  @override
  void initState() {
    isVisible = false;
    minLength = 8;
    isObscureText = true;
    isValidFormat = true;
    if (widget.focusNode != null) {
      widget.focusNode!.addListener(() {
        if (this.mounted) {
          setState(() {
            isVisible = !isVisible;
          });
        }
      });
    }
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  String? validate(String value) {
    if (value.isEmpty) {
      return "Password must not be empty";
    } else if (value.length < minLength) {
      return "Password must contain at least $minLength characters";
    } else if (!regPattern.hasMatch(value)) {
      return "Password must contain at least one letter and one number";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.edgeInsets ?? EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
      child: TextFormField(
        cursorColor: widget.cursorColor,
        textAlign: widget.textAlign ?? TextAlign.start,
        onFieldSubmitted: (term) {
          if (widget.focusNode != null && widget.nextFocusNode != null) {
            utils.fieldFocusChange(widget.focusNode!, widget.nextFocusNode!);
          }
        },
        obscureText: isObscureText,
        autofillHints: widget.isNewPassword ? [AutofillHints.newPassword] : [AutofillHints.password],
        focusNode: widget.focusNode,
        style: widget.labelHintStyle ?? TextStyle(
          color: mainColor,
        ),
        controller: widget.controller,
        validator: (String? value) {
          return validate(value!);
        },
        maxLength: 25,
        decoration: InputDecoration(
          fillColor: widget.fillColor,
          filled: widget.fillColor != null,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
            borderSide: BorderSide(
              color: widget.borderColor ?? mainColor,
            ),
          ),
          helperText: widget.helper,
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 2,
          counterText: "",
          labelText: widget.showLabel ? widget.label : null,
          labelStyle: widget.labelHintStyle ?? TextStyle(
            color: unfocusedColor,
          ),
          hintText: widget.showHint ? "Password" : null,
          hintStyle: widget.labelHintStyle ?? TextStyle(
            color: unfocusedColor,
          ),

          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: widget.borderColor ?? mainColor,
            ),
          ),
          suffixIcon: !widget.needSuffix ? null : AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: kThemeAnimationDuration,
            child: IconButton(
              icon: Icon(
                isObscureText ? MdiIcons.eye : MdiIcons.eyeOff,
                color: unfocusedColor,
              ),
              onPressed: () {
                setState(() {
                  isObscureText = !isObscureText;
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}