import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/configs/dialog_configs.dart';

class DialogPositiveButton extends StatefulWidget {
  final Function onOkTap;
  final String? buttonOkText;
  final Color? okColor;

  DialogPositiveButton({
    Key? key,
    required this.onOkTap,
    this.buttonOkText,
    this.okColor,
  }) : super(key: key);

  @override
  DialogPositiveButtonState createState() => DialogPositiveButtonState();
}

class DialogPositiveButtonState extends State<DialogPositiveButton> {


  @override
  Widget build(BuildContext context) {

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: widget.okColor ?? Colors.green,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(DialogConfig.dialogCornerRadius),
        ),
      ),
      onPressed: () => widget.onOkTap(),
      child: Text(
        widget.buttonOkText ?? 'OK',
        style: TextStyle(
            color: Colors.white
        ),
      ),
    );
  }
}