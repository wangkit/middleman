import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/configs/dialog_configs.dart';
import 'package:choicestory/widgets/button/dialog_negative_button.dart';

import 'dialog_positive_button.dart';

class DialogButtonRow extends StatefulWidget {
  final Function onOkTap;
  final Function? onCancelTap;
  final String? buttonCancelText;
  final String? buttonOkText;
  final Color? okColor;
  final Color? cancelColor;
  final Color? backgroundColor;
  final double topPadding;

  DialogButtonRow({
    Key? key,
    required this.onOkTap,
    this.onCancelTap,
    this.buttonCancelText,
    this.buttonOkText,
    this.okColor,
    this.cancelColor,
    this.backgroundColor,
    this.topPadding = 0,
  }) : super(key: key);

  @override
  DialogButtonRowState createState() => DialogButtonRowState();
}

class DialogButtonRowState extends State<DialogButtonRow> {


  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.only(top: widget.topPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          DialogNegativeButton(
            onCancelTap: widget.onCancelTap != null ? () => widget.onCancelTap!() : () => Navigator.of(context).pop(),
            buttonCancelText: widget.buttonCancelText ?? 'Cancel',
            cancelColor: widget.cancelColor ?? Colors.grey,
          ),
          DialogPositiveButton(
            onOkTap: () => widget.onOkTap(),
            buttonOkText: widget.buttonOkText ?? 'Confirm',
            okColor: widget.okColor ?? Colors.green,
          ),
        ],
      ),
    );
  }
}