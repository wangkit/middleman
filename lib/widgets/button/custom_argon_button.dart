import 'dart:async';
import 'dart:ui';

import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:soy_common/widgets/widgets.dart';

class CustomArgonButton extends StatefulWidget {
  final bool isReverseColor;
  final Function onPressed;
  final String title;
  final double? height;
  final double? elevation;
  final double? width;
  final Color? color;
  final double bottomLeftRadius;
  final double bottomRightRadius;
  final TextStyle? textStyle;
  final bool isEnabled;
  final Color? splashColor;
  final Color? loadColor;
  final bool liquidLoader;

  CustomArgonButton({
    Key? key,
    this.textStyle,
    this.isEnabled = true,
    this.bottomLeftRadius = 36,
    this.bottomRightRadius = 36,
    this.height,
    this.color,
    this.width,
    this.splashColor,
    this.elevation,
    this.isReverseColor = false,
    this.loadColor,
    this.liquidLoader = false,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  @override
  CustomArgonButtonState createState() => CustomArgonButtonState();
}

class CustomArgonButtonState extends State<CustomArgonButton> {


  late bool disabled;
  late Color bgColor;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void disableButton() {
    disabled = true;
    refreshThisPage();
  }

  void enableButton() {
    disabled = false;
    refreshThisPage();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    disabled = !widget.isEnabled;
    bgColor = (widget.isReverseColor ? Colors.grey : widget.color != null ? widget.color : appIconColor)!;

    return AbsorbPointer(
      absorbing: disabled,
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: TakenArgonButton(
          focusElevation: widget.elevation,
          hoverElevation: widget.elevation,
          highlightElevation: widget.elevation,
          width: widget.width != null ? widget.width! : deviceWidth * 0.9,
          height: widget.height != null ? widget.height! : 50,
          bottomLeftRadius: widget.bottomLeftRadius,
          bottomRightRadius: widget.bottomRightRadius,
          color: bgColor,
          splashColor: widget.splashColor ?? bgColor.withOpacity(0.5),
          elevation: widget.elevation ?? 0.0,
          onTap: (startLoading, stopLoading, btnState) async {
            if (btnState == ButtonState.Idle) {
              startLoading();
              await widget.onPressed();
              stopLoading();
            }
          },
          loader: widget.liquidLoader ? Container(
            width: widget.width != null ? widget.width : deviceWidth * 0.9,
            height: widget.height != null ? widget.height : 50,
            child: LiquidLinearProgressIndicator(
              value: 1,
              valueColor: AlwaysStoppedAnimation(widget.loadColor == null ? appIconColor : widget.loadColor!),
              backgroundColor: Colors.white,
              direction: Axis.horizontal,
            ),
          ) : LoadingWidget(),
          child: Text(
            widget.title,
            textAlign: TextAlign.center,
            style: widget.textStyle != null ? widget.textStyle : TextStyle(
              fontSize: 18.0,
            ),
          ),
        ),
      ),
    );
  }
}


class TakenArgonButton extends StatefulWidget {
  final double height;
  final double width;
  final double minWidth;
  final Widget? loader;
  final Duration animationDuration;
  final Curve curve;
  final Curve reverseCurve;
  final Widget child;
  final Function(
      Function startLoading, Function stopLoading, ButtonState btnState)? onTap;
  final Color? color;
  final Color? focusColor;
  final Color? hoverColor;
  final Color? highlightColor;
  final Color? splashColor;
  final Brightness? colorBrightness;
  final double? elevation;
  final double? focusElevation;
  final double? hoverElevation;
  final double? highlightElevation;
  final EdgeInsetsGeometry padding;
  final Clip clipBehavior;
  final FocusNode? focusNode;
  final MaterialTapTargetSize? materialTapTargetSize;
  final bool roundLoadingShape;
  final double bottomLeftRadius;
  final double bottomRightRadius;
  final BorderSide borderSide;
  final double? disabledElevation;
  final Color? disabledColor;
  final Color? disabledTextColor;

  TakenArgonButton(
      {required this.height,
        required this.width,
        this.minWidth: 0,
        this.loader,
        this.animationDuration: const Duration(milliseconds: 450),
        this.curve: Curves.easeInOutCirc,
        this.reverseCurve: Curves.easeInOutCirc,
        required this.child,
        this.onTap,
        this.color,
        this.focusColor,
        this.hoverColor,
        this.highlightColor,
        this.splashColor,
        this.colorBrightness,
        this.elevation,
        this.focusElevation,
        this.hoverElevation,
        this.highlightElevation,
        this.padding: const EdgeInsets.all(0),
        this.bottomLeftRadius: 0.0,
        this.bottomRightRadius: 0.0,
        this.clipBehavior: Clip.none,
        this.focusNode,
        this.materialTapTargetSize,
        this.roundLoadingShape: true,
        this.borderSide: const BorderSide(color: Colors.transparent, width: 0),
        this.disabledElevation,
        this.disabledColor,
        this.disabledTextColor})
      : assert(elevation == null || elevation >= 0.0),
        assert(focusElevation == null || focusElevation >= 0.0),
        assert(hoverElevation == null || hoverElevation >= 0.0),
        assert(highlightElevation == null || highlightElevation >= 0.0),
        assert(disabledElevation == null || disabledElevation >= 0.0),
        assert(clipBehavior != null);

  @override
  _TakenArgonButtonState createState() => _TakenArgonButtonState();
}

class _TakenArgonButtonState extends State<TakenArgonButton>
    with TickerProviderStateMixin {
  double? loaderWidth;

  late Animation<double> _animation;
  late AnimationController _controller;
  ButtonState btn = ButtonState.Idle;

  GlobalKey _buttonKey = GlobalKey();
  double _minWidth = 0;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: widget.animationDuration);

    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: widget.curve,
        reverseCurve: widget.reverseCurve));

    _animation.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) {
        setState(() {
          btn = ButtonState.Idle;
        });
      }
    });

    minWidth = widget.height;
    loaderWidth = widget.height;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void animateForward() {
    setState(() {
      btn = ButtonState.Busy;
    });
    _controller.forward();
  }

  void animateReverse() {
    _controller.reverse();
  }

  lerpWidth(a, b, t) {
    if (a == 0.0 || b == 0.0) {
      return null;
    } else {
      return a + (b - a) * t;
    }
  }

  double get minWidth => _minWidth;
  set minWidth(double w) {
    if (widget.minWidth == 0) {
      _minWidth = w;
    } else {
      _minWidth = widget.minWidth;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return buttonBody();
      },
    );
  }

  Widget buttonBody() {
    return Container(
      height: widget.height,
      width: lerpWidth(widget.width, minWidth, _animation.value),
      child: ButtonTheme(
        height: widget.height,
        shape: RoundedRectangleBorder(
          side: widget.borderSide,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(widget.bottomLeftRadius),
            bottomRight: Radius.circular(widget.bottomRightRadius),
          )
        ),
        child: RaisedButton(
            key: _buttonKey,
            color: widget.color,
            focusColor: widget.focusColor,
            hoverColor: widget.hoverColor,
            highlightColor: widget.highlightColor,
            splashColor: widget.splashColor,
            colorBrightness: widget.colorBrightness,
            elevation: widget.elevation,
            focusElevation: widget.focusElevation,
            hoverElevation: widget.hoverElevation,
            highlightElevation: widget.highlightElevation,
            padding: widget.padding,
            clipBehavior: widget.clipBehavior,
            focusNode: widget.focusNode,
            materialTapTargetSize: widget.materialTapTargetSize,
            disabledElevation: widget.disabledElevation,
            disabledColor: widget.disabledColor,
            disabledTextColor: widget.disabledTextColor,
            onPressed: () {
              widget.onTap!(
                      () => animateForward(), () => animateReverse(), btn);
              // btnClicked();
            },
            child: btn == ButtonState.Idle ? widget.child : widget.loader),
      ),
    );
  }
}

class ArgonTimerButton extends StatefulWidget {
  final double height;
  final double width;
  final double minWidth;
  final Function(int time)? loader;
  final Duration animationDuration;
  final Curve curve;
  final Curve reverseCurve;
  final Widget child;
  final Function(Function startTimer, ButtonState? btnState)? onTap;
  final Color? color;
  final Color? focusColor;
  final Color? hoverColor;
  final Color? highlightColor;
  final Color? splashColor;
  final Brightness? colorBrightness;
  final double? elevation;
  final double? focusElevation;
  final double? hoverElevation;
  final double? highlightElevation;
  final EdgeInsetsGeometry padding;
  final Clip clipBehavior;
  final FocusNode? focusNode;
  final MaterialTapTargetSize? materialTapTargetSize;
  final bool roundLoadingShape;
  final double borderRadius;
  final BorderSide borderSide;
  final double? disabledElevation;
  final Color? disabledColor;
  final Color? disabledTextColor;
  final int initialTimer;

  ArgonTimerButton(
      {required this.height,
        required this.width,
        this.minWidth: 0,
        this.loader,
        this.animationDuration: const Duration(milliseconds: 450),
        this.curve: Curves.easeInOutCirc,
        this.reverseCurve: Curves.easeInOutCirc,
        required this.child,
        this.onTap,
        this.color,
        this.focusColor,
        this.hoverColor,
        this.highlightColor,
        this.splashColor,
        this.colorBrightness,
        this.elevation,
        this.focusElevation,
        this.hoverElevation,
        this.highlightElevation,
        this.padding: const EdgeInsets.all(0),
        this.borderRadius: 0.0,
        this.clipBehavior: Clip.none,
        this.focusNode,
        this.materialTapTargetSize,
        this.roundLoadingShape: true,
        this.borderSide: const BorderSide(color: Colors.transparent, width: 0),
        this.disabledElevation,
        this.disabledColor,
        this.disabledTextColor,
        this.initialTimer: 0})
      : assert(elevation == null || elevation >= 0.0),
        assert(focusElevation == null || focusElevation >= 0.0),
        assert(hoverElevation == null || hoverElevation >= 0.0),
        assert(highlightElevation == null || highlightElevation >= 0.0),
        assert(disabledElevation == null || disabledElevation >= 0.0),
        assert(clipBehavior != null);

  @override
  _ArgonTimerButtonState createState() => _ArgonTimerButtonState();
}

class _ArgonTimerButtonState extends State<ArgonTimerButton>
    with TickerProviderStateMixin {
  double? loaderWidth;

  late Animation<double> _animation;
  late AnimationController _controller;
  ButtonState? btn;
  int secondsLeft = 0;
  Timer? _timer;
  Stream emptyStream = Stream.empty();
  double _minWidth = 0;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: widget.animationDuration);

    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: widget.curve,
        reverseCurve: widget.reverseCurve));

    _animation.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) {
        setState(() {
          btn = ButtonState.Idle;
        });
      }
    });

    if (widget.initialTimer == 0) {
      btn = ButtonState.Idle;
    } else {
      startTimer(widget.initialTimer);
      btn = ButtonState.Busy;
    }

    minWidth = widget.height;
    loaderWidth = widget.height;
  }

  @override
  void dispose() {
    if (_timer != null) _timer!.cancel();
    _controller.dispose();
    super.dispose();
  }

  void animateForward() {
    setState(() {
      btn = ButtonState.Busy;
    });
    _controller.forward();
  }

  void animateReverse() {
    _controller.reverse();
  }

  lerpWidth(a, b, t) {
    if (a == 0.0 || b == 0.0) {
      return null;
    } else {
      return a + (b - a) * t;
    }
  }

  double get minWidth => _minWidth;
  set minWidth(double w) {
    if (widget.minWidth == 0) {
      _minWidth = w;
    } else {
      _minWidth = widget.minWidth;
    }
  }

  void startTimer(int newTime) {
    if (newTime == 0) {
      throw ("Count Down Time can not be null");
    }

    animateForward();

    setState(() {
      secondsLeft = newTime;
    });

    if (_timer != null) {
      _timer!.cancel();
    }

    var oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
          if (secondsLeft < 1) {
            timer.cancel();
          } else {
            secondsLeft = secondsLeft - 1;
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return buttonBody();
      },
    );
  }

  Widget buttonBody() {
    return Container(
      height: widget.height,
      width: lerpWidth(widget.width, minWidth, _animation.value),
      child: ButtonTheme(
        height: widget.height,
        shape: RoundedRectangleBorder(
          side: widget.borderSide,
          borderRadius: BorderRadius.circular(widget.roundLoadingShape
              ? lerpDouble(
              widget.borderRadius, widget.height / 2, _animation.value)!
              : widget.borderRadius),
        ),
        child: RaisedButton(
            color: widget.color,
            focusColor: widget.focusColor,
            hoverColor: widget.hoverColor,
            highlightColor: widget.highlightColor,
            splashColor: widget.splashColor,
            colorBrightness: widget.colorBrightness,
            elevation: widget.elevation,
            focusElevation: widget.focusElevation,
            hoverElevation: widget.hoverElevation,
            highlightElevation: widget.highlightElevation,
            padding: widget.padding,
            clipBehavior: widget.clipBehavior,
            focusNode: widget.focusNode,
            materialTapTargetSize: widget.materialTapTargetSize,
            disabledElevation: widget.disabledElevation,
            disabledColor: widget.disabledColor,
            disabledTextColor: widget.disabledTextColor,
            onPressed: () {
              widget.onTap!((newCounter) => startTimer(newCounter), btn);
            },
            child: btn == ButtonState.Idle
                ? widget.child
                : StreamBuilder(
                stream: emptyStream,
                builder: (context, snapshot) {
                  if (secondsLeft == 0) {
                    animateReverse();
                  }
                  return widget.loader!(secondsLeft);
                })),
      ),
    );
  }
}
