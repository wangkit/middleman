import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/apps/image/image_page.dart';
import 'package:choicestory/constants/const.dart';

class NetworkThumbnail extends StatefulWidget {
  final String imageUrl;
  final double width;
  final double height;
  final double radius;
  final bool showBorder;
  final Function? onError;
  final String? hero;
  final bool disableOnTap;

  NetworkThumbnail({
    Key? key,
    required this.imageUrl,
    this.width = 70.0,
    this.height = 70.0,
    this.radius = 200.0,
    this.showBorder = false,
    this.onError,
    this.hero,
    this.disableOnTap = false,
  }) : super(key: key);

  @override
  NetworkThumbnailState createState() => NetworkThumbnailState();
}

class NetworkThumbnailState extends State<NetworkThumbnail> {

  late String _hero;

  @override
  void initState() {
    _hero = widget.hero ?? UniqueKey().toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: widget.disableOnTap ? null : () {
        getRoute.navigateTo(ImagePage(
          imageUrl: widget.imageUrl,
          heroTag: _hero,
        ),);
      },
      child: Hero(
        tag: _hero,
        child: Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.radius),
            image: DecorationImage(
              fit: BoxFit.cover,
              onError: (ctx, stacktrace) {
                if (widget.onError != null) {
                  widget.onError!(stacktrace);
                }
              },
              image: CachedNetworkImageProvider(
                widget.imageUrl,
              ),
            ),
          ),
        ),
      ),
    );
  }
}