import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/widgets/loading/loading_media_widget.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import 'package:soy_common/color/color.dart';

/// Holds an image and acts as an access point to image viewer
class ImageHolder extends StatefulWidget {
  final String heroKey;
  final bool disableOnTap;
  final double? width;
  final double? height;
  final List<String> imageList;
  final FilterQuality quality;
  final Color userColor;
  final int index;
  final Color? color;
  final String name;
  final Function nextPage;
  final Function previousPage;

  ImageHolder({
    Key? key,
    required this.index,
    required this.userColor,
    this.color,
    this.width,
    this.name = "",
    required this.nextPage,
    required this.previousPage,
    required this.imageList,
    this.quality = FilterQuality.low,
    this.height,
    this.disableOnTap = false,
    required this.heroKey,
  }) : super(key: key);
  @override
  _ImageHolderState createState() => _ImageHolderState();
}

class _ImageHolderState extends State<ImageHolder> with AutomaticKeepAliveClientMixin<ImageHolder> {

  get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: widget.color,
      width: widget.width,
      child: Hero(
        tag: widget.heroKey,
        child: PinchZoom(
          zoomedBackgroundColor: Colors.black.withOpacity(0.5),
          image: CachedNetworkImage(
            filterQuality: widget.quality,
            fit: BoxFit.cover,
            imageUrl: widget.imageList[widget.index],
            errorWidget: (context, url, error) => Icon(Icons.error, color: errorColor),
            placeholder: (BuildContext context, String url) {
              return Container(
                width: widget.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    LoadingMediaWidget(),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}