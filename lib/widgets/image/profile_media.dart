import 'dart:async';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/widgets/image/image_holder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class ProfileMedia extends StatefulWidget {
  final UserProfile userProfile;
  final bool shouldAutoPlay;
  final bool disableTap;
  final Function? changePictureIndex;
  final double? height;

  ProfileMedia({
    Key? key,
    required this.userProfile,
    this.shouldAutoPlay = false,
    this.disableTap = true,
    this.changePictureIndex,
    this.height,
  }) : super(key: key);

  @override
  ProfileMediaState createState() => ProfileMediaState();
}

class ProfileMediaState extends State<ProfileMedia> {

  late UserProfile _thisProfile;
  late SwiperController _swipeController;
  late int _realMediaLength;

  @override
  void initState() {
    _thisProfile = widget.userProfile;
    _swipeController = SwiperController();
    _realMediaLength = _thisProfile.images!.length;
    super.initState();
  }

  @override
  void dispose() {
    _swipeController.dispose();
    super.dispose();
  }

  int getCurrentIndex() {
    return _swipeController.index;
  }

  void nextPage() {
    _swipeController.next();
  }

  void previousPage() {
    _swipeController.previous();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: deviceWidth,
      height: widget.height != null ? widget.height : utils.getProfileMediaHeight() - 20,
      child: Swiper(
        itemCount: _realMediaLength == 0 ? 1 : _realMediaLength,
        autoplay: widget.shouldAutoPlay,
        autoplayDisableOnInteraction: true,
        onIndexChanged: (int index) {
          if (widget.changePictureIndex != null) {
            widget.changePictureIndex!(index);
          }
        },
        /// if number of images are smaller than or equal to 1, then no need scroll
        physics: _realMediaLength <= 1 ? NeverScrollableScrollPhysics() : null,
        autoplayDelay: 10000,
        controller: _swipeController,
        itemWidth: deviceWidth,
        itemBuilder: (ctx, newIndex) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(commonRadius),
              child: ImageHolder(
                index: newIndex,
                width: deviceWidth,
                name: utils.getFullName(_thisProfile),
                userColor: utils.convertStrToColor(_thisProfile.id!),
                imageList: _thisProfile.images!,
                heroKey: UniqueKey().toString(),
                disableOnTap: widget.disableTap,
                nextPage: () {
                  if (_realMediaLength > 1) {
                    _swipeController.next();
                  }
                },
                previousPage: () {
                  if (_realMediaLength > 1) {
                    _swipeController.previous();
                  }
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
