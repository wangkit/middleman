import 'dart:async';
import 'dart:convert';
import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/landing/landing_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:choicestory/apps/home/main/main_page.dart';
import 'package:choicestory/apps/splash/soy_splash_page.dart';
import 'package:soy_common/color/color.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/analysis.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/remote_config_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'dart:io';
import 'package:choicestory/constants/const.dart';
import 'package:soy_common/models/env.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  /// This is basically useless now since we have getInitialMessage at our disposal

  print("Handling a background message: ${message.data}");
}

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  Future<void> initSharedPreference() async {
    await utils.loadPref();
    await commonUtils.init();
    if (qp.id!.isNotEmpty) {
      DocumentSnapshot _profile = await firestore.collection(FirestoreCollections.profiles).doc(qp.id!).get();
      QuerySnapshot _match = await firestore.collection(FirestoreCollections.matches).where(MatchCollection.id, isEqualTo: qp.id!).get();
      QuerySnapshot _greet = await firestore.collection(FirestoreCollections.greets).where(MatchCollection.id, isEqualTo: qp.id!).get();
      DocumentSnapshot _preference = await firestore.collection(FirestoreCollections.preferences).doc(qp.id!).get();
      if (_match.docs.isNotEmpty) {
        for (QueryDocumentSnapshot match in _match.docs) {
          likeOrPassOrGreetedIds.add((match.data()! as Map)[MatchCollection.targetId]);
        }
      }
      if (_greet.docs.isNotEmpty) {
        for (QueryDocumentSnapshot greet in _greet.docs) {
          likeOrPassOrGreetedIds.add((greet.data()! as Map)[GreetCollection.targetId]);
        }
      }
      if (_profile.exists) {
        qp = UserProfile.fromMapToProfile(_profile.data() as Map);
        if (_preference.exists) {
          myPreference = UserPreference.fromMapToPreference(_preference.data() as Map);
        }
      } else {
        utils.logoutClearData();
      }
    }
  }

  Future<bool> initDynamicLink() async {

    Future<bool> _handleShareGame(Uri dl) async {
      return true;
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData? dynamicLink) async {
          if (dynamicLink != null) {
            return await _handleShareGame(dynamicLink.link);
          }
          return true;
        },
        onError: (OnLinkErrorException e) async {
          debugPrint('onLinkError');
          debugPrint(e.message);
        }
    );

    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    if (data != null) {
      return await _handleShareGame(data.link);
    }
    return true;
  }

  void _initCalls() {
    /// Step one: Init instances and fetch remote
    apiBaseUrl = env!.baseUrl;
    bool needForceUpdate = false;
    Map _educationMap = json.decode(remoteConfig!.getValue(RemoteConfigKey.education).asString());
    Map _religionMap = json.decode(remoteConfig!.getValue(RemoteConfigKey.religions).asString());
    Map _professionsMap = json.decode(remoteConfig!.getValue(RemoteConfigKey.professions).asString());
    Map _countryMap = json.decode(remoteConfig!.getValue(RemoteConfigKey.country).asString());
    educationLevelList = _educationMap[RemoteConfigKey.education];
    countryList = _countryMap[RemoteConfigKey.country];
    religionList = _religionMap[RemoteConfigKey.religions];
    professionList = _professionsMap[RemoteConfigKey.professions];
    WidgetsFlutterBinding.ensureInitialized();
    /// Step three: Init shared preference
    initSharedPreference().then((_) {
      Future.wait([
        PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
          /// Using packageInfo.appName here will get a null value for iOS.
          /// Since we will not change the app name in the future, simply hardcoding the app name here solves the problem.
          appName = myAppName;
          versionCode = int.parse(packageInfo.buildNumber);
          versionName = packageInfo.version;
          int installedVersionNumber = int.parse(versionName.toString().trim().replaceAll(".", ""));
          int requiredVersionNumber = int.parse(remoteConfig!.getString(RemoteConfigKey.minimumVersion).toString().trim().replaceAll(".", ""));
          needForceUpdate = installedVersionNumber < requiredVersionNumber && remoteConfig!.getBool(RemoteConfigKey.enforceMinimumVersion);
        }),
      ]).then((_) {
        if (!needForceUpdate) {
          /// Step nine: Navigate to forum app.main page if myProfile is got and this app does not need to update
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) async {
            /// Init firebase messaging instance
            firebaseMessaging = FirebaseMessaging.instance;
            /// Register firebase permission moved to main page after logged in
            FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
            FirebaseMessaging.onMessage.listen((RemoteMessage message) {
              print('Got a message whilst in the foreground!');
              print('Message data: ${message.data}');
              if (message.notification != null) {
                print('Message also contained a notification: ${message.notification}');
              }
            });
            /// Go to dynamic link
            /// Can't use utils.hasLoggedIn here because we haven't signed in to firebase yet
            if (!qp.email!.isNullOrEmpty && !qp.authPw!.isNullOrEmpty && !qp.id!.isNullOrEmpty) {
              await apiManager.signInToFirebase();
              /// Check if this is opened from a notification
              RemoteMessage? initialMessage = await FirebaseMessaging.instance.getInitialMessage();
              // if (initialMessage != null && initialMessage.data.containsKey("screen")) {
              //   String _targetScreen = initialMessage.data["screen"];
              //   String? _gameId;
              //   switch (_targetScreen) {
              //     case "gameDetails":
              //       getRoute.navigateToAndPopAll(MainPage(
              //         showSpotlight: false,
              //       ));
              //       apiManager.logAnalysis(Analysis.openAppByNotification);
              //       _gameId = initialMessage.data[GameCollection.gameId];
              //       var _thisGame = await firestore.collection(FirestoreCollections.games).doc(_gameId).get();
              //       return;
              //   }
              // }
              bool _needContinueToMainPage = await initDynamicLink();
              if (_needContinueToMainPage) {
                /// If normal open, go to main page
                getRoute.navigateToAndPopAll(MainPage());
              }
            } else {
              getRoute.navigateToAndPopAll(LandingPage());
            }
          });
        } else if (needForceUpdate) {
//          showForceUpdateDialog();
        } else {
          utils.toast(TextData.connectionFailed, isWarning: true);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return SoySplashPage(
      buildFunctions: () {
        deviceWidth = MediaQuery.of(context).size.width;
        deviceHeight = MediaQuery.of(context).size.height;
      },
      backgroundColorCode: utils.getHexCodeByColor(littleGrey),
      initFunctions: () => _initCalls(),
      assetPath: "assets/lottie/thunder.json",
    );
  }
}