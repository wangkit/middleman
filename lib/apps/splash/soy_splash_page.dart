import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:supercharged/supercharged.dart';

class SoySplashPage extends StatefulWidget {

  final Function initFunctions;
  final Function buildFunctions;
  final String backgroundColorCode;
  final String assetPath;

  SoySplashPage({
    Key? key,
    required this.initFunctions,
    required this.buildFunctions,
    required this.backgroundColorCode,
    required this.assetPath,
  }) : super(key: key);

  @override
  _SoySplashPageState createState() => _SoySplashPageState();
}

class _SoySplashPageState extends State<SoySplashPage> {

  Color? backgroundColor;
  String? backgroundColorCode;

  @override
  void initState() {
    backgroundColorCode = widget.backgroundColorCode;
    backgroundColor = Colors.grey;
    if (backgroundColorCode != null) {
      if (!backgroundColorCode!.contains("#")) {
        backgroundColorCode = "#" + backgroundColorCode!;
      }
      if (backgroundColorCode!.length > 7) {
        backgroundColorCode = backgroundColorCode!.replaceFirst("#ff", "#");
      }
      backgroundColor = backgroundColorCode!.toColor();
    }
    super.initState();
    widget.initFunctions();
  }

  @override
  Widget build(BuildContext context) {

    widget.buildFunctions();

    return Scaffold(
      body: Container(
        color: backgroundColor,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Center(
                child: Hero(
                  tag: UniqueKey(),
                  child: Lottie.asset(
                    widget.assetPath,
                    width: 150,
                    height: 150,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SafeArea(
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.grey[400]!),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
