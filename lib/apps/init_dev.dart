import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:choicestory/choice_story_root.dart';
import 'package:soy_common/models/env.dart';
import '../constants/const.dart';

void main() async {
  BuildEnvironment.init(
    flavor: BuildFlavor.development,
    baseUrl: "https://us-central1-choice-story-8533a.cloudfunctions.net",
  );
  WidgetsFlutterBinding.ensureInitialized();
  assert(env != null);
  await Firebase.initializeApp();
  remoteConfig = RemoteConfig.instance;
  await remoteConfig!.setConfigSettings(RemoteConfigSettings(
    fetchTimeout: Duration(seconds: 10),
    minimumFetchInterval: Duration(seconds: 1),
  ));
  await remoteConfig!.fetchAndActivate();
  prefs = await SharedPreferences.getInstance();
  runApp(ChoiceStoryRoot());
}