import 'package:choicestory/apps/setup/setup_page.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/textfield/email_textfield.dart';
import 'package:choicestory/widgets/textfield/password_textfield.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';


class LoginPage extends StatefulWidget {

  LoginPage({
    Key? key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  late TextEditingController emailController;
  late TextEditingController passwordController;
  late FocusNode emailNode;
  late FocusNode passwordNode;
  GlobalKey<FormState> formKey = GlobalKey();

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    emailNode = FocusNode();
    passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    emailNode.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          stops: [0.1, 0.9],
          colors: [
            Colors.amber[300]!,
            Colors.amber[300]!,
          ],
        ),
      ),
      child: Scaffold(
        appBar: CustomAppBar(
          backgroundColor: Colors.amber[300]!,
          leading: IconButton(
            onPressed: () => getRoute.pop(),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        body: Center(
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Tooltip(
                    message: myAppName,
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(commonRadius * 3),
                      ),
                      shadowColor: Colors.amber[900],
                      color: Colors.amber[100],
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(200),
                        child: Image.asset(
                          "assets/icon/middleman_without_bg.png",
                          width: 200,
                          height: 200,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 30,),
                  EmailTextField(
                    controller: emailController,
                    focusNode: emailNode,
                    nextFocusNode: passwordNode,
                    textAlign: TextAlign.center,
                    borderRadius: commonRadius * 2,
                    cursorColor: Colors.white,
                    borderColor: Colors.amber[400],
                    needSuffix: false,
                    labelHintStyle: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.15,
                    ),
                  ),
                  SizedBox(height: 5,),
                  PasswordTextField(
                    controller: passwordController,
                    focusNode: passwordNode,
                    nextFocusNode: emailNode,
                    borderColor: Colors.amber[400],
                    needSuffix: false,
                    cursorColor: Colors.white,
                    textAlign: TextAlign.center,
                    borderRadius: commonRadius * 2,
                    labelHintStyle: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.15,
                    ),
                  ),
                  SizedBox(height: 30,),
                  LongElevatedButton(
                    splashColor: Colors.blue,
                    color: Colors.white,
                    onPressed: () async {
                      if (formKey.currentState!.validate()) {
                        await apiManager.signIn(emailController.text, passwordController.text);
                      }
                    },
                    title: TextData.signIn.allInCaps,
                    textStyle: TextStyle(
                      fontSize: 18,
                      letterSpacing: 1.15,
                      color: Colors.yellow[800],
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 15,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Don't have an account?",
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.white,
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          getRoute.navigateToAndReplace(SetupPage());
                        },
                        child: Text(
                          "Sign Up Now",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
