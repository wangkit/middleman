import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/custom_date_picker.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/details/review_card.dart';
import 'package:choicestory/widgets/details/review_title.dart';
import 'package:choicestory/widgets/drop_down/generic_drop_down_field.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/textfield/generic_textfield.dart';
import 'package:choicestory/widgets/textfield/generic_type_ahead_textfield.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:group_button/group_button.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class EditProfilePage extends StatefulWidget {

  EditProfilePage({
    Key? key,
  }) : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> with AutomaticKeepAliveClientMixin<EditProfilePage> {

  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late TextEditingController _lastNameController;
  late TextEditingController _firstNameController;
  late TextEditingController _countryController;
  late TextEditingController _cityController;
  late TextEditingController _professionController;
  late TextEditingController _personalityController;
  late DateTime _myBDay;
  late String? _physicalGender;
  late String? _genderIdentity;
  late String _religion;
  late String _education;
  late String _relationshipStatus;
  late String? _zodiacSign;
  late double? _longitude;
  late double? _latitude;

  void _setToCurrentProfile() {
    _relationshipStatus = RelationshipStatusExtension.names[qp.relationshipStatus]!;
    _myBDay = qp.birthday!;
    _physicalGender = qp.physicalGender != null ? GenderExtension.names[qp.physicalGender]! : "";
    _genderIdentity = qp.genderIdentity != null ? GenderExtension.names[qp.genderIdentity]! : "";
    _religion = qp.religion != null ? qp.religion! : "";
    _education = qp.education != null ? qp.education! : "";
    _professionController.text = qp.profession ?? "";
    _personalityController.text = qp.personality ?? "";
    _cityController.text = qp.city ?? "";
    _countryController.text = qp.country ?? "";
    _firstNameController.text = qp.firstName ?? "";
    _lastNameController.text = qp.lastName ?? "";
    _longitude = qp.longitude;
    _latitude = qp.latitude;
    _zodiacSign = ZodiacSignExtension.names[qp.zodiacSign]!;
  }

  @override
  void initState() {
    _professionController = TextEditingController();
    _cityController = TextEditingController();
    _personalityController = TextEditingController();
    _countryController = TextEditingController();
    _firstNameController = TextEditingController();
    _lastNameController = TextEditingController();
    _setToCurrentProfile();
    super.initState();
  }

  @override
  void dispose() {
    _professionController.dispose();
    _cityController.dispose();
    _personalityController.dispose();
    _countryController.dispose();
    _firstNameController.dispose();
    _lastNameController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    super.build(context);

    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        heroTag: UniqueKey().toString(),
        onPressed: () async {
          if (_formKey.currentState!.validate()) {
            try {
              List<Location> _confirmLocation = await locationFromAddress(_cityController.text + ", " + _countryController.text);
              _latitude = _confirmLocation.first.latitude;
              _longitude = _confirmLocation.first.longitude;
            } on Exception catch (e) {
              utils.toast("Sorry, this address is not available", isWarning: true);
              return;
            }
            await apiManager.updateProfile(UserProfile(
              city: _cityController.text.trim(),
              country: _countryController.text.trim(),
              birthday: _myBDay,
              age: utils.getAge(_myBDay),
              profession: _professionController.text.trim(),
              education: _education,
              religion: _religion,
              personality: _personalityController.text.trim(),
              physicalGender: GenderExtension.enums[_physicalGender],
              genderIdentity: GenderExtension.enums[_genderIdentity],
              relationshipStatus: RelationshipStatusExtension.enums[_relationshipStatus],
              lastName: _lastNameController.text.trim(),
              firstName: _firstNameController.text.trim(),
              longitude: _longitude,
              latitude: _latitude,
              zodiacSign: ZodiacSignExtension.enums[_zodiacSign],
            ));
            qp.city = _cityController.text.trim();
            qp.country = _countryController.text.trim();
            qp.birthday = _myBDay;
            qp.age = utils.getAge(_myBDay);
            qp.profession = _professionController.text.trim();
            qp.education = _education;
            qp.religion = _religion;
            qp.personality = _personalityController.text.trim();
            qp.physicalGender = GenderExtension.enums[_physicalGender];
            qp.genderIdentity = GenderExtension.enums[_genderIdentity];
            qp.relationshipStatus = RelationshipStatusExtension.enums[_relationshipStatus];
            qp.lastName = _lastNameController.text.trim();
            qp.firstName = _firstNameController.text.trim();
            qp.longitude = _longitude;
            qp.latitude = _latitude;
            qp.zodiacSign = ZodiacSignExtension.enums[_zodiacSign];
            getRoute.pop();
            utils.toast(TextData.updateSuccessfully);
          }
        },
        child: Icon(
          Icons.done,
          color: Colors.white,
        ),
      ),
      appBar: CustomAppBar(
        automaticallyImplyLeading: true,
        centerTitle: false,
        title: Text(
          "Edit Profile",
        ),
        actions: [
          IconButton(
            onPressed: () {
              getBinaryDialog("Reset Profile", TextData.areYouSure, () {
                getRoute.pop();
                _setToCurrentProfile();
                refreshThisPage();
              });
            },
            icon: Icon(
              Icons.redo,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10,),
              ReviewTitle(
                title: "Name",
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      SizedBox(height: 10,),
                      GenericTextField(
                        controller: _firstNameController,
                        borderRadius: commonRadius,
                        hint: "First Name",
                        maxLines: 1,
                        textInputType: TextInputType.name,
                        textCapitalization: TextCapitalization.words,
                        inputFormatter: [
                          FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                        ],
                        autofillHints: [AutofillHints.givenName],
                        validation: (String newValue) {
                          if (newValue.isNullOrEmpty) {
                            return "Name must not be empty";
                          }
                          if (newValue.contains(" ")) {
                            return "Name must not contain white space";
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 10,),
                      GenericTextField(
                        controller: _lastNameController,
                        borderRadius: commonRadius,
                        hint: "Last Name",
                        maxLines: 1,
                        textInputType: TextInputType.name,
                        textCapitalization: TextCapitalization.words,
                        inputFormatter: [
                          FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                        ],
                        autofillHints: [AutofillHints.familyName],
                        validation: (String newValue) {
                          if (newValue.isNullOrEmpty) {
                            return "Name must not be empty";
                          }
                          if (newValue.contains(" ")) {
                            return "Name must not contain white space";
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 10,),
                    ],
                  );
                },
              ),
              SizedBox(height: 10,),
              ReviewTitle(
                title: "Birthday",
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            _myBDay.year.toString() + "-",
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          Text(
                            _myBDay.month.toString() + "-",
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          Text(
                            _myBDay.day.toString(),
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          Container(width: 10,),
                          Theme(
                            data: ThemeData(
                              primaryColorBrightness: Brightness.dark,
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.edit,
                                color: mainColor,
                              ),
                              onPressed: () {
                                showCustomDatePicker(
                                  context: getRoute.getContext(),
                                  initialDate: _myBDay,
                                  firstDate: DateTime(1950, 1, 1),
                                  lastDate: DateTime.now().toLocal(),
                                  cancelText: TextData.cancel,
                                  confirmText: TextData.confirm,
                                  initialEntryMode: DatePickerEntryMode.calendar,
                                  initialDatePickerMode: DatePickerMode.year,
                                ).then((DateTime? date) {
                                  if (date != null) {
                                    _myBDay = date;
                                    customState(() {});
                                  }
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
              SizedBox(height: 10,),
              ReviewTitle(
                title: "Location",
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      GenericTypeAheadTextField(
                        dataList: countryList,
                        controller: _countryController,
                        borderRadius: commonRadius,
                        hint: "Country",
                        maxLines: 1,
                        canEmpty: false,
                        textInputType: TextInputType.streetAddress,
                        focusNode: FocusNode(),
                      ),
                      GenericTextField(
                        controller: _cityController,
                        borderRadius: commonRadius,
                        hint: "City",
                        maxLines: 1,
                        textInputType: TextInputType.streetAddress,
                        textCapitalization: TextCapitalization.words,
                        inputFormatter: [
                          FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                        ],
                        autofillHints: [AutofillHints.addressCity],
                        validation: (String newValue) {
                          if (newValue.isNullOrEmpty) {
                            return "City must not be empty";
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 10,),
                    ],
                  );
                },
              ),
              SizedBox(height: 10,),
              ReviewTitle(
                title: "Gender",
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      GenericStringDropDownField(
                        text: "Physical Gender",
                        onChanged: (String newValue) {
                          customState(() {
                            _physicalGender = newValue;
                            _genderIdentity = newValue;
                          });
                        },
                        canEmpty: false,
                        list: GenderExtension.valueList.toList(),
                        value: _physicalGender!,
                      ),
                      GenericStringDropDownField(
                        text: "Gender Identity",
                        onChanged: (String newValue) {
                          customState(() {
                            _genderIdentity = newValue;
                          });
                        },
                        canEmpty: false,
                        list: GenderExtension.valueList.toList(),
                        value: _genderIdentity!,
                      ),
                    ],
                  );
                },
              ),
              SizedBox(height: 10,),
              ReviewTitle(
                title: TextData.personality,
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      SizedBox(height: 10,),
                      GenericTextField(
                        controller: _personalityController,
                        borderRadius: commonRadius,
                        hint: TextData.personality,
                        textInputAction: TextInputAction.newline,
                        textInputType: TextInputType.multiline,
                        textCapitalization: TextCapitalization.words,
                        maxLines: null,
                        maxLength: AppConfig.personalityMaxLength,
                        validation: (String newValue) {
                          if (newValue.isNullOrEmpty) {
                            return "${TextData.personality} must not be empty";
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 10,),
                    ],
                  );
                },
              ),
              SizedBox(height: 10,),
              ReviewTitle(
                title: "Relationship",
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      SizedBox(height: 20,),
                      FadeIn(
                        child: GroupButton(
                          key: Key(utils.generatePassword()),
                          onSelected: (int index, bool isSelected) {
                            _relationshipStatus = RelationshipStatusExtension.valueList.toList()[index];
                          },
                          selectedButton: RelationshipStatusExtension.valueList.toList().indexOf(_relationshipStatus),
                          spacing: 10,
                          selectedColor: appIconColor,
                          selectedTextStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                          buttons: RelationshipStatusExtension.valueList.toList(),
                          borderRadius: BorderRadius.circular(commonRadius),
                        ),
                      ),
                      SizedBox(height: 10,),
                    ],
                  );
                },
              ),
              SizedBox(height: 10,),
              ReviewTitle(
                title: "Others",
              ),
              StatefulBuilder(
                builder: (ctx, customState) {
                  return ReviewCard(
                    children: [
                      GenericTypeAheadTextField(
                        dataList: professionList,
                        controller: _professionController,
                        borderRadius: commonRadius,
                        hint: "Profession",
                        maxLines: 1,
                        canEmpty: true,
                        textInputType: TextInputType.text,
                        focusNode: FocusNode(),
                      ),
                      GenericStringDropDownField(
                        text: "Education",
                        onChanged: (String newValue) {
                          customState(() {
                            _education = newValue;
                          });
                        },
                        list: educationLevelList,
                        value: _education,
                      ),
                      GenericStringDropDownField(
                        text: "Religion",
                        onChanged: (String newValue) {
                          customState(() {
                            _religion = newValue;
                          });
                        },
                        list: religionList,
                        value: _religion,
                      ),
                    ],
                  );
                },
              ),
              SizedBox(height: 80,),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
