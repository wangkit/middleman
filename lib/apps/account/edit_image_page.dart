import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/apps/home/main/main_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/analysis.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/remote_config_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/custom_date_picker.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/custom_icon_stepper/custom_icon_stepper.dart';
import 'package:choicestory/widgets/drop_down/generic_drop_down_field.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/textfield/password_textfield.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoding/geocoding.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/scrollable_footer/scrollable_footer.dart';
import 'package:choicestory/widgets/textfield/email_textfield.dart';
import 'package:choicestory/widgets/textfield/generic_textfield.dart';
import 'package:choicestory/widgets/textfield/generic_type_ahead_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:group_button/group_button.dart';
import 'package:im_stepper/stepper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:reorderables/reorderables.dart';
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';
import 'package:validators2/validators.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class EditImagePage extends StatefulWidget {

  EditImagePage({
    Key? key,
  }) : super(key: key);

  @override
  _EditImagePageState createState() => _EditImagePageState();
}

class _EditImagePageState extends State<EditImagePage> with TickerProviderStateMixin {

  late List<dynamic> _imageFiles;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _imageFiles = [];
    for (int index = 0; index < AppConfig.allowedPicturePerProfile; index++) {
      if (qp.images != null && qp.images!.length - 1 >= index) {
        _imageFiles.add(qp.images![index]);
      } else {
        _imageFiles.add(null);
      }
    }
    super.initState();
  }

  Future<void> _pickImage(ImageSource source) async {
    bool _allFull = false;
    if (source == ImageSource.camera) {
      /// If camera, use image picker
      PickedFile? _pickedFile = await imagePicker.getImage(
        source: source,
      );
      if (_pickedFile != null) {
        for (int i = 0; i < _imageFiles.length; i++) {
          if (_imageFiles[i] == null) {
            /// Replace null if have null
            _allFull = false;
            _imageFiles.removeAt(i);
            _imageFiles.insert(i, File(_pickedFile.path));
            break;
          } else {
            _allFull = true;
          }
        }
        if (_allFull) {
          /// Replace first if all have images
          _imageFiles.removeAt(0);
          _imageFiles.insert(0, File(_pickedFile.path));
        }
      }
    } else {
      List<int> _emptySlot = [];
      /// else use multiple image picker
      FilePickerResult? _results = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.image,
      );
      if (_results != null) {
        if (_results.files.length > AppConfig.allowedPicturePerProfile) {
          /// Make sure picked file number is acceptable
          _results.files.sublist(0, AppConfig.allowedPicturePerProfile);
        }
        for (int i = 0; i < _imageFiles.length; i++) {
          if (_imageFiles[i] == null) {
            _emptySlot.add(i);
          }
        }
        for (int i = 0; i < _results.files.length; i++) {
          if (_emptySlot.length >= i + 1) {
            _imageFiles.removeAt(_emptySlot[i]);
            _imageFiles.insert(_emptySlot[i], File(_results.files[i].path!));
          } else {
            _imageFiles.removeAt(_results.files.length - i - 1);
            _imageFiles.insert(_results.files.length - i - 1, File(_results.files[i].path!));
          }
        }
      }
    }
    refreshThisPage();
  }

  bool _hasNewOrChanged() {
    String _original = "";
    String _now = "";
    for (int i = 0; i < _imageFiles.length; i++) {
      if (_imageFiles[i] is File) {
        return true;
      }
    }
    if (qp.images != null) {
      qp.images!.forEach((element) {
        _original += element;
      });
      _imageFiles.forEach((element) {
        if (element is String) {
          _now += element;
        }
      });
    }
    return _original != _now;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      floatingActionButton: !_hasNewOrChanged() ? null : FloatingActionButton.extended(
        onPressed: () async {
          Dialogs.showLoadingDialog(title: "Uploading...");
          List<String> _newFilePaths = [];
          List<String> _newImages = [];
          for (dynamic file in _imageFiles) {
            if (file is File) {
              if (!_newFilePaths.contains(file.path)) {
                String? _link = await uploadManager.uploadFileToFirebase(file);
                if (_link != null) {
                  _newFilePaths.add(file.path);
                  _newImages.add(_link);
                }
              }
            } else if (file is String) {
              _newImages.add(file);
            }
          }
          if (_newImages.isNotEmpty) {
            qp.images = _newImages;
            await apiManager.updateImage(qp.images!);
          }
          getRoute.pop();
          getRoute.pop();
        },
        backgroundColor: appIconColor,
        label: Text(
          "Update",
        ),
        icon: Icon(
          Icons.check,
        ),
      ),
      appBar: CustomAppBar(
        title: Text(
          "Edit Image",
        ),
      ),
      body: ReorderableWrap(
        runSpacing: 8.0,
        padding: EdgeInsets.symmetric(vertical: 24.0),
        children: List.generate(AppConfig.allowedPicturePerProfile, (index) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 6.0),
            child: GestureDetector(
              onTap: () {
                getGenericBottomSheet(context, [
                  _imageFiles[index] != null ? Container() : BottomSheetButton(
                    onTap: () {
                      _pickImage(
                        ImageSource.camera,
                      );
                    },
                    iconData: Icons.add_a_photo_outlined,
                    label: "From Camera",
                  ),
                  _imageFiles[index] != null ? Container() : BottomSheetButton(
                    onTap: () {
                      _pickImage(
                        ImageSource.gallery,
                      );
                    },
                    iconData: Icons.perm_media_outlined,
                    label: "From Gallery",
                  ),
                  BottomSheetButton(
                    onTap: () {
                      if (_imageFiles[index] != null) {
                        _imageFiles.removeAt(index);
                        if (_imageFiles.length != AppConfig.allowedPicturePerProfile) {
                          _imageFiles.add(null);
                        }
                        refreshThisPage();
                      }
                    },
                    iconData: Icons.clear_outlined,
                    label: "Remove Image",
                  ),
                ]);
              },
              child: Badge(
                position: BadgePosition(
                  top: 0,
                  end: 5,
                ),
                badgeColor: appIconColor,
                badgeContent: Text(
                  (index + 1).toString(),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(commonRadius),
                    border: Border.all(
                      color: appIconColor,
                    ),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(commonRadius),
                    child: _imageFiles[index] != null ? _imageFiles[index] is String ? CachedNetworkImage(
                      imageUrl: _imageFiles[index],
                      fit: BoxFit.cover,
                      width: deviceWidth / 3.4,
                      height: deviceWidth / 2,
                    ) : Image.file(
                      _imageFiles[index]!,
                      fit: BoxFit.cover,
                      width: deviceWidth / 3.4,
                      height: deviceWidth / 2,
                    ) : Container(
                      width: deviceWidth / 3.4,
                      height: deviceWidth / 2,
                      child: Icon(
                        MdiIcons.image,
                        color: mainColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
        onReorder: (int oldIndex, int newIndex) {
          dynamic _tempFile = _imageFiles[oldIndex];
          _imageFiles.removeAt(oldIndex);
          _imageFiles.insert(newIndex, _tempFile);
          refreshThisPage();
        },
      ),
    );
  }
}
