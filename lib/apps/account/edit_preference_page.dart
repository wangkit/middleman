import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/details/review_card.dart';
import 'package:choicestory/widgets/details/review_title.dart';
import 'package:choicestory/widgets/drop_down/generic_drop_down_field.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/textfield/generic_type_ahead_textfield.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:group_button/group_button.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class EditPreferencePage extends StatefulWidget {

  EditPreferencePage({
    Key? key,
  }) : super(key: key);

  @override
  _EditPreferencePageState createState() => _EditPreferencePageState();
}

class _EditPreferencePageState extends State<EditPreferencePage> with AutomaticKeepAliveClientMixin<EditPreferencePage> {

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late double _preferredDistanceValue;
  late RangeValues _preferredAgeValue;
  late ZodiacSign? _zodiacSign;
  late RelationshipStatus? _relationshipStatus;
  late String _physicalGender;
  late String _genderIdentity;
  late TextEditingController _professionController;
  late FocusNode _professionFocusNode;
  late String _religion;
  late String _education;

  void _setToCurrentPreference() {
    _preferredDistanceValue = myPreference.distance != null ? myPreference.distance! : AppConfig.minDistance;
    _preferredAgeValue = RangeValues(
      myPreference.minAge != null ? myPreference.minAge!.toDouble() : AppConfig.minAge.toDouble(),
      myPreference.maxAge != null ? myPreference.maxAge!.toDouble() : AppConfig.maxAge.toDouble(),
    );
    _zodiacSign = myPreference.zodiacSign;
    _relationshipStatus = myPreference.relationshipStatus;
    _physicalGender = myPreference.physicalGender != null ? GenderExtension.names[myPreference.physicalGender]! : "";
    _genderIdentity = myPreference.genderIdentity != null ? GenderExtension.names[myPreference.genderIdentity]! : "";
    _religion = myPreference.religion != null ? myPreference.religion! : "";
    _education = myPreference.education != null ? myPreference.education! : "";
    _professionController.text = myPreference.profession ?? "";
  }

  @override
  void initState() {
    _professionController = TextEditingController(
      text: myPreference.profession,
    );
    _professionFocusNode = FocusNode();
    _setToCurrentPreference();
    super.initState();
  }

  @override
  void dispose() {
    _professionController.dispose();
    _professionFocusNode.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _textDisplay(String value, {Color? textColor}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
      child: Text(
        value,
        style: TextStyle(
          color: textColor,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    super.build(context);

    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          myPreference = UserPreference(
            distance: _preferredDistanceValue,
            physicalGender: GenderExtension.enums[_physicalGender],
            genderIdentity: GenderExtension.enums[_genderIdentity],
            maxAge: _preferredAgeValue.end.toInt(),
            minAge: _preferredAgeValue.start.toInt(),
            zodiacSign: _zodiacSign,
            profession: _professionController.text.trim(),
            religion: _religion,
            education: _education,
            relationshipStatus: _relationshipStatus,
          );
          await apiManager.updatePreference(myPreference);
          getRoute.pop();
          utils.toast(TextData.updateSuccessfully);
        },
        child: Icon(
          Icons.done,
          color: Colors.white,
        ),
      ),
      appBar: CustomAppBar(
        automaticallyImplyLeading: true,
        centerTitle: false,
        title: Text(
          "Edit Preference",
        ),
        actions: [
          IconButton(
            onPressed: () {
              getBinaryDialog("Reset Preference", TextData.areYouSure, () {
                getRoute.pop();
                _setToCurrentPreference();
                refreshThisPage();
              });
            },
            icon: Icon(
              Icons.redo,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            ReviewTitle(
              title: "Distance",
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ReviewCard(
                  children: [
                    SizedBox(height: 10,),
                    Slider.adaptive(
                      value: _preferredDistanceValue,
                      min: AppConfig.minDistance.toDouble(),
                      max: AppConfig.maxDistance.toDouble(),
                      divisions: (AppConfig.maxDistance - AppConfig.minDistance).toInt(),
                      activeColor: appIconColor,
                      inactiveColor: unfocusedColor,
                      onChanged: (double value) {
                        customState(() {
                          _preferredDistanceValue = value;
                        });
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: Icon(
                            Icons.remove,
                            color: mainColor,
                          ),
                          onPressed: () {
                            if (_preferredDistanceValue > AppConfig.minDistance) {
                              customState(() {
                                _preferredDistanceValue--;
                              });
                            }
                          },
                        ),
                        _textDisplay("At most ${_preferredDistanceValue.round().toString()}${AppConfig.distanceSuffix} away from you"),
                        IconButton(
                          icon: Icon(
                            Icons.add,
                            color: mainColor,
                          ),
                          onPressed: () {
                            if (_preferredDistanceValue < AppConfig.maxDistance) {
                              customState(() {
                                _preferredDistanceValue++;
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ],
                );
              },
            ),
            SizedBox(height: 10,),
            ReviewTitle(
              title: "Age and Zodiac Sign",
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ReviewCard(
                  children: [
                    SizedBox(height: 20,),
                    FadeIn(
                      child: GroupButton(
                        key: Key(utils.generatePassword()),
                        onSelected: (int index, bool isSelected) {
                          _zodiacSign = ZodiacSignExtension.enums[ZodiacSignExtension.valueList.toList()[index]];
                        },
                        selectedButton: _zodiacSign != null ? ZodiacSignExtension.valueList.toList().indexOf(ZodiacSignExtension.names[_zodiacSign]!) : 0,
                        spacing: 10,
                        selectedColor: appIconColor,
                        selectedTextStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        buttons: ZodiacSignExtension.valueList.toList(),
                        borderRadius: BorderRadius.circular(commonRadius),
                      ),
                    ),
                    SizedBox(height: 20,),
                    RangeSlider(
                      values: _preferredAgeValue,
                      min: AppConfig.minAge.toDouble(),
                      max: AppConfig.maxAge.toDouble(),
                      divisions: AppConfig.maxAge - AppConfig.minAge,
                      activeColor: appIconColor,
                      inactiveColor: unfocusedColor,
                      onChanged: (RangeValues value) {
                        customState(() {
                          _preferredAgeValue = value;
                        });
                      },
                    ),
                    _textDisplay("Age ${_preferredAgeValue.start.round()} - ${_preferredAgeValue.end.round()}"),
                    SizedBox(height: 10,),
                  ],
                );
              },
            ),
            SizedBox(height: 10,),
            ReviewTitle(
              title: "Gender",
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ReviewCard(
                  children: [
                    SizedBox(height: 20,),
                    GenericStringDropDownField(
                      text: "Physical Gender",
                      onChanged: (String newValue) {
                        customState(() {
                          _physicalGender = newValue;
                          _genderIdentity = newValue;
                        });
                      },
                      list: GenderExtension.valueListWithoutSpace.toList(),
                      value: _physicalGender,
                    ),
                    GenericStringDropDownField(
                      text: "Gender Identity",
                      onChanged: (String newValue) {
                        customState(() {
                          _genderIdentity = newValue;
                        });
                      },
                      list: GenderExtension.valueListWithoutSpace.toList(),
                      value: _genderIdentity,
                    ),
                    SizedBox(height: 10,),
                  ],
                );
              },
            ),
            SizedBox(height: 10,),
            ReviewTitle(
              title: "Relationship",
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ReviewCard(
                  children: [
                    SizedBox(height: 20,),
                    FadeIn(
                      child: GroupButton(
                        key: Key(utils.generatePassword()),
                        onSelected: (int index, bool isSelected) {
                          _relationshipStatus = RelationshipStatusExtension.enums[RelationshipStatusExtension.valueList.toList()[index]];
                        },
                        selectedButton: _relationshipStatus != null ? RelationshipStatusExtension.valueList.toList().indexOf(RelationshipStatusExtension.names[_relationshipStatus]!) : 0,
                        spacing: 10,
                        selectedColor: appIconColor,
                        selectedTextStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        buttons: RelationshipStatusExtension.valueList.toList(),
                        borderRadius: BorderRadius.circular(commonRadius),
                      ),
                    ),
                    SizedBox(height: 10,),
                  ],
                );
              },
            ),
            SizedBox(height: 10,),
            ReviewTitle(
              title: "Others",
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ReviewCard(
                  children: [
                    SizedBox(height: 20,),
                    GenericTypeAheadTextField(
                      dataList: professionList,
                      controller: _professionController,
                      focusNode: _professionFocusNode,
                      borderRadius: commonRadius,
                      hint: "Profession",
                      maxLines: 1,
                      canEmpty: true,
                      textInputType: TextInputType.text,
                    ),
                    GenericStringDropDownField(
                      text: "Education",
                      onChanged: (String newValue) {
                        customState(() {
                          _education = newValue;
                        });
                      },
                      list: educationLevelList,
                      value: _education,
                    ),
                    GenericStringDropDownField(
                      text: "Religion",
                      onChanged: (String newValue) {
                        customState(() {
                          _religion = newValue;
                        });
                      },
                      list: religionList,
                      value: _religion,
                    ),
                    SizedBox(height: 10,),
                  ],
                );
              },
            ),
            SizedBox(height: 80,),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
