import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/bubble/preference_bubble.dart';
import 'package:choicestory/widgets/bubble/profile_bubble.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/image/profile_media.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:preload_page_view/preload_page_view.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class ViewProfilePage extends StatefulWidget {

  final String targetId;
  final UserProfile? profile;

  ViewProfilePage({
    Key? key,
    required this.targetId,
    this.profile,
  }) : super(key: key);

  @override
  _ViewProfilePageState createState() => _ViewProfilePageState();
}

class _ViewProfilePageState extends State<ViewProfilePage> with AutomaticKeepAliveClientMixin<ViewProfilePage> {

  late Query _query;
  late Future<QuerySnapshot> _getProfile;

  @override
  void initState() {
    _query = firestore.collection(FirestoreCollections.profiles).where(UserCollection.id, isEqualTo: widget.targetId);
    _getProfile = _query.get();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      appBar: CustomAppBar(
        title: Text(
          TextData.profile.capitalize,
        ),
      ),
      body: widget.profile != null ? ProfileBubble(
        profile: widget.profile!,
        height: deviceHeight - kToolbarHeight - 20,
        showBottom: false,
      ) : FutureBuilder<QuerySnapshot>(
        future: _getProfile,
        builder: (ctx, snapshot) {

          if (snapshot.connectionState != ConnectionState.done) {
            return LoadingWidget();
          }

          if (snapshot.data == null || snapshot.data!.docs.isEmpty) {
            return CommonWidgets.emptyMessage("No such user");
          }

          Map _map = snapshot.data!.docs.first.data() as Map;
          UserProfile _thisProfile = UserProfile.fromMapToProfile(_map);

          return ProfileBubble(
            profile: _thisProfile,
            height: deviceHeight - kToolbarHeight - 20,
            showBottom: false,
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
