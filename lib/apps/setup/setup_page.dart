import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:badges/badges.dart';
import 'package:choicestory/apps/home/main/main_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/analysis.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/remote_config_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/custom_date_picker.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/custom_icon_stepper/custom_icon_stepper.dart';
import 'package:choicestory/widgets/drop_down/generic_drop_down_field.dart';
import 'package:choicestory/widgets/textfield/password_textfield.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoding/geocoding.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/scrollable_footer/scrollable_footer.dart';
import 'package:choicestory/widgets/textfield/email_textfield.dart';
import 'package:choicestory/widgets/textfield/generic_textfield.dart';
import 'package:choicestory/widgets/textfield/generic_type_ahead_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:group_button/group_button.dart';
import 'package:im_stepper/stepper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:reorderables/reorderables.dart';
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';
import 'package:validators2/validators.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class SetupPage extends StatefulWidget {

  SetupPage({
    Key? key,
  }) : super(key: key);

  @override
  _SetupPageState createState() => _SetupPageState();
}

class _SetupPageState extends State<SetupPage> with TickerProviderStateMixin {

  late PageController _pageController;
  final _formNameKey = GlobalKey<FormState>();
  final _formEmailKey = GlobalKey<FormState>();
  final _formPersonalityKey = GlobalKey<FormState>();
  final _formGenderKey = GlobalKey<FormState>();
  final _formLocationKey = GlobalKey<FormState>();
  Duration _animationDuration = Duration(milliseconds: 300);
  Curve _animationCurve = Curves.easeInToLinear;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _lastNameController;
  late TextEditingController _firstNameController;
  late TextEditingController _countryController;
  late TextEditingController _cityController;
  late TextEditingController _professionController;
  late TextEditingController _educationController;
  late TextEditingController _personalityController;
  late FocusNode _personalityFocusNode;
  late FocusNode _passwordFocusNode;
  late FocusNode _educationFocusNode;
  late FocusNode _countryFocusNode;
  late FocusNode _cityFocusNode;
  late FocusNode _professionFocusNode;
  late FocusNode _lastNameFocusNode;
  late FocusNode _firstNameFocusNode;
  late FocusNode _emailFocusNode;
  double? _latitude;
  double? _longitude;
  late DateTime _myBDay;
  late UserProfile newProfile;
  late String _physicalGender;
  late String _genderIdentity;
  late int _activeStep;
  List<File?> _imageFiles = [];

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _pageController.dispose();
    _emailController.dispose();
    _emailFocusNode.dispose();
    _cityController.dispose();
    _firstNameController.dispose();
    _lastNameController.dispose();
    _educationController.dispose();
    _professionController.dispose();
    _professionFocusNode.dispose();
    _educationFocusNode.dispose();
    _firstNameFocusNode.dispose();
    _lastNameFocusNode.dispose();
    _passwordController.dispose();
    _countryController.dispose();
    _cityFocusNode.dispose();
    _countryFocusNode.dispose();
    _personalityController.dispose();
    _passwordFocusNode.dispose();
    _personalityFocusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _physicalGender = "";
    _genderIdentity = "";
    _latitude = null;
    _longitude = null;
    newProfile = UserProfile();
    _myBDay = DateTime.now().subtract(Duration(days: 1)).toLocal();
    _pageController = PageController();
    _emailController = TextEditingController();
    _lastNameController = TextEditingController();
    _passwordController = TextEditingController();
    _firstNameController = TextEditingController();
    _cityController = TextEditingController();
    _countryController = TextEditingController();
    _educationController = TextEditingController();
    _professionController = TextEditingController();
    _personalityController = TextEditingController();
    _personalityFocusNode = FocusNode();
    _cityFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    _educationFocusNode = FocusNode();
    _countryFocusNode = FocusNode();
    _professionFocusNode = FocusNode();
    _firstNameFocusNode = FocusNode();
    _lastNameFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _activeStep = 0;
    resetImageFiles();
    super.initState();
  }

  void _nextPage({bool unfocus = true}) {
    if (unfocus) {
      utils.quitFocus(createNewFocusToRemoveOld: true);
    }
    _pageController.nextPage(duration: _animationDuration, curve: _animationCurve);
    _activeStep++;
    refreshThisPage();
  }

  void _previousPage() {
    _pageController.previousPage(duration: _animationDuration, curve: _animationCurve);
    _activeStep--;
    refreshThisPage();
  }

  Widget _nextButton({Function? onNext, String title = "Next"}) {
    return LongElevatedButton(
      padding: EdgeInsets.only(bottom: 12),
      title: title,
      onPressed: onNext ?? _nextPage,
    );
  }

  Widget _backButton() {
    return LongElevatedButton(
      isReverseColor: true,
      padding: EdgeInsets.only(bottom: 24.0),
      title: TextData.back,
      onPressed: _previousPage,
    );
  }

  Widget _buttonColumn({bool needBackButton = true, Function? onNext, String nextButtonText = "Next"}) {
    return Expanded(
        child: SafeArea(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Flexible(
                  child: _nextButton(
                    onNext: onNext,
                    title: nextButtonText,
                  ),
                ),
                SizedBox(
                  height: needBackButton ? 12 : 24,
                ),
                needBackButton ? Flexible(
                  child: _backButton(),
                ) : SizedBox(),
                SizedBox(
                  height: needBackButton ? 24 : 0,
                ),
              ],
            ),
          ),
        )
    );
  }

  void _onBackPage() {
    getBinaryDialog(TextData.back, "Are you sure you want to quit setting up? You will have to redo the whole process next time.", () {
      utils.closeKeyboard();
      getRoute.pop();
      getRoute.pop();
    }, positiveText: TextData.back, positiveColor: errorColor);
  }

  Future<void> _pickImage(ImageSource source) async {
    bool _allFull = false;
    if (source == ImageSource.camera) {
      /// If camera, use image picker
      PickedFile? _pickedFile = await imagePicker.getImage(
        source: source,
      );
      if (_pickedFile != null) {
        for (int i = 0; i < _imageFiles.length; i++) {
          if (_imageFiles[i] == null) {
            /// Replace null if have null
            _allFull = false;
            _imageFiles.removeAt(i);
            _imageFiles.insert(i, File(_pickedFile.path));
            break;
          } else {
            _allFull = true;
          }
        }
        if (_allFull) {
          /// Replace first if all have images
          _imageFiles.removeAt(0);
          _imageFiles.insert(0, File(_pickedFile.path));
        }
      }
    } else {
      List<int> _emptySlot = [];
      /// else use multiple image picker
      FilePickerResult? _results = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.image,
      );
      if (_results != null) {
        if (_results.files.length > AppConfig.allowedPicturePerProfile) {
          /// Make sure picked file number is acceptable
          _results.files.sublist(0, AppConfig.allowedPicturePerProfile);
        }
        for (int i = 0; i < _imageFiles.length; i++) {
          if (_imageFiles[i] == null) {
            _emptySlot.add(i);
          }
        }
        for (int i = 0; i < _results.files.length; i++) {
          if (_emptySlot.length >= i + 1) {
            _imageFiles.removeAt(_emptySlot[i]);
            _imageFiles.insert(_emptySlot[i], File(_results.files[i].path!));
          } else {
            _imageFiles.removeAt(_results.files.length - i - 1);
            _imageFiles.insert(_results.files.length - i - 1, File(_results.files[i].path!));
          }
        }
      }
    }
    refreshThisPage();
  }

  List<File> _validImageFiles() {
    List<File> _result = [];
    for (int i = 0; i < _imageFiles.length; i++) {
      if (_imageFiles[i] != null) {
        _result.add(_imageFiles[i]!);
      }
    }
    return _result;
  }

  void resetImageFiles() {
    _imageFiles = List.generate(AppConfig.allowedPicturePerProfile, (index) => null);
  }

  Widget _titleDisplay(String value) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 12.0, top: 30.0),
      child: Text(
        value,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 19,
        ),
      ),
    );
  }

  Widget _textDisplay(String value) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 12.0, bottom: 20),
      child: Text(
        value,
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }

  String _getTitle() {
    switch (_activeStep) {
      case 0:
        return "Email";
      case 1:
        return "Name";
      case 2:
        return "Location";
      case 3:
        return "Birthday";
      case 4:
        return "Gender";
      case 5:
        return "Tell the MiddleMan";
      case 6:
        return "Target";
    }
    return "Profile Pictures";
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async {
        _onBackPage();
        return false;
      },
      child: Scaffold(
        appBar: CustomAppBar(
          title: Text(
              _getTitle(),
          ),
          leading: IconButton(
            onPressed: () {
              _onBackPage();
            },
            icon: Icon(
              Icons.clear,
            ),
          ),
        ),
        bottomNavigationBar: Container(
          height: kToolbarHeight * 1.1,
          child: BottomAppBar(
            child: CustomIconStepper(
              pageController: _pageController,
              enableNextPreviousButtons: false,
              enableStepTapping: false,
              stepColor: unfocusedColor,
              activeStepColor: tomato,
              activeStepBorderColor: tomato,
              lineColor: tomato,
              icons: [
                Icon(
                  Icons.email_outlined,
                ),
                Icon(
                  Icons.supervised_user_circle_outlined,
                ),
                Icon(
                  Icons.location_on_outlined,
                ),
                Icon(
                  Icons.calendar_today_outlined,
                ),
                Icon(
                  Icons.transgender_outlined,
                ),
                Icon(
                  MdiIcons.text,
                ),
                Icon(
                  MdiIcons.heartOutline,
                ),
                Icon(
                  MdiIcons.imageOutline,
                ),
              ],
              // activeStep property set to activeStep variable defined above.
              activeStep: _activeStep,

              // This ensures step-tapping updates the activeStep.
              onStepReached: (index) {
                setState(() {
                  _activeStep = index;
                });
              },
            ),
          ),
        ),
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: [
            Form(
              key: _formEmailKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay("What is your email address?"),
                  _textDisplay("An email containing a verification link will be sent to this address. Please make sure you have access to this email."),
                  EmailTextField(
                    controller: _emailController,
                    focusNode: _emailFocusNode,
                    borderRadius: commonRadius,
                  ),
                  PasswordTextField(
                    controller: _passwordController,
                    focusNode: _passwordFocusNode,
                    borderRadius: commonRadius,
                  ),
                  _buttonColumn(
                    needBackButton: false,
                    onNext: () async {
                      if (_formEmailKey.currentState!.validate()) {
                        Dialogs.showLoadingDialog();
                        bool _isTaken = await apiManager.isEmailTaken(_emailController.text.trim());
                        getRoute.pop();
                        if (!_isTaken) {
                          _nextPage();
                        } else {
                          utils.toast("This email has already signed up", isWarning: true);
                        }
                      } else {
                        utils.toast("Please input a valid email address", isWarning: true);
                      }
                    },
                  ),
                ],
              ),
            ),
            Form(
              key: _formNameKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay("What's your name?"),
                  GenericTextField(
                    controller: _firstNameController,
                    focusNode: _firstNameFocusNode,
                    nextFocusNode: _lastNameFocusNode,
                    borderRadius: commonRadius,
                    hint: "First Name",
                    maxLines: 1,
                    textInputType: TextInputType.name,
                    textCapitalization: TextCapitalization.words,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                    ],
                    autofillHints: [AutofillHints.givenName],
                    validation: (String newValue) {
                      if (newValue.isNullOrEmpty) {
                        return "Name must not be empty";
                      }
                      if (newValue.contains(" ")) {
                        return "Name must not contain white space";
                      }
                      return null;
                    },
                  ),
                  GenericTextField(
                    controller: _lastNameController,
                    focusNode: _lastNameFocusNode,
                    nextFocusNode: _firstNameFocusNode,
                    borderRadius: commonRadius,
                    hint: "Last Name",
                    maxLines: 1,
                    textInputType: TextInputType.name,
                    textCapitalization: TextCapitalization.words,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                    ],
                    autofillHints: [AutofillHints.familyName],
                    validation: (String newValue) {
                      if (newValue.isNullOrEmpty) {
                        return "Name must not be empty";
                      }
                      if (newValue.contains(" ")) {
                        return "Name must not contain white space";
                      }
                      return null;
                    },
                  ),
                  _buttonColumn(
                    needBackButton: true,
                    onNext: () async {
                      if (_formNameKey.currentState!.validate()) {
                        refreshThisPage();
                        _nextPage();
                      }
                    },
                  ),
                ],
              ),
            ),
            Form(
              key: _formLocationKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay("Where are you, ${_firstNameController.text.trim()}?"),
                  _textDisplay("We need your approximate location for the sole purpose of helping you find potential matches near you. Your data will never be shared with anyone."),
                  GenericTypeAheadTextField(
                    dataList: countryList,
                    controller: _countryController,
                    focusNode: _countryFocusNode,
                    nextFocusNode: _cityFocusNode,
                    borderRadius: commonRadius,
                    hint: "Country",
                    maxLines: 1,
                    canEmpty: false,
                    textInputType: TextInputType.streetAddress,
                  ),
                  GenericTextField(
                    controller: _cityController,
                    focusNode: _cityFocusNode,
                    nextFocusNode: _countryFocusNode,
                    borderRadius: commonRadius,
                    hint: "City",
                    maxLines: 1,
                    textInputType: TextInputType.streetAddress,
                    textCapitalization: TextCapitalization.words,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                    ],
                    autofillHints: [AutofillHints.addressCity],
                    validation: (String newValue) {
                      if (newValue.isNullOrEmpty) {
                        return "City must not be empty";
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ArgonButton(
                    height: 50,
                    width: 140,
                    borderRadius: commonRadius,
                    color: appBgColor,
                    elevation: 6,
                    highlightElevation: 0,
                    roundLoadingShape: true,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Icon(
                            Icons.location_searching,
                            color: utils.getColorByHex(remoteConfig!.getString(RemoteConfigKey.startBackgroundNewMemberButtonColor)),
                          ),
                        ),
                        SizedBox(width: 10),
                        Flexible(
                          child: Text(
                            "Auto-fill",
                          ),
                        ),
                      ],
                    ),
                    loader: Container(
                      padding: EdgeInsets.all(10),
                      child: SpinKitRotatingCircle(
                        color: utils.getColorByHex(remoteConfig!.getString(RemoteConfigKey.startBackgroundNewMemberButtonColor)),
                      ),
                    ),
                    onTap: (startLoading, stopLoading, btnState) async {

                      Future<void> _getLocation() async {
                        bool isLocationEnabled  = await Geolocator.isLocationServiceEnabled();
                        if (!isLocationEnabled) {
                          if (Platform.isAndroid) {
                            await Geolocator.openLocationSettings();
                          } else {
                            await Geolocator.openAppSettings();
                          }
                        } else {
                          try {
                            Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);
                            _longitude = position.longitude;
                            _latitude = position.latitude;
                            List<Placemark> placemarks = await placemarkFromCoordinates(_latitude!, _longitude!);
                            if (placemarks.isNotEmpty) {
                              _countryController.text = placemarks.first.country!;
                              _cityController.text = placemarks.first.locality!.isEmpty ?
                              placemarks.first.administrativeArea!.isNullOrEmpty ? placemarks.first.thoroughfare! : placemarks.first.administrativeArea! : placemarks.first.locality!;
                              if (_cityController.text.isNullOrEmpty) {
                                utils.toast("We failed to find this city", isWarning: true);
                              }
                              if (_countryController.text.isNullOrEmpty) {
                                utils.toast("We failed to find this country", isWarning: true);
                              }
                            } else {
                              utils.toast("Failed to retrieve information", isWarning: true);
                            }
                          } on Exception catch (e) {
                            print("e: $e");
                            utils.toast("Permission denied", isWarning: true);
                          }
                        }
                      }

                      if (btnState == ButtonState.Idle) {
                        apiManager.logAnalysis(Analysis.getLocationBtn);
                        if (!hasShownLocationDialog) {
                          getBinaryDialog("Location Permission", "We need your permission to retrieve your geolocation. Feel free to deny if you are not comfortable with this.", () async {
                            startLoading();
                            await _getLocation();
                            stopLoading();
                          }, onDismiss: () {
                            hasShownLocationDialog = true;
                            utils.saveBoo(PrefKey.hasShownLocationDialog, hasShownLocationDialog);
                          });
                        } else {
                          startLoading();
                          await _getLocation();
                          stopLoading();
                        }
                      }
                    },
                  ),
                  _buttonColumn(
                    needBackButton: true,
                    onNext: () async {
                      if (_formLocationKey.currentState!.validate()) {
                        if (!countryList.contains(_countryController.text.trim())) {
                          utils.toast("Please provide a valid country name", isWarning: true);
                        } else {
                          if (_latitude == null || _longitude == null) {
                            try {
                              List<Location> _confirmLocation = await locationFromAddress(_cityController.text + ", " + _countryController.text);
                              if (_latitude == null) {
                                _latitude = _confirmLocation.first.latitude;
                              }
                              if (_longitude == null) {
                                _longitude = _confirmLocation.first.longitude;
                              }
                            } on Exception catch (e) {
                              utils.toast("Sorry, this address is not available", isWarning: true);
                            }
                          }
                          _countryFocusNode.unfocus();
                          _cityFocusNode.unfocus();
                          if (_latitude == null || _longitude == null) {
                            throw Exception("Location unavailable");
                          }
                          _nextPage();
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ScrollableFooter(
                  children: [
                    _titleDisplay("When were you born?"),
                    _textDisplay("Please note that you have to be older than ${AppConfig.minAge} in order to proceed."),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          _myBDay.year.toString() + "-",
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        Text(
                          _myBDay.month.toString() + "-",
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        Text(
                          _myBDay.day.toString(),
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        Container(width: 10,),
                        Theme(
                          data: ThemeData(
                            primaryColorBrightness: Brightness.dark,
                          ),
                          child: IconButton(
                            icon: Icon(
                              Icons.edit,
                              color: mainColor,
                            ),
                            onPressed: () {
                              showCustomDatePicker(
                                context: getRoute.getContext(),
                                initialDate: _myBDay,
                                firstDate: DateTime(1950, 1, 1),
                                lastDate: DateTime.now().toLocal(),
                                cancelText: TextData.cancel,
                                confirmText: TextData.confirm,
                                initialEntryMode: DatePickerEntryMode.calendar,
                                initialDatePickerMode: DatePickerMode.year,
                              ).then((DateTime? date) {
                                if (date != null) {
                                  _myBDay = date;
                                  customState(() {});
                                }
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    _buttonColumn(onNext: () {
                      if (utils.isAdult(_myBDay)) {
                        newProfile.zodiacSign = utils.getZodiacSign(_myBDay);
                        newProfile.age = utils.getAge(_myBDay);
                        newProfile.birthday = _myBDay;
                        apiManager.logAnalysis(Analysis.fillBDay);
                        _nextPage();
                      } else {
                        utils.toast("You are under age", isWarning: true);
                      }
                    }),
                  ],
                );
              },
            ),
            Form(
              key: _formGenderKey,
              child: StatefulBuilder(
                builder: (ctx, customState) {
                  return ScrollableFooter(
                    children: [
                      _titleDisplay("What is your physical gender and gender identity?"),
                      GenericStringDropDownField(
                        text: "Physical Gender",
                        onChanged: (String newValue) {
                          customState(() {
                            _physicalGender = newValue;
                            _genderIdentity = newValue;
                          });
                        },
                        canEmpty: false,
                        list: GenderExtension.valueList.toList(),
                        value: _physicalGender,
                      ),
                      GenericStringDropDownField(
                        text: "Gender Identity",
                        onChanged: (String newValue) {
                          customState(() {
                            _genderIdentity = newValue;
                          });
                        },
                        canEmpty: false,
                        list: GenderExtension.valueList.toList(),
                        value: _genderIdentity,
                      ),
                      _buttonColumn(
                        needBackButton: true,
                        onNext: () {
                          if (_genderIdentity.isNotEmpty && _physicalGender.isNotEmpty) {
                            newProfile.genderIdentity = GenderExtension.enums[_genderIdentity];
                            newProfile.physicalGender = GenderExtension.enums[_physicalGender];
                            _nextPage();
                          } else {
                            utils.toast("Please select a gender", isWarning: true);
                          }
                        },
                      ),
                    ],
                  );
                },
              ),
            ),
            Form(
              key: _formPersonalityKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay("Describe yourself and the people you are looking for in a few words"),
                  _textDisplay("Try to be more precise since this is what the middle man will rely on to make decisions."),
                  GenericTextField(
                    controller: _personalityController,
                    focusNode: _personalityFocusNode,
                    borderRadius: commonRadius,
                    hint: TextData.personality,
                    textInputType: TextInputType.multiline,
                    textCapitalization: TextCapitalization.words,
                    textInputAction: TextInputAction.newline,
                    maxLines: null,
                    maxLength: AppConfig.personalityMaxLength,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                    ],
                    validation: (String newValue) {
                      if (newValue.isNullOrEmpty) {
                        return "${TextData.personality} must not be empty";
                      }
                      return null;
                    },
                  ),
                  _buttonColumn(
                    needBackButton: true,
                    onNext: () {
                      if (_formPersonalityKey.currentState!.validate()) {
                        _nextPage();
                      }
                    },
                  ),
                ],
              ),
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ScrollableFooter(
                  children: [
                    _titleDisplay("One last question, what kind of a relationship are you seeking?"),
                    SizedBox(
                      height: 5,
                    ),
                    FadeIn(
                      child: GroupButton(
                        onSelected: (int index, bool isSelected) {
                          customState(() {
                            newProfile.relationshipStatus = RelationshipStatusExtension.enums[RelationshipStatusExtension.valueList.toList()[index]];
                          });
                        },
                        selectedButton: newProfile.relationshipStatus == null ? 0 : RelationshipStatusExtension.valueList.toList().indexOf(newProfile.relationshipStatus!.name!),
                        spacing: 10,
                        selectedColor: appIconColor,
                        selectedTextStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        buttons: RelationshipStatusExtension.valueList.toList(),
                        borderRadius: BorderRadius.circular(commonRadius),
                      ),
                    ),
                    _buttonColumn(
                      needBackButton: true,
                      onNext: () {
                        if (newProfile.relationshipStatus == null) {
                          newProfile.relationshipStatus = RelationshipStatusExtension.enums[RelationshipStatusExtension.valueList.toList()[0]];
                        }
                        _nextPage();
                      },
                    ),
                  ],
                );
              },
            ),
            StatefulBuilder(
              builder: (ctx, customState) {
                return ScrollableFooter(
                  children: [
                    ReorderableWrap(
                      runSpacing: 8.0,
                      padding: EdgeInsets.symmetric(vertical: 24.0),
                      children: List.generate(_imageFiles.length, (index) {
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: 6.0),
                          child: GestureDetector(
                            onTap: () {
                              getGenericBottomSheet(context, [
                                _imageFiles[index] != null ? Container() : BottomSheetButton(
                                  onTap: () {
                                    _pickImage(
                                      ImageSource.camera,
                                    );
                                  },
                                  iconData: Icons.add_a_photo_outlined,
                                  label: "From Camera",
                                ),
                                _imageFiles[index] != null ? Container() : BottomSheetButton(
                                  onTap: () {
                                    _pickImage(
                                      ImageSource.gallery,
                                    );
                                  },
                                  iconData: Icons.perm_media_outlined,
                                  label: "From Gallery",
                                ),
                                BottomSheetButton(
                                  onTap: () {
                                    if (_imageFiles[index] != null) {
                                      _imageFiles.removeAt(index);
                                      if (_imageFiles.length != AppConfig.allowedPicturePerProfile) {
                                        _imageFiles.add(null);
                                      }
                                      refreshThisPage();
                                    }
                                  },
                                  iconData: Icons.clear_outlined,
                                  label: "Remove Image",
                                ),
                              ]);
                            },
                            child: Badge(
                              position: BadgePosition(
                                top: 0,
                                end: 5,
                              ),
                              badgeColor: appIconColor,
                              badgeContent: Text(
                                (index + 1).toString(),
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(commonRadius),
                                  border: Border.all(
                                    color: appIconColor,
                                  ),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(commonRadius),
                                  child: _imageFiles[index] != null ? Image.file(
                                    _imageFiles[index]!,
                                    fit: BoxFit.cover,
                                    width: deviceWidth / 3.4,
                                    height: deviceWidth / 2,
                                  ) : Container(
                                    width: deviceWidth / 3.4,
                                    height: deviceWidth / 2,
                                    child: Icon(
                                      MdiIcons.image,
                                      color: mainColor,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                      onReorder: (int oldIndex, int newIndex) {
                        File? _tempFile = _imageFiles[oldIndex];
                        _imageFiles.removeAt(oldIndex);
                        _imageFiles.insert(newIndex, _tempFile);
                        customState(() {});
                      },
                    ),
                    _buttonColumn(
                      needBackButton: true,
                      nextButtonText: "Done",
                      onNext: () async {
                        if (_validImageFiles().length > 0) {
                          Dialogs.showLoadingDialog(title: "Creating account...");
                          newProfile.latitude = _latitude;
                          newProfile.longitude = _longitude;
                          newProfile.email = _emailController.text.trim();
                          newProfile.lastName = _lastNameController.text.trim();
                          newProfile.firstName = _firstNameController.text.trim();
                          newProfile.country = _countryController.text.trim();
                          newProfile.city = _cityController.text.trim();
                          newProfile.personality = _personalityController.text.trim();
                          await apiManager.createAccount(_passwordController.text.trim(), newProfile);
                          getRoute.pop();
                          Dialogs.showLoadingDialog(title: "Uploading images...");
                          List<String> _imageLinks = [];
                          for (int i = 0; i < _validImageFiles().length; i++) {
                            String? _link = await uploadManager.uploadFileToFirebase(_validImageFiles()[i]);
                            if (_link != null) {
                              _imageLinks.add(_link);
                            }
                          }
                          await apiManager.updateImage(_imageLinks);
                          getRoute.pop();
                          getRoute.navigateToAndPopAll(MainPage());
                        } else {
                          utils.toast("Please choose at least one picture of yourself", isWarning: true);
                        }
                      },
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
