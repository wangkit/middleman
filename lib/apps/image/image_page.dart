import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/widgets/loading/loading_media_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';

class ImagePage extends StatefulWidget {

  final String imageUrl;
  final String heroTag;

  ImagePage({
    Key? key,
    required this.imageUrl,
    required this.heroTag,
  }) : super(key: key);

  @override
  _ImagePageState createState() => _ImagePageState();
}

class _ImagePageState extends State<ImagePage> with AutomaticKeepAliveClientMixin<ImagePage> {

  late bool _showTopBottomBars;
  late double _opacity;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _showTopBottomBars = true;
    setOpacity();
    super.initState();
  }

  void showHideBars() {
    _showTopBottomBars = !_showTopBottomBars;
    setOpacity();
    refreshThisPage();
  }

  void setOpacity() {
    if (_showTopBottomBars) {
      _opacity = 1.0;
    } else {
      _opacity = 0.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      appBar: CustomAppBar(),
      body: SafeArea(
        child: Container(
          width: deviceWidth,
          height: deviceHeight,
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              PhotoViewGallery.builder(
                itemCount: 1,
                enableRotation: false,
                builder: (ctx, index) {
                  return PhotoViewGalleryPageOptions(
                    heroAttributes: PhotoViewHeroAttributes(tag: widget.heroTag),
                    imageProvider: CachedNetworkImageProvider(widget.imageUrl),
                  );
                },
                backgroundDecoration: BoxDecoration(
                  color: appBgColor,
                ),
                loadingBuilder: (context, loadingProgress) {
                  return LoadingMediaWidget();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
