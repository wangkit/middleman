import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/bubble/discover_bubble.dart';
import 'package:choicestory/widgets/bubble/preference_bubble.dart';
import 'package:choicestory/widgets/bubble/profile_bubble.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/image/profile_media.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:preload_page_view/preload_page_view.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class DiscoveryPage extends StatefulWidget {

  DiscoveryPage({
    Key? key,
  }) : super(key: key);

  @override
  _DiscoveryPageState createState() => _DiscoveryPageState();
}

class _DiscoveryPageState extends State<DiscoveryPage> with AutomaticKeepAliveClientMixin<DiscoveryPage>, TickerProviderStateMixin {

  late Query _profileQuery;
  late Future<QuerySnapshot> _getPotentials;
  List<UserProfile> _profileList = [];
  List<String> _profileId = [];

  @override
  void initState() {
    _profileQuery = firestore.collection(FirestoreCollections.profiles);
    List _filterList = [qp.id];
    if (qp.currentBatchIds != null && qp.currentBatchIds!.isNotEmpty) {
      _filterList.addAll(qp.currentBatchIds!);
    }
    if (likeOrPassOrGreetedIds.isNotEmpty) {
      /// Filter out already pass / match profiles and current batch profiles
      _filterList.addAll(likeOrPassOrGreetedIds);
    }
    _profileQuery = _profileQuery.where(UserCollection.id, whereNotIn: _filterList);
    _profileQuery = _profileQuery.limit(AppConfig.getDiscoverCount);
    _getPotentials = _profileQuery.get();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return FutureBuilder<QuerySnapshot>(
      future: _getPotentials,
      builder: (ctx, snapshot) {

        if (snapshot.connectionState != ConnectionState.done) {
          return LoadingWidget();
        }

        if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
          return CommonWidgets.emptyMessage("Come back later for more!");
        }

        void _handleProfile(List? profiles) {

          if (profiles == null || profiles.length == 0) {
            return;
          }

          for (QueryDocumentSnapshot _profile in profiles) {
            UserProfile _thisProfile = UserProfile.fromMapToProfile(_profile.data() as Map);

            if (!_profileId.contains(_thisProfile.id) && _thisProfile.id != qp.id!) {
              _profileList.add(_thisProfile);
              _profileId.add(_thisProfile.id!);
            }
          }
        }
        _handleProfile(snapshot.data!.docs);

        return ListView(
          children: List.generate(_profileList.length, (index) {

            UserProfile _thisProfile = _profileList[index];

            return DiscoverBubble(
              profile: _thisProfile,
            );
          }),
        );


      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
