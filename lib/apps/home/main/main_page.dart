import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:badges/badges.dart';
import 'package:choicestory/apps/home/chat/chat_page.dart';
import 'package:choicestory/apps/home/discovery/discovery_page.dart';
import 'package:choicestory/apps/home/flashery/flashery_page.dart';
import 'package:choicestory/apps/home/notification/notification_page.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/icon/chat_icon.dart';
import 'package:choicestory/widgets/icon/gradient_icon.dart';
import 'package:choicestory/widgets/icon/notification_icon.dart';
import 'package:choicestory/widgets/image/file_thumbnail.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class MainPage extends StatefulWidget {

  bool showSpotlight;

  MainPage({
    Key? key,
    this.showSpotlight = true,
  }) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with AutomaticKeepAliveClientMixin<MainPage>, SingleTickerProviderStateMixin {

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late ValueNotifier<int> _currentIndex;
  int _bottomBarItemCount = 5;
  final _homeIndex = 0;
  final _notificationIndex = 1;
  final _discoverIndex = 2;
  final _chatIndex = 3;
  final _settingIndex = 4;
  final double _bottomBarIconSize = 29;
  GlobalKey<NotificationIconState> _notificationKey = GlobalKey();
  GlobalKey<ChatIconState> _chatKey = GlobalKey();
  late PageController _pageController;

  @override
  void initState() {
    _currentIndex = ValueNotifier(0);
    _pageController = PageController();
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      if (utils.hasLoggedIn()) {
        /// Register firebase messaging
        NotificationSettings settings = await firebaseMessaging!.requestPermission(
          alert: true,
          announcement: false,
          badge: true,
          carPlay: false,
          criticalAlert: false,
          provisional: false,
          sound: true,
        );
      }
    });
    SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
      if (_currentIndex.value == _homeIndex) {
        FeatureDiscovery.discoverFeatures(
          context,
          const <String>{
            DiscoveryValue.tapToFlip,
          },
        );
      }
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _profileIcon() {
    return NetworkThumbnail(
      imageUrl: utils.hasLoggedIn() ? qp.images!.first : verticalGreyPlaceholder,
      hero: UniqueKey().toString(),
      width: _bottomBarIconSize,
      height: _bottomBarIconSize,
      disableOnTap: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      key: _scaffoldKey,
      bottomNavigationBar: StatefulBuilder(
        builder: (ctx, customState) {
          return BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            elevation: 4,
            currentIndex: _currentIndex.value,
            onTap: (index) {
              if (index == _chatIndex) {
                /// Clear unread chat count
                _chatKey.currentState!.clearUnread();
              }
              if (index != _currentIndex.value) {
                _currentIndex.value = index;
                if (index == _notificationIndex) {
                  /// Clear unseen notification count
                  _notificationKey.currentState!.clearUnseen();
                }
                customState(() {});
                _pageController.animateToPage(index, duration: kThemeAnimationDuration, curve: Curves.linearToEaseOut);
              }
            },
            showSelectedLabels: false,
            showUnselectedLabels: false,
            items: List.generate(_bottomBarItemCount, (index) {
              return BottomNavigationBarItem(
                tooltip: index == _notificationIndex ? TextData.notifications : index == _chatIndex ? TextData.messages : index == _homeIndex ? TextData.home : index == _discoverIndex ? TextData.discover : TextData.profile,
                label: "",
                icon: index == _notificationIndex ?
                NotificationIcon(
                  key: _notificationKey,
                  isSelected: _currentIndex.value == index,
                  iconSize: _bottomBarIconSize,
                ) :
                index == _chatIndex ? ChatIcon(
                  key: _chatKey,
                  isSelected: _currentIndex.value == index,
                  iconSize: _bottomBarIconSize,
                ) : index == _settingIndex ? _profileIcon() : Swing(
                  child: GradientIcon(
                    index == _homeIndex ? Icons.home_filled
                        : index == _discoverIndex ? Icons.search
                        : Icons.settings,
                    _bottomBarIconSize,
                    LinearGradient(
                      colors: _currentIndex.value == index ? [
                        turquoise,
                        lightTurquoise,
                      ] : [unfocusedColor, unfocusedColor],
                    ),
                  ),
                ),
              );
            }),
          );
        },
      ),
      appBar: CustomAppBar(
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        actions: [
          // IconButton(
          //   onPressed: () async {
          //     await utils.shareShortUrl("https://middleman.com/share");
          //   },
          //   icon: Icon(
          //     MdiIcons.accountPlusOutline,
          //   ),
          // ),
        ],
        title: BounceInLeft(
          child: ValueListenableBuilder(
            valueListenable: _currentIndex,
            builder: (ctx, value, child) {
              return Row(
                children: [
                  SizedBox(width: 5,),
                  AnimatedContainer(
                    width: 50,
                    height: 50,
                    duration: kThemeAnimationDuration,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        colorFilter: ColorFilter.mode(value == _homeIndex ? appIconColor : value == _chatIndex ? neonCarrot : value == _discoverIndex ? dustyRose : value == _notificationIndex ? electricViolet : java, BlendMode.srcATop),
                        image: AssetImage(
                          "assets/icon/middleman_without_bg.png",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Text(
                    value == _homeIndex ? myAppName : value == _chatIndex ? TextData.messages : value == _discoverIndex ? TextData.discover : value == _notificationIndex ? TextData.notifications :  TextData.profile,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              );
            },
          ),
        ),
      ),
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        pageSnapping: true,
        children: [
          FlasheryPage(),
          NotificationPage(),
          DiscoveryPage(),
          ChatPage(),
          SettingsPage(),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
