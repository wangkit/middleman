import 'dart:async';
import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:choicestory/apps/account/view_profile_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/block_dialog.dart';
import 'package:choicestory/widgets/chat/chat_input_bar.dart';
import 'package:choicestory/widgets/chat/message_bubble.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MessagePage extends StatefulWidget {
  final UserProfile targetProfile;
  final Function? setNoUnread;

  MessagePage({
    Key? key,
    required this.targetProfile,
    this.setNoUnread,
  }) : super(key: key);

  @override
  MessagePageState createState() => MessagePageState();
}

class MessagePageState extends State<MessagePage> with AutomaticKeepAliveClientMixin<MessagePage> {

  late TextEditingController _messageTextController;
  late String chatId;
  late double scrollOffset;
  late ScrollController _scrollController;
  late String scrollPositionKey;
  late ExpandableController _expandableController;
  late RefreshController _refreshController;
  DocumentSnapshot? _lastDocument;
  late Color _commonColor;
  late Query _query;
  List<String> _selectedMessageId = [];

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    if (widget.setNoUnread != null) {
      widget.setNoUnread!();
    }
    chatId = utils.getChatId([qp.id!, widget.targetProfile.id!]);
    _query = firestore
        .collection(FirestoreCollections.messages)
        .where(MessageCollection.chatId, isEqualTo: chatId)
        .orderBy(MessageCollection.createdAt, descending: true)
        .limit(AppConfig.numberOfMessagesPerPage);
    _refreshController = RefreshController(
      initialRefresh: false,
    );
    scrollPositionKey = chatId + "_scrollPosition";
    /// Set chat item to no unread first
    firestore.collection(FirestoreCollections.chats)
        .doc(qp.id! + widget.targetProfile.id!).update({
      ChatCollection.hasUnread: false,
    });
    _messageTextController = TextEditingController();
    _expandableController = ExpandableController();
    /// Scroll position got from shared preference or 0.0 if null
    scrollOffset = prefs!.getDouble(scrollPositionKey) ?? 0.0;

    /// Set init scroll offset according to the scrollPosition retrieved
    _scrollController = ScrollController(
      initialScrollOffset: scrollOffset,
    );

    /// Listen to the change of scroll offset and set it to scrollPosition
    _scrollController.addListener(() async {
      scrollOffset = _scrollController.offset;
      utils.saveDouble(scrollPositionKey, scrollOffset);
    });
    _commonColor = utils.getCorrectContrastColor(utils.getHexCodeByColor(utils.convertStrToColor(widget.targetProfile.id!)));
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _messageTextController.dispose();
    super.dispose();
  }

  void messagesStream() async {
    await for (var snapshot in firestore.collection(FirestoreCollections.messages).snapshots()) {
      for (var message in snapshot.docs) {
        print(message.data);
      }
    }
  }

  void scrollToBottom({int waitMilliseconds = 100}) {
    if (_scrollController.hasClients) {
      _scrollController.jumpTo(
        0.0,
      );
      /// A delay is needed for the scroll controller to update its max extent and current position
      Future.delayed(Duration(milliseconds: waitMilliseconds), () {
        if (scrollOffset != 0.0) {
          scrollToBottom();
        }
      });
    }
  }

  void scrollToBottomOnKeyboardUp() {
    /// If the user is at the bottom, scroll the chat list view to the bottom when the user opens the keyboard by clicking the input bar
    /// If the user is NOT at the bottom at the moment when the user clicks the input bar, do nothing
    if (_scrollController.position.atEdge && _scrollController.position.pixels != 0) {
      WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
        scrollToBottom();
      });
    }
  }

  void refreshThisPage() {
    if (this.mounted){
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      backgroundColor: appBgColor,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => getRoute.pop(),
          icon: Icon(
            Icons.arrow_back,
            color: _commonColor,
          ),
        ),
        actions: <Widget>[
          _selectedMessageId.length > 0 ? IconButton(
            onPressed: () {
              _selectedMessageId.forEach((messageId) {
                apiManager.clearSingleMessage(messageId, widget.targetProfile.id!);
              });
              _selectedMessageId.clear();
              refreshThisPage();
            },
            icon: Icon(
                Icons.delete,
                color: mainColor
            ),
          ) : Container(),
          PopupMenuButton<int>(
            child: IconButton(
                onPressed: null,
                icon: Icon(
                  Icons.more_vert,
                  color: _commonColor,
                )
            ),
            onSelected: (int value) async {
              switch (value) {
                case 0:
                  getBinaryDialog(TextData.deleteAllMessages, TextData.deleteAllMessagesAlert, () {
                    getRoute.pop();
                    apiManager.clearMessageInChat(chatId, widget.targetProfile.id!);
                  });
                  break;
                case 1:
                  await getBlockDialog(widget.targetProfile);
                  getRoute.pop();
                  break;
              }
            },
            itemBuilder: (ctx) {
              return <PopupMenuEntry<int>>[
                PopupMenuItem(
                  value: 0,
                  child: Text(
                    TextData.clearChat,
                  ),
                ),
                PopupMenuItem(
                  value: 1,
                  child: Text(
                    TextData.blockUser,
                  ),
                ),
              ];
            },
          ),
        ],
        title: InkWell(
          splashColor: utils.convertStrToColor(qp.id!),
          onTap: () {
            getRoute.navigateTo(
              ViewProfilePage(
                targetId: widget.targetProfile.id!,
              )
            );
          },
          child: Container(
            height: kToolbarHeight * 0.9,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                NetworkThumbnail(
                  width: 30,
                  height: 30,
                  imageUrl: utils.getProfileThumbnail(widget.targetProfile.images!),
                ),
                SizedBox(width: 20,),
                Text(
                  utils.getFullName(widget.targetProfile),
                ),
              ],
            ),
          ),
        ),
        backgroundColor: utils.convertStrToColor(widget.targetProfile.id!),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: _query.snapshots(),
              builder: (context, snapshot) {

                List<MessageBubble> messageBubbles = [];

                void _handleMessages(List<QueryDocumentSnapshot> messages) {
                  
                  if (messages.isEmpty) {
                    return;
                  }

                  _lastDocument = messages.last;

                  for (QueryDocumentSnapshot message in messages) {

                    Map _data = message.data() as Map;
                    bool isMyself = qp.id! == _data[MessageCollection.senderId];

                    final messageBubble = MessageBubble(
                      messageId: _data[MessageCollection.messageId],
                      message: _data[MessageCollection.message],
                      senderId: _data[MessageCollection.senderId],
                      senderName: utils.getFullName(isMyself ? qp : widget.targetProfile),
                      createdAt: timeUtils.getDateTimeFromTimeStamp(_data[MessageCollection.createdAt])!.toLocal(),
                      receiverId: _data[MessageCollection.receiverId],
                      isSelected: _selectedMessageId.contains(_data[MessageCollection.messageId]),
                      receiverName: utils.getFullName(isMyself ? widget.targetProfile : qp),
                      selectedMessages: _selectedMessageId,
                      notifyParent: refreshThisPage,
                      isMyself: isMyself,
                    );
                    if (!messageBubbles.contains(messageBubble)) {
                      messageBubbles.add(messageBubble);
                    }
                  }
                }

                if (!snapshot.hasData) {
                  return Container();
                }
                
                _handleMessages(snapshot.data!.docs);
                
                return Expanded(
                  child: SmartRefresher(
                    controller: _refreshController,
                    reverse: true,
                    enablePullUp: true,
                    enablePullDown: false,
                    onLoading: () {
                      if (_lastDocument == null) {
                        _query.get().then((result) {
                          final messages = result.docs;
                          _handleMessages(messages);
                          _refreshController.loadComplete();
                        });
                      } else {
                        _query
                            .startAfterDocument(_lastDocument!)
                            .get()
                            .then((result) {
                          final messages = result.docs;
                          _handleMessages(messages);
                          _refreshController.loadComplete();
                        });
                      }
                    },
                    child: ListView(
                      reverse: true,
                      controller: _scrollController,
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      children: messageBubbles,
                    ),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SafeArea(
                child: ChatInputBar(
                  targetProfile: widget.targetProfile,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
