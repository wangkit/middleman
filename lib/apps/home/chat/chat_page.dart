import 'dart:async';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/bubble/chat_bubble.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:soy_common/color/color.dart';

class ChatPage extends StatefulWidget {
  ChatPage({Key? key}) : super(key: key);

  @override
  ChatPageState createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> with AutomaticKeepAliveClientMixin<ChatPage> {

  late Query _query;
  late Stream<QuerySnapshot> _stream;

  @override
  bool get wantKeepAlive => true;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _query = firestore.collection(FirestoreCollections.chats)
        .where(ChatCollection.userId, isEqualTo: qp.id)
        .orderBy(ChatCollection.updatedAt, descending: true);
    _stream = _query.snapshots();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<QuerySnapshot>(
      stream: _stream,
      builder: (ctx, snapshot) {

        if (snapshot.connectionState != ConnectionState.active) {
          return LoadingWidget();
        }

        if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
          return CommonWidgets.emptyMessage("No chat room");
        }

        final chats = snapshot.data!.docs;
        List<ChatItem> _chatItems = [];

        for (QueryDocumentSnapshot chat in chats) {

          Map data = chat.data() as Map;
          ChatItem _chatItem = ChatItem.fromMapToChatItem(data);
          _chatItems.add(_chatItem);
        }

        return ListView.separated(
          separatorBuilder: (ctx, index) {
            return Divider(
              color: unfocusedColor,
              height: 1,
            );
          },
          itemCount: _chatItems.length,
          itemBuilder: (context, index) {
            return ChatBubble(
              key: Key(_chatItems[index].hasUnread.toString() + _chatItems[index].lastMessage + _chatItems[index].updatedAt.toString()),
              chatItem: _chatItems[index],
            );
          },
        );
      },
    );
  }
}

