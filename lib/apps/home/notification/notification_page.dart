import 'dart:async';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/enum/activity.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/bubble/chat_bubble.dart';
import 'package:choicestory/widgets/bubble/notification_bubble.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:soy_common/color/color.dart';

class NotificationPage extends StatefulWidget {
  NotificationPage({Key? key}) : super(key: key);

  @override
  NotificationPageState createState() => NotificationPageState();
}

class NotificationPageState extends State<NotificationPage> with AutomaticKeepAliveClientMixin<NotificationPage> {

  late Query _query;
  late Stream<QuerySnapshot> _stream;
  late RefreshController _refreshController;
  DocumentSnapshot? _lastDocument;
  List<String> _notificationIds = [];

  @override
  bool get wantKeepAlive => true;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _query = firestore.collection(FirestoreCollections.notifications)
        .where(NotificationCollection.targetId, isEqualTo: qp.id)
        .limit(40)
        .orderBy(ChatCollection.createdAt, descending: true);
    _refreshController = RefreshController(
      initialRefresh: false,
    );
    _stream = _query.snapshots();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<QuerySnapshot>(
      stream: _stream,
      builder: (ctx, snapshot) {

        if (snapshot.connectionState != ConnectionState.active) {
          return LoadingWidget();
        }

        if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
          return CommonWidgets.emptyMessage("No notifications");
        }

        List<Widget> _notificationBubbles = [];
        List<NotificationMessage> _notificationItems = [];

        void _addDivider() {
          _notificationBubbles.add(Divider(
            color: unfocusedColor,
            height: 1,
            indent: 20,
            endIndent: 20,
          ));
        }

        void _handleNotifications(List notifications) {

          if (notifications.isEmpty) {
            return;
          }

          for (QueryDocumentSnapshot notification in notifications) {
            Map data = notification.data() as Map;
            _lastDocument = notifications.last;
            NotificationMessage _notification = NotificationMessage.fromMapToNotification(data);
            if (_notificationBubbles.isEmpty || (_notificationBubbles.isNotEmpty && (_notification.createdAt.day != _notificationItems.last.createdAt.day || _notification.createdAt.month != _notificationItems.last.createdAt.month || _notification.createdAt.year != _notificationItems.last.createdAt.year))) {
              /// If this chat item is on different day from the last one in the list, then add a time row
              NotificationMessage _dayMessage = NotificationMessage(id: "", senderId: "", targetId: "", senderLastName: "", senderFirstName: "", senderImages: [], action: 0, isRead: false, createdAt: _notification.createdAt, isTime: true);
              _notificationBubbles.add(NotificationBubble(
                key: Key(_notification.isRead.toString() + _notification.id),
                notification: _dayMessage,
              ));
              _addDivider();
            }
            if (!_notificationIds.contains(_notification.id)) {
              _notificationIds.add(_notification.id);
              _notificationItems.add(_notification);
              _notificationBubbles.add(NotificationBubble(
                key: Key(_notification.isRead.toString() + _notification.id),
                notification: _notification,
              ));
              _addDivider();
            }
          }
        }

        _handleNotifications(snapshot.data!.docs);

        return SmartRefresher(
          controller: _refreshController,
          enablePullUp: true,
          enablePullDown: false,
          onLoading: () async {
            late dynamic result;
            if (_lastDocument == null) {
              result = await _query.get();
            } else {
              result = await _query
                  .startAfterDocument(_lastDocument!)
                  .get();
            }
            _handleNotifications(result.docs);
            _refreshController.loadComplete();
            print("here");
          },
          child: ListView(
            children: _notificationBubbles,
          ),
        );
      },
    );
  }
}

