import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/home/settings/settings_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/resources/enum/gender.dart';
import 'package:choicestory/resources/enum/relationship_status.dart';
import 'package:choicestory/resources/enum/zodiac_sign.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/bubble/preference_bubble.dart';
import 'package:choicestory/widgets/bubble/profile_bubble.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/image/profile_media.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:lottie/lottie.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/discovery_value.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/widgets/animations/faded_indexed_stack.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/button/bottom_sheet_button.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:preload_page_view/preload_page_view.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';

class FlasheryPage extends StatefulWidget {

  FlasheryPage({
    Key? key,
  }) : super(key: key);

  @override
  _FlasheryPageState createState() => _FlasheryPageState();
}

class _FlasheryPageState extends State<FlasheryPage> with AutomaticKeepAliveClientMixin<FlasheryPage>, TickerProviderStateMixin {

  late Query _profileQuery;
  late Future<QuerySnapshot> _getPotentials;
  List<UserProfile> _profileList = [];
  late PreloadPageController _preloadPageController;
  late bool isFinish;
  late final AnimationController _lController;
  ValueNotifier<double> _opacity = ValueNotifier(0);
  bool _getNewBatch = false;
  bool _usedUpProfile = false;
  List<String> _profileId = [];

  @override
  void initState() {
    isFinish = false;
    _preloadPageController = PreloadPageController();
    _profileQuery = firestore.collection(FirestoreCollections.profiles);
    /// The ordering of this where clauses is important and can't be changed
    if (qp.lastActiveAt != null && DateTime.now().isBefore(qp.lastActiveAt!.add(Duration(hours: AppConfig.batchInterval)))) {
      if (qp.currentBatchIds != null && qp.currentBatchIds!.isNotEmpty) {
        _profileQuery = _profileQuery.where(UserCollection.id, whereIn: qp.currentBatchIds!);
      } else {
        _usedUpProfile = true;
      }
    } else {
      _getNewBatch = true;
      if (likeOrPassOrGreetedIds.isNotEmpty) {
        _profileQuery = _profileQuery.where(UserCollection.id, whereNotIn: likeOrPassOrGreetedIds);
      }
      if (myPreference.education != null && myPreference.education!.isNotEmpty) {
        _profileQuery = _profileQuery.where(UserCollection.education, isEqualTo: myPreference.education!);
      }
      if (myPreference.profession != null && myPreference.profession!.isNotEmpty) {
        _profileQuery = _profileQuery.where(UserCollection.profession, isEqualTo: myPreference.profession!);
      }
      if (myPreference.religion != null && myPreference.religion!.isNotEmpty) {
        _profileQuery = _profileQuery.where(UserCollection.religion, isEqualTo: myPreference.religion!);
      }
      if (myPreference.zodiacSign != null && myPreference.zodiacSign != ZodiacSign.noPreference) {
        _profileQuery = _profileQuery.where(UserCollection.zodiacSign, isEqualTo: ZodiacSignExtension.names[myPreference.zodiacSign]);
      }
      if (myPreference.physicalGender != null) {
        _profileQuery = _profileQuery.where(UserCollection.physicalGender, isEqualTo: GenderExtension.names[myPreference.physicalGender]);
      }
      if (myPreference.genderIdentity != null) {
        _profileQuery = _profileQuery.where(UserCollection.genderIdentity, isEqualTo: GenderExtension.names[myPreference.genderIdentity]);
      }
      if (myPreference.relationshipStatus != null) {
        _profileQuery = _profileQuery.where(UserCollection.relationshipStatus, isEqualTo: RelationshipStatusExtension.names[myPreference.relationshipStatus]);
      }
    }
    if (!_usedUpProfile) {
      _profileQuery = _profileQuery.limit(AppConfig.getPotentialCount);
      _getPotentials = _profileQuery.get();
    } else {
      _getPotentials = Future.delayed(Duration(microseconds: 100));
    }
    _lController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );
    super.initState();
  }

  @override
  void dispose() {
    _lController.dispose();
    _preloadPageController.dispose();
    _opacity.dispose();
    super.dispose();
  }


  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void nextPage(bool isMatch) {
    if (isMatch && showLikeAnimation) {
      _opacity.value = 1;
      _lController.forward();
      Future.delayed(Duration(seconds: 2), () {
        _opacity.value = 0;
        refreshThisPage();
        _lController.reverse();
      });
    }
    _preloadPageController.nextPage(duration: kThemeAnimationDuration, curve: Curves.bounceIn);
    if (_preloadPageController.page! >= _profileList.length - 1) {
      isFinish = true;
      _profileList.clear();
      _profileId.clear();
      refreshThisPage();
    }
  }

  Widget _empty() {
    return Center(
      child: Text(
        "Try altering your preference for a better result",
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return FutureBuilder<QuerySnapshot>(
      future: _getPotentials,
      builder: (ctx, snapshot) {

        if (snapshot.connectionState != ConnectionState.done) {
          return LoadingWidget();
        }

        if (!snapshot.hasData || snapshot.data!.docs.length == 0) {
          return _empty();
        }

        void _handleProfile(List? profiles) {

          if (profiles == null || profiles.length == 0) {
            return;
          }

          for (QueryDocumentSnapshot _profile in profiles) {
            UserProfile _thisProfile = UserProfile.fromMapToProfile(_profile.data() as Map);

            if (!_profileId.contains(_thisProfile.id) && _thisProfile.id != qp.id!) {
              if (myPreference.minAge != null && myPreference.maxAge != null) {
                if (_thisProfile.age! > myPreference.maxAge! || _thisProfile.age! < myPreference.minAge!) {
                  break;
                }
              }
              _profileList.add(_thisProfile);
              _profileId.add(_thisProfile.id!);
            }
          }
          if (_getNewBatch) {
            apiManager.updateCurrentBatch(_profileId);
          }
        }

        _handleProfile(snapshot.data!.docs);

        return PreloadPageView(
          controller: _preloadPageController,
          preloadPagesCount: 3,
          scrollDirection: Axis.vertical,
          pageSnapping: true,
          physics: NeverScrollableScrollPhysics(),
          children: List.generate(_profileList.length, (index) {

            UserProfile _thisProfile = _profileList[index];

            return isFinish ? _empty() : Stack(
              children: [
                ProfileBubble(
                  profile: _thisProfile,
                  nextPage: nextPage,
                ),
                Align(
                  alignment: Alignment.center,
                  child: ValueListenableBuilder<double>(
                    valueListenable: _opacity,
                    builder: (ctx, value, child) {
                      return value == 0 ? CommonWidgets.emptyBox() : AnimatedOpacity(
                        opacity: value,
                        duration: Duration(milliseconds: 200),
                        child: Lottie.asset(
                          'assets/lottie/match.json',
                          controller: _lController,
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          }),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
