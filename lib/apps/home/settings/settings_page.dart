import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/account/edit_image_page.dart';
import 'package:choicestory/apps/account/edit_preference_page.dart';
import 'package:choicestory/apps/account/edit_profile_page.dart';
import 'package:choicestory/apps/account/view_profile_page.dart';
import 'package:choicestory/apps/settings/user_experience_settings.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/models/models.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/bottom_sheet/custom_bottom_sheet.dart';
import 'package:choicestory/widgets/common/common_widgets.dart';
import 'package:choicestory/widgets/image/file_thumbnail.dart';
import 'package:choicestory/widgets/image/network_thumbnail.dart';
import 'package:choicestory/widgets/loading/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:soy_common/bottom_sheet/generic_bottom_sheet.dart';
import 'package:soy_common/color/color.dart';
import 'package:soy_common/pages/soy_about_page.dart';

class SettingsPage extends StatefulWidget {

  SettingsPage({Key? key,}) : super(key: key);

  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {

  double? _iconSize;

  late Map<String, Widget> _detailMap;

  Map<String, String> _subtitleMap = {
    TextData.myProfile: "View / Edit Your Profile",
    TextData.myPreference: "View / Edit Your Preference",
    TextData.aboutUs: "About Spinner of Yarns",
    TextData.userSettings: "Fine Tune Your Experience",
  };

  Map<String, Icon>? _iconMap;

  @override
  void initState() {
    _iconSize = 52;
    _iconMap = {
      TextData.myProfile: Icon(
        Icons.account_circle,
        size: _iconSize,
        color: utils.hasLoggedIn() ? utils.convertStrToColor(qp.id!) : Colors.green,
      ),
      TextData.myPreference: Icon(
        Icons.room_preferences,
        size: _iconSize,
        color: Colors.yellow,
      ),
      TextData.aboutUs: Icon(
        MdiIcons.information,
        size: _iconSize,
        color: Colors.red
      ),
      TextData.userSettings: Icon(
        Icons.settings,
        size: _iconSize,
        color: Colors.blue
      ),
    };
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      _detailMap = {
        TextData.myProfile: EditProfilePage(),
        TextData.myPreference: EditPreferencePage(),
        TextData.aboutUs: SoyAboutPage(
          mainColor: mainColor,
          shareMsg: "I have been using $myAppName! Let's play together!",
          elevation: Theme.of(getRoute.getContext()).appBarTheme.elevation!,
          title: TextData.about,
          titleBarColor: Colors.white70,
          backgroundColor: appBgColor,
          smallTextStyle: TextStyle(
            fontSize: 12,
          ),
          appName: myAppName,
          version: versionName! + "(" + versionCode.toString() + ")",
        ),
        TextData.userSettings: UserExperienceSettings(),
      };
    });
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _line() {
    return Divider(
      endIndent: 20,
      indent: 20,
      height: 10,
      thickness: 0.7,
      color: utils.convertStrToColor(qp.id!),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            utils.hasLoggedIn() ? GestureDetector(
              onTap: () {
                if (utils.hasLoggedIn()) {
                  getRoute.navigateTo(EditImagePage());
                }
              },
              child: NetworkThumbnail(
                radius: 100,
                width: 120,
                height: 120,
                disableOnTap: true,
                showBorder: false,
                imageUrl: utils.getProfileThumbnail(qp.images!),
              ),
            ) : CustomFileImage(
              width: 120,
              height: 120,
              radius: 100,
              disableOnTap: true,
              file: File("assets/placeholder.png"),
            ),
            SizedBox(
              height: 20,
            ),
            _line(),
            Container(
              height: 70,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: List.generate(3, (index) {

                  RefreshController _refreshController = RefreshController();
                  DocumentSnapshot? _lastDocument;
                  late Query _query;
                  if (index == 0) {
                    _query = firestore.collection(FirestoreCollections.matches).where(MatchCollection.targetId, isEqualTo: qp.id).limit(10);
                  } else if (index == 1) {
                    _query = firestore.collection(FirestoreCollections.pairs).where(PairCollection.pairs, arrayContains: qp.id).limit(10);
                  } else {
                    _query = firestore.collection(FirestoreCollections.greets).where(GreetCollection.targetId, isEqualTo: qp.id).limit(10);
                  }

                  return InkWell(
                    onTap: () {
                      getCustomBottomSheet(context, [
                        Expanded(
                          child: FutureBuilder<QuerySnapshot>(
                            future: _query.get(),
                            builder: (ctx, snapshot) {

                              if (snapshot.connectionState != ConnectionState.done) {
                                return LoadingWidget();
                              }

                              if (!snapshot.hasData || snapshot.data == null || snapshot.data!.size == 0) {
                                return CommonWidgets.emptyMessage("No users found");
                              }

                              List<String> _targetIds = [];

                              void _handleData(List<QueryDocumentSnapshot> data) {

                                if (data.isEmpty) {
                                  return;
                                }

                                _lastDocument = data.last;

                                for (QueryDocumentSnapshot oneData in data) {
                                  String _id = "";
                                  Map _map = oneData.data() as Map;
                                  if (index == 0) {
                                    _id = _map[MatchCollection.id];
                                  } else if (index == 1) {
                                    List<String> _ids = _map[PairCollection.pairs].cast<String>();
                                    for (String id in _ids) {
                                      if (id != qp.id) {
                                        _id = id;
                                      }
                                    }
                                  } else {
                                    _id = _map[GreetCollection.id];
                                  }
                                  if (!_targetIds.contains(_id)) {
                                    _targetIds.add(_id);
                                  }
                                }
                              }

                              _handleData(snapshot.data!.docs);

                              return StatefulBuilder(
                                builder: (ctx, customState) {
                                  return SmartRefresher(
                                    controller: _refreshController,
                                    enablePullUp: true,
                                    enablePullDown: false,
                                    onLoading: () async {
                                      if (_lastDocument == null) {
                                        var result = await _query.get();
                                        final data = result.docs;
                                        _handleData(data);
                                        _refreshController.loadComplete();
                                      } else {
                                        var result = await _query.startAfterDocument(_lastDocument!).get();
                                        final data = result.docs;
                                        _handleData(data);
                                        _refreshController.loadComplete();
                                      };
                                      customState(() {});
                                    },
                                    child: ListView.builder(
                                      itemCount: _targetIds.length,
                                      itemBuilder: (ctx, index) {
                                        return FutureBuilder<DocumentSnapshot>(
                                          key: Key(_targetIds[index]),
                                          future: firestore.collection(FirestoreCollections.profiles).doc(_targetIds[index]).get(),
                                          builder: (ctx, snapshot) {

                                            if (snapshot.data == null || !snapshot.hasData) {
                                              return CommonWidgets.emptyBox();
                                            }

                                            var data = snapshot.data!.data() as Map;
                                            UserProfile profile = UserProfile.fromMapToProfile(data);

                                            return Padding(
                                              padding: const EdgeInsets.only(top: 8.0),
                                              child: ListTile(
                                                onTap: () {
                                                  getRoute.pop();
                                                  getRoute.navigateTo(ViewProfilePage(
                                                    targetId: profile.id!,
                                                    profile: profile,
                                                  ),);
                                                },
                                                title: Text(
                                                  utils.getFullName(profile),
                                                ),
                                                leading: NetworkThumbnail(
                                                  imageUrl: profile.images!.first,
                                                  width: 50,
                                                  height: 50,
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  );
                                },
                              );
                            },
                          ),
                        ),
                      ]);
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            utils.shortStringForLongInt(index == 0 ? qp.likeCount : index == 1 ? qp.matchCount : qp.greetCount),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            index == 0 ? TextData.likes : index == 1 ? TextData.matches : TextData.greets,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
            ),
            _line(),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Text(
                qp.personality!,
                key: UniqueKey(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20
                ),
              ),
            ),
            Container(
              height: 30,
            ),
            Flexible(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: GridView.builder(
                  itemCount: 4,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    childAspectRatio: 3,
                  ),
                  itemBuilder: (ctx, index) {
                    return Card(
                      elevation: 4.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(commonRadius / 2),
                      ),
                      child: InkWell(
                        onTap: () {
                          getRoute.navigateTo(_detailMap.values.toList()[index]);
                        },
                        borderRadius: BorderRadius.circular(commonRadius / 2),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 24.0),
                              child: _iconMap![_iconMap!.keys.toList()[index]],
                            ),
                            Expanded(
                              child: Center(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(horizontal: 12.0),
                                      child: Text(
                                        _subtitleMap.keys.toList()[index],
                                        style: TextStyle(
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 12, right: 0, top: 12, bottom: 0),
                                        child: Text(
                                          _subtitleMap.values.toList()[index],
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
