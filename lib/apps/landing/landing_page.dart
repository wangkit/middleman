import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/login/login_page.dart';
import 'package:choicestory/apps/setup/setup_page.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/values/analysis.dart';
import 'package:choicestory/resources/values/remote_config_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/widgets/button/long_elevated_button.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:choicestory/utils/extensions/string_extensions.dart';
import 'package:soy_common/color/color.dart';

class LandingPage extends StatefulWidget {

  LandingPage({
    Key? key,
  }) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> with TickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [utils.getColorByHex(remoteConfig!.getString(RemoteConfigKey.startBackgroundTopColor)),  utils.getColorByHex(remoteConfig!.getString(RemoteConfigKey.startBackgroundBottomColor))],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            height: deviceHeight,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 14.0, bottom: 24),
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(100.0),
                              child: Hero(
                                tag: UniqueKey(),
                                child: Image.asset(
                                  "assets/icon/middleman_without_bg.png",
                                  fit: BoxFit.cover,
                                  color: Colors.white,
                                  width: 70,
                                  height: 70,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        child: BounceInDown(
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 30.0,),
                            child: Text(
                              remoteConfig!.getString(RemoteConfigKey.landingPageQuote),
                              style: TextStyle(
                                fontSize: 56,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 8.0, top: 4.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          FadeInLeft(
                            child: LongElevatedButton(
                              height: 55,
                              splashColor: Colors.blue,
                              padding: EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0,),
                              title: TextData.newMember,
                              color: utils.getColorByHex(remoteConfig!.getString(RemoteConfigKey.startBackgroundNewMemberButtonColor)),
                              textStyle: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1.1,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                apiManager.logAnalysis(Analysis.newMemberButtonClick);
                                getRoute.navigateTo(
                                    SetupPage()
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 17,
                          ),
                          FadeInRight(
                            child: LongElevatedButton(
                              height: 55,
                              padding: EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0,),
                              isReverseColor: true,
                              splashColor: Colors.red,
                              color: transparent,
                              title: TextData.existingMember,
                              textStyle: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1.1,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                apiManager.logAnalysis(Analysis.existingMemberButtonClick);
                                getRoute.navigateTo(
                                  LoginPage()
                                );
                              },
                            ),
                          ),
                          Expanded(
                            child: SafeArea(
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 12.0),
                                  child: RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: TextData.byContinuing,
                                          style: TextStyle(
                                          ),
                                        ),
                                        TextSpan(
                                          recognizer: TapGestureRecognizer()..onTap = () {
                                            utils.launchURL(privacyPolicyUrl);
                                          },
                                          text: TextData.privacyPolicy,
                                          style: TextStyle(
                                              decoration: TextDecoration.underline,
                                          ),
                                        ),
                                        TextSpan(
                                          text: " " + TextData.and + " ",
                                        ),
                                        TextSpan(
                                          recognizer: TapGestureRecognizer()..onTap = () {
                                            utils.launchURL(eulaLink);
                                          },
                                          text: TextData.endUserLicenseAgreement,
                                          style: TextStyle(
                                            decoration: TextDecoration.underline,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
