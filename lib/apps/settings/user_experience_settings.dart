import 'dart:async';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:choicestory/apps/landing/landing_page.dart';
import 'package:choicestory/configs/app_config.dart';
import 'package:choicestory/constants/const.dart';
import 'package:choicestory/resources/color/color.dart';
import 'package:choicestory/resources/models/dbmodels.dart';
import 'package:choicestory/resources/values/analysis.dart';
import 'package:choicestory/resources/values/pref_key.dart';
import 'package:choicestory/resources/values/text.dart';
import 'package:choicestory/utils/dialog/binary_dialog.dart';
import 'package:choicestory/utils/dialog/loading_dialog.dart';
import 'package:choicestory/widgets/appbar/custom_appbar.dart';
import 'package:choicestory/widgets/details/review_card.dart';
import 'package:choicestory/widgets/details/review_title.dart';
import 'package:flutter/material.dart';
import 'package:soy_common/color/color.dart';

class UserExperienceSettings extends StatefulWidget {

  UserExperienceSettings({Key? key,}) : super(key: key);

  @override
  _UserExperienceSettingsState createState() => _UserExperienceSettingsState();
}

class _UserExperienceSettingsState extends State<UserExperienceSettings> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _item(String value, {Widget? child, Function? onPressed, TextAlign? textAlign}) {
    return InkWell(
      onTap: () {
        if (onPressed != null) {
          onPressed();
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: textAlign != null ? 0 : 12.0, top: 20.0, bottom: 20.0),
              child: Text(
                value,
                textAlign: textAlign,
                maxLines: 4,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ),
          child != null ? child : Container(),
          onPressed == null ? Container() : Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Icon(
                Icons.navigate_next,
                color: mainColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: Text(
          TextData.settings,
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.redo,
            ),
            onPressed: () {
              getBinaryDialog("Reset", "All settings will be reset. Are you sure?", () {
                getRoute.pop();
                showGreetAnimation = true;
                showLikeAnimation = true;
                showMessageCount = true;
                showNotificationCount = true;
                utils.saveBoo(PrefKey.showLikeAnimation, showLikeAnimation);
                utils.saveBoo(PrefKey.showGreetAnimation, showGreetAnimation);
                utils.saveBoo(PrefKey.showMessageCount, showMessageCount);
                utils.saveBoo(PrefKey.showNotificationCount, showNotificationCount);
                qp.isReceive = true;
                apiManager.setConfig(UserCollection.isReceive, qp.isReceive!);
                refreshThisPage();
              });
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 10),
            ReviewTitle(title: "Animation"),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) async {
                    showLikeAnimation = newValue;
                    utils.saveBoo(PrefKey.showLikeAnimation, showLikeAnimation);
                    refreshThisPage();
                  },
                  activeColor: utils.convertStrToColor(qp.id!),
                  value: showLikeAnimation,
                  title: Text(
                    "Show Like Animation",
                  ),
                ),
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) async {
                    showGreetAnimation = newValue;
                    utils.saveBoo(PrefKey.showGreetAnimation, showGreetAnimation);
                    refreshThisPage();
                  },
                  activeColor: utils.convertStrToColor(qp.id!),
                  value: showGreetAnimation,
                  title: Text(
                    "Show Greet Animation",
                  ),
                ),
              ],
            ),
            ReviewTitle(title: "Discovery"),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) async {
                    await apiManager.setConfig(UserCollection.isShowDiscover, newValue);
                    qp.isShowDiscover = newValue;
                    refreshThisPage();
                  },
                  activeColor: utils.convertStrToColor(qp.id!),
                  value: qp.isShowDiscover!,
                  title: Text(
                    "Show Your Profile In Discovery",
                  ),
                ),
              ],
            ),
            ReviewTitle(title: "Appearance"),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) async {
                    showMessageCount = newValue;
                    utils.saveBoo(PrefKey.showMessageCount, showMessageCount);
                    refreshThisPage();
                  },
                  activeColor: utils.convertStrToColor(qp.id!),
                  value: showMessageCount,
                  title: Text(
                    "Show Message Count",
                  ),
                ),
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) async {
                    showNotificationCount = newValue;
                    utils.saveBoo(PrefKey.showNotificationCount, showNotificationCount);
                    refreshThisPage();
                  },
                  activeColor: utils.convertStrToColor(qp.id!),
                  value: showNotificationCount,
                  title: Text(
                    "Show Notification Count",
                  ),
                ),
                _item(
                  "Shade Value",
                  textAlign: TextAlign.center,
                ),
                Slider.adaptive(
                  value: profileShadeValue,
                  min: AppConfig.minShade.toDouble(),
                  max: AppConfig.maxShade.toDouble(),
                  divisions: 10,
                  activeColor: appIconColor,
                  inactiveColor: unfocusedColor,
                  label: profileShadeValue < 0.3 ? "More shade" : profileShadeValue > 0.7 ? "Less shade" : "Medium shade",
                  onChanged: (double newValue) {
                    profileShadeValue = newValue;
                    utils.saveDouble(PrefKey.profileShadeValue, profileShadeValue);
                    refreshThisPage();
                  },
                ),
              ],
            ),
            ReviewTitle(title: "Push Notifications"),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) async {
                    await apiManager.setConfig(UserCollection.isReceive, newValue);
                    qp.isReceive = newValue;
                    refreshThisPage();
                  },
                  activeColor: utils.convertStrToColor(qp.id!),
                  value: qp.isReceive!,
                  title: Text(
                    "Allow Push Notifications",
                  ),
                ),
                _item("Use This Device To Receive Notifications", onPressed: () {
                  getBinaryDialog("Set Device", "The previously registered device will no longer receive push notifications for ${qp.email}. Are you sure?", () async {
                    getRoute.pop();
                    await apiManager.setFcmToken(forceUseToken: true);
                    utils.toast("Set successfully");
                  });
                }),
              ],
            ),
            InkWell(
              onTap: () {
                getBinaryDialog(TextData.signOut, TextData.areYouSure, () {
                  /// No need to remove uniqueid and secret from pref
                  utils.logoutClearData();
                  getRoute.navigateToAndPopAll(LandingPage());
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: null,
                    child: Text(
                      TextData.signOut,
                    ),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () {
                getBinaryDialog(TextData.deleteAccount, TextData.deleteAccountAreYouSure, () async {
                  getRoute.pop();
                  await getBinaryDialog("Confirm Deletion", "You will not be able to user this email ${qp.email} to register again. Are you sure?", () async {
                    getRoute.pop();
                    Dialogs.showLoadingDialog(title: "Deleting...");
                    apiManager.logAnalysis(Analysis.deleteAccount);
                    await apiManager.deleteAccount();
                    getRoute.pop();
                    utils.logoutClearData();
                    getRoute.navigateToAndPopAll(LandingPage());
                  });
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: null,
                    child: Text(
                      TextData.deleteAccount,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
