import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();

const db = admin.firestore();

export const sendPush = functions.https.onRequest(async (request, response) => {
    const targetId: string = request.query.targetId as string;
    const action: string = request.query.action as string;
    const firstName: string = request.query.firstName as string;

    if (targetId && action && firstName) {
        let title = "";
        let message = "";
        let screen = "";
        switch (action) {
            case "0": {
                title = "You are a match";
                screen = "match";
                message = firstName + " find you attractive!";
                break;
            }
            case "1": {
                title = "Hello there!";
                screen = "chat";
                message = `${firstName} sent you greetings`;
                break;
            }
            default: {
                title = "New Message";
                screen = "chat";
                message = `${firstName} sent you a new message`;
            }
        }
        const targetProfile = await db.collection("profiles").doc(targetId).get();
        if (targetProfile.exists) {
            const targetProfileData = targetProfile.data();
            if (targetProfileData) {
                const isReceive = targetProfileData.isReceive;
                if (isReceive) {
                    const fcmResult = await db.collection("fcm_token").doc(targetId).get();
                    if (fcmResult.exists) {
                        const fcmTokenData = fcmResult.data();
                        if (fcmTokenData) {
                            const payload = {
                                notification: {
                                    title: title,
                                    body: message,
                                },
                                data: {
                                    click_action: "FLUTTER_NOTIFICATION_CLICK",
                                    sound: "default",
                                    targetId: targetId,
                                    screen: screen,
                                },
                            };
                            const fcmToken = fcmTokenData.token;
                            await admin.messaging().sendToDevice(fcmToken, payload);
                        }
                    }
                }
            }
        }
    }
    response.send({
        statusCode: 200,
    });
});
